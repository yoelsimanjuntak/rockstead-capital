/*
SQLyog Community v12.09 (64 bit)
MySQL - 10.1.19-MariaDB : Database - db_taskmgmt
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `mcategory` */

DROP TABLE IF EXISTS `mcategory`;

CREATE TABLE `mcategory` (
  `ID_Category` bigint(10) NOT NULL AUTO_INCREMENT,
  `ID_Type` tinyint(1) NOT NULL,
  `NM_Category` varchar(50) NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_Category`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `mcategory` */

insert  into `mcategory`(`ID_Category`,`ID_Type`,`NM_Category`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (5,0,'New Implementation','admin','2019-10-08 15:42:15','admin','2019-10-08 15:49:09'),(6,0,'Enhancement','admin','2019-10-08 15:49:17',NULL,NULL),(7,0,'Support / Maintenance','admin','2019-10-08 15:49:27',NULL,NULL);

/*Table structure for table `mcustomer` */

DROP TABLE IF EXISTS `mcustomer`;

CREATE TABLE `mcustomer` (
  `ID_Customer` bigint(10) NOT NULL AUTO_INCREMENT,
  `NM_Customer` varchar(200) NOT NULL,
  `NM_Address` varchar(200) NOT NULL,
  `NM_PhoneNo` varchar(50) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_Customer`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `mcustomer` */

insert  into `mcustomer`(`ID_Customer`,`NM_Customer`,`NM_Address`,`NM_PhoneNo`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (3,'PT. MAJU MUNDUR','Middle of Nowhere','123456','admin','2019-10-01 18:18:54',NULL,NULL),(4,'PT. TIMBUL TENGGELAM','Middle of Nowhere','123456','admin','2019-10-01 18:22:48',NULL,NULL);

/*Table structure for table `mdepartment` */

DROP TABLE IF EXISTS `mdepartment`;

CREATE TABLE `mdepartment` (
  `ID_Department` bigint(10) NOT NULL AUTO_INCREMENT,
  `NM_Department` varchar(50) NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_Department`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `mdepartment` */

insert  into `mdepartment`(`ID_Department`,`NM_Department`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (1,'Accounting','admin','2019-10-01 13:36:00',NULL,NULL),(2,'Personalia','admin','2019-10-01 13:36:08',NULL,NULL),(3,'Information & Technology','admin','2019-10-01 13:36:22',NULL,NULL),(4,'Research & Development','admin','2019-10-01 13:36:34',NULL,NULL),(5,'Finance','admin','2019-10-01 13:36:49',NULL,NULL),(6,'General Affair','admin','2019-10-01 13:36:59',NULL,NULL);

/*Table structure for table `memployee` */

DROP TABLE IF EXISTS `memployee`;

CREATE TABLE `memployee` (
  `ID_Employee` bigint(10) NOT NULL AUTO_INCREMENT,
  `ID_Department` bigint(10) NOT NULL,
  `ID_Position` bigint(10) NOT NULL,
  `NM_NIK` varchar(50) NOT NULL,
  `NM_Employee` varchar(50) NOT NULL,
  `NM_JoinedDate` date NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_Employee`),
  KEY `FK_Employee_Dept` (`ID_Department`),
  KEY `FK_Employee_Pos` (`ID_Position`),
  CONSTRAINT `FK_Employee_Dept` FOREIGN KEY (`ID_Department`) REFERENCES `mdepartment` (`ID_Department`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_Employee_Pos` FOREIGN KEY (`ID_Position`) REFERENCES `mposition` (`ID_Position`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `memployee` */

insert  into `memployee`(`ID_Employee`,`ID_Department`,`ID_Position`,`NM_NIK`,`NM_Employee`,`NM_JoinedDate`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (1,3,6,'11112087','Yoel Rolas Simanjuntak','2018-09-30','admin','2019-10-01 19:21:15',NULL,NULL),(2,3,7,'11112088','Derseli Marpaung','2018-09-30','admin','2019-10-01 19:30:30','admin','2019-10-01 19:30:46'),(3,4,9,'11112089','Partopi Tao','2018-09-30','admin','2019-10-01 19:31:40','admin','2019-10-01 19:32:08'),(4,3,6,'11112090','Parlapo Tuak','2019-10-08','admin','2019-10-08 06:12:38',NULL,NULL);

/*Table structure for table `mposition` */

DROP TABLE IF EXISTS `mposition`;

CREATE TABLE `mposition` (
  `ID_Position` bigint(10) NOT NULL AUTO_INCREMENT,
  `NM_Position` varchar(50) NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_Position`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `mposition` */

insert  into `mposition`(`ID_Position`,`NM_Position`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (1,'Dept. Head','admin','2019-10-01 13:41:49','admin','2019-10-01 13:47:18'),(6,'Staff - Software Engineer','admin','2019-10-01 13:44:05','admin','2019-10-01 13:45:45'),(7,'Team Leader','admin','2019-10-01 13:44:42','admin','2019-10-01 13:46:39'),(8,'Staff - QA','admin','2019-10-01 13:46:07',NULL,NULL),(9,'Staff - System Engineer','admin','2019-10-01 13:46:25',NULL,NULL);

/*Table structure for table `mpriority` */

DROP TABLE IF EXISTS `mpriority`;

CREATE TABLE `mpriority` (
  `ID_Priority` bigint(10) NOT NULL AUTO_INCREMENT,
  `NM_Priority` varchar(50) NOT NULL,
  `NM_SortValue` double NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_Priority`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `mpriority` */

insert  into `mpriority`(`ID_Priority`,`NM_Priority`,`NM_SortValue`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (1,'High',1,'admin','2019-10-01 13:53:16','admin','2019-10-02 04:27:25'),(2,'Medium',2,'admin','2019-10-01 13:53:24','admin','2019-10-02 04:27:30'),(3,'Low',3,'admin','2019-10-01 13:53:29','admin','2019-10-02 04:27:36');

/*Table structure for table `mstatus` */

DROP TABLE IF EXISTS `mstatus`;

CREATE TABLE `mstatus` (
  `ID_Status` bigint(10) NOT NULL AUTO_INCREMENT,
  `NM_Status` varchar(50) NOT NULL,
  `NM_LabelColor` varchar(10) NOT NULL,
  `IS_Default` tinyint(1) DEFAULT '0',
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_Status`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `mstatus` */

insert  into `mstatus`(`ID_Status`,`NM_Status`,`NM_LabelColor`,`IS_Default`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (12,'New','#988888',1,'admin','2019-10-08 15:55:10','admin','2019-10-08 16:07:08'),(13,'Ongoing','#F1D219',0,'admin','2019-10-08 15:55:53',NULL,NULL),(14,'Closed','#07E31C',0,'admin','2019-10-08 15:57:52','admin','2019-10-08 15:58:46'),(15,'Hold','#EF4F0F',0,'admin','2019-10-08 16:02:54',NULL,NULL),(16,'Waiting for User','#1757F5',0,'admin','2019-10-08 16:03:38',NULL,NULL);

/*Table structure for table `postcategories` */

DROP TABLE IF EXISTS `postcategories`;

CREATE TABLE `postcategories` (
  `PostCategoryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryName` varchar(50) NOT NULL,
  `PostCategoryLabel` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`PostCategoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `postcategories` */

insert  into `postcategories`(`PostCategoryID`,`PostCategoryName`,`PostCategoryLabel`) values (1,'Berita','#f56954'),(2,'Artikel','#00a65a'),(3,'Kegiatan','#f39c12'),(4,'Galeri','#00c0ef'),(5,'Others','#3c8dbc');

/*Table structure for table `postimages` */

DROP TABLE IF EXISTS `postimages`;

CREATE TABLE `postimages` (
  `PostImageID` bigint(10) NOT NULL AUTO_INCREMENT,
  `PostID` bigint(10) NOT NULL,
  `FileName` varchar(250) NOT NULL,
  `Description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`PostImageID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `postimages` */

insert  into `postimages`(`PostImageID`,`PostID`,`FileName`,`Description`) values (1,5,'my_pic.jpg',NULL),(2,6,'Sipinsur.jpg',NULL),(3,7,'pindah_datang.jpg',NULL),(4,8,'utk-website.jpeg',NULL);

/*Table structure for table `posts` */

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `PostID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryID` int(10) NOT NULL,
  `PostDate` date NOT NULL,
  `PostTitle` varchar(200) NOT NULL,
  `PostSlug` varchar(200) NOT NULL,
  `PostContent` longtext NOT NULL,
  `PostExpiredDate` date NOT NULL,
  `TotalView` int(11) NOT NULL DEFAULT '0',
  `LastViewDate` datetime DEFAULT NULL,
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '1',
  `FileName` varchar(250) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  PRIMARY KEY (`PostID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `posts` */

insert  into `posts`(`PostID`,`PostCategoryID`,`PostDate`,`PostTitle`,`PostSlug`,`PostContent`,`PostExpiredDate`,`TotalView`,`LastViewDate`,`IsSuspend`,`FileName`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (4,5,'2019-06-26','Hubungi Kami','hubungi-kami','<p><strong>Dinas Komunikasi Dan Informatika Kabupaten Humbang Hasundutan</strong></p>\r\n\r\n<p>Jl. SM. Raja Kompleks Perkantoran Tano Tubu, Doloksanggul 22457</p>\r\n\r\n<table border=\"0\" cellpadding=\"1\" cellspacing=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Telp</td>\r\n			<td>:</td>\r\n			<td>&nbsp;(0633) 31555</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Email&nbsp;</td>\r\n			<td>:</td>\r\n			<td colspan=\"4\">&nbsp;diskominfo@humbanghasundutankab.go.id</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<hr />\r\n<p>&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><iframe frameborder=\"0\" height=\"450\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3986.6996793830294!2d98.76777421426368!3d2.265541658581495!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x302e3cb7ff0bcc73%3A0x2c1a5b16f34531dc!2sDinas+Komunikasi+dan+Informatika+Kab.+Humbang+Hasundutan!5e0!3m2!1sen!2sid!4v1561433370666!5m2!1sen!2sid\" style=\"border:0\" width=\"600\"></iframe></p>\r\n','2020-12-31',9,NULL,0,NULL,'admin','2019-06-26 08:27:46','admin','2019-06-26 08:27:46'),(5,2,'2019-07-22','Humbahas EXPO 2019','humbahas-expo-2019','<p>contoh&nbsp;</p>\r\n\r\n<p><strong>Doloksanggul</strong>&nbsp;- Dalam rangka Perayaan Hari Ulang Tahun Kabupaten Humbang Hasundutan Ke-16, Panitia Hari Ulang Tahun Kabupaten Humbang Hasundutan Ke-16 bekerjasama dengan Tim Kreatif &ldquo;DOXA STREET&rdquo; melaksanakan Pameran Pembangunan yaitu HUMBAHAS EXPO 2019 di Jalan Sisingamangaraja XII (depan SMP Negeri 2 Doloksanggul dan depan Kantor Polsek Doloksanggul), Minggu (21/7).<br />\r\n&nbsp;</p>\r\n\r\n<p>Kegiatan ini adalah sebagai upaya memberikan informasi seluas-luasnya kepada masyarakat tentang berbagai kemajuan yang telah dicapai Kabupaten Humbang Hasundutan sekaligus mempromosikan berbagai Usaha Mikro Kecil dan Menengah (UMKM) di Kabupaten Humbang Hasundutan.</p>\r\n\r\n<p>Hadir dalam pameran ini Wakil Bupati Humbang Hasundutan Saut Palindungan Simamora, Sekretaris Daerah Kabupaten Humbang Hasundutan Drs. Tonny Sihombing, M.IP, Pabung 0210/TU Mayor Infantri Holden Gultom, Para Pimpinan OPD, Pimpinan BUMN dan BUMD dan Tim Penggerak PKK Kabupaten Humbang Hasundutan.</p>\r\n\r\n<p>Dalam sambutannya, Wakil Bupati mengatakan agar UMKM harus terus didukung pertumbuhannya oleh seluruh kalangan masyarakat, khususnya generasi muda untuk memiliki keyakinan berwirausaha dan membuat lapangan kerjanya sendiri.<br />\r\nUMKM juga merupakan salah satu pilar pendukung utama perekonomian nasional. UMKM merupakan kunci keberhasilan negara menghadapi krisis ekonomi global dan telah terbukti tangguh menghadapi persaingan bahkan dari berbagai perusahaan yang telah mapan.</p>\r\n\r\n<p>Selain HUMBAHAS EXPO 2019, pada Pameran Pembangunan ini juga diselenggarakan pagelaran Marching Band (parade) dari SD, SMP, SMA dan SMK di Kabupaten Humbang Hasundutan, lomba melukis di atas kaos, lomba mewarnai untuk TK dan SD kelas 1 dan Kelas 2, lomba makan lampet dan lomba makan mie gomak.&nbsp;<strong>(Diskominfo)</strong></p>\r\n\r\n<hr />','2019-07-31',3,NULL,0,NULL,'admin','2019-07-22 03:22:56','admin','2019-07-22 03:25:19'),(6,1,'2019-07-22','Workshop Penyuluhan Pertanian','workshop-penyuluhan-pertanian','<p>Workshop Penyuluhan Pertanian Kabupaten Humbang Hasundutan dilakukan di kecamatan Pollung berjalan dengan Lancar dihadiri oleh 30 kelompok Tani.</p>\r\n\r\n<p>&nbsp;</p>\r\n','2020-07-20',5,NULL,0,NULL,'admin','2019-07-22 03:24:15','admin','2019-07-22 03:24:45'),(7,1,'2019-07-26','Sosoalisasi Penggunaan Aplikasi Simluhtan di DInas Kominfo','sosoalisasi-penggunaan-aplikasi-simluhtan-di-dinas-kominfo','<p>Sosoalisasi Penggunaan Aplikasi Simluhtan di DInas Kominfo</p>\r\n','2019-07-31',0,NULL,0,NULL,'admin','2019-07-26 07:52:07','admin','2019-07-26 07:52:07'),(8,4,'2019-08-08','Test','test','<p>Test</p>\r\n','2019-08-20',2,NULL,0,NULL,'admin','2019-08-08 08:31:49','admin','2019-08-08 08:31:49');

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `RoleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(50) NOT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `roles` */

insert  into `roles`(`RoleID`,`RoleName`) values (1,'Administrator'),(2,'Project Manager'),(3,'Project Manager Officer'),(4,'Employee');

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `SettingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`SettingID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `settings` */

insert  into `settings`(`SettingID`,`SettingLabel`,`SettingName`,`SettingValue`) values (1,'SETTING_WEB_NAME','SETTING_WEB_NAME','Project Tracker'),(2,'SETTING_WEB_DESC','SETTING_WEB_DESC','Sistem Informasi Pengerjaan Proyek'),(3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','https://project-tracker.disqus.com/embed.js'),(4,'SETTING_ORG_NAME','SETTING_ORG_NAME','Dinas Pertanian Kabupaten Humbang Hasundutan'),(5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','Jl. Sidikalang Km. 3.5 Simpang Sitapongan Desa Simangaronsang, Doloksanggul'),(6,'SETTING_ORG_LAT','SETTING_ORG_LAT',''),(7,'SETTING_ORG_LONG','SETTING_ORG_LONG',''),(8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','(0633) 31101, (0633) 31104'),(9,'SETTING_ORG_FAX','SETTING_ORG_FAX','(0633) 31103, (0633) 31103'),(10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','distan@humbanghasundutankab.go.id'),(11,'SETTING_WEB_API_FOOTERLINK','SETTING_WEB_API_FOOTERLINK','https://diskominfo.humbanghasundutankab.go.id/index.php/api/data_footer_link'),(12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','logo.png'),(13,'SETTING_WEB_SKIN_CLASS','SETTING_WEB_SKIN_CLASS','skin-green-light'),(14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','loader-128x/Ellipsis-3s-200px.gif'),(15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.0');

/*Table structure for table `tproject` */

DROP TABLE IF EXISTS `tproject`;

CREATE TABLE `tproject` (
  `ID_Project` bigint(10) NOT NULL AUTO_INCREMENT,
  `ID_Category` bigint(10) NOT NULL,
  `ID_Customer` bigint(10) NOT NULL,
  `ID_Status` bigint(10) NOT NULL,
  `ID_PM` bigint(10) NOT NULL,
  `ID_PMO` bigint(10) DEFAULT NULL,
  `NM_Project` varchar(200) NOT NULL,
  `NM_ProjectDesc` text,
  `NM_Attachment` varchar(200) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_Project`),
  KEY `FK_Project_Cust` (`ID_Customer`),
  KEY `FK_Project_Stat` (`ID_Status`),
  KEY `FK_Project_Cat` (`ID_Category`),
  KEY `FK_Project_PM` (`ID_PM`),
  KEY `FK_Project_PMO` (`ID_PMO`),
  CONSTRAINT `FK_Project_Cat` FOREIGN KEY (`ID_Category`) REFERENCES `mcategory` (`ID_Category`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_Project_Cust` FOREIGN KEY (`ID_Customer`) REFERENCES `mcustomer` (`ID_Customer`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_Project_PM` FOREIGN KEY (`ID_PM`) REFERENCES `memployee` (`ID_Employee`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_Project_PMO` FOREIGN KEY (`ID_PMO`) REFERENCES `memployee` (`ID_Employee`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_Project_Stat` FOREIGN KEY (`ID_Status`) REFERENCES `mstatus` (`ID_Status`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `tproject` */

insert  into `tproject`(`ID_Project`,`ID_Category`,`ID_Customer`,`ID_Status`,`ID_PM`,`ID_PMO`,`NM_Project`,`NM_ProjectDesc`,`NM_Attachment`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (7,5,3,13,2,1,'Chernobyl','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Design.pdf','dev.derseli','2019-10-08 16:19:43','admin','2019-10-08 16:37:54'),(8,5,3,12,2,1,'Bugtracker','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',NULL,'admin','2019-10-08 16:38:51',NULL,NULL);

/*Table structure for table `tproject_activity` */

DROP TABLE IF EXISTS `tproject_activity`;

CREATE TABLE `tproject_activity` (
  `ID_Activity` bigint(10) NOT NULL AUTO_INCREMENT,
  `ID_Task` bigint(10) NOT NULL,
  `ID_Employee` bigint(10) NOT NULL,
  `NM_Activity` text NOT NULL,
  `NM_Attachment` varchar(50) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_Activity`),
  KEY `FK_Activity_Task` (`ID_Task`),
  KEY `FK_Activity_Employee` (`ID_Employee`),
  CONSTRAINT `FK_Activity_Employee` FOREIGN KEY (`ID_Employee`) REFERENCES `memployee` (`ID_Employee`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_Activity_Task` FOREIGN KEY (`ID_Task`) REFERENCES `tproject_task` (`ID_Task`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `tproject_activity` */

insert  into `tproject_activity`(`ID_Activity`,`ID_Task`,`ID_Employee`,`NM_Activity`,`NM_Attachment`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (4,10,3,'Test','picture14869903408154.png','dev.partopitao','2019-10-08 16:34:27',NULL,NULL),(5,10,3,'Test 2','picture148699034081541.png','dev.partopitao','2019-10-08 16:34:34',NULL,NULL),(6,10,3,'Test 3',NULL,'dev.partopitao','2019-10-08 16:34:57',NULL,NULL);

/*Table structure for table `tproject_comment` */

DROP TABLE IF EXISTS `tproject_comment`;

CREATE TABLE `tproject_comment` (
  `ID_Comment` bigint(10) NOT NULL AUTO_INCREMENT,
  `ID_Project` bigint(10) NOT NULL,
  `ID_Task` bigint(10) DEFAULT NULL,
  `ID_Employee` bigint(10) NOT NULL,
  `NM_Comment` varchar(200) NOT NULL,
  `NM_Attachment` varchar(200) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_Comment`),
  KEY `FK_Comment_Project` (`ID_Project`),
  KEY `FK_Comment_Task` (`ID_Task`),
  KEY `FK_Comment_Employee` (`ID_Employee`),
  CONSTRAINT `FK_Comment_Employee` FOREIGN KEY (`ID_Employee`) REFERENCES `memployee` (`ID_Employee`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_Comment_Project` FOREIGN KEY (`ID_Project`) REFERENCES `tproject` (`ID_Project`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Comment_Task` FOREIGN KEY (`ID_Task`) REFERENCES `tproject_task` (`ID_Task`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tproject_comment` */

/*Table structure for table `tproject_employee` */

DROP TABLE IF EXISTS `tproject_employee`;

CREATE TABLE `tproject_employee` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `ID_Task` bigint(10) NOT NULL,
  `ID_Employee` bigint(10) NOT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_ProjectEmp_Task` (`ID_Task`),
  KEY `FK_ProjectEmp_Emp` (`ID_Employee`),
  CONSTRAINT `FK_ProjectEmp_Emp` FOREIGN KEY (`ID_Employee`) REFERENCES `memployee` (`ID_Employee`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_ProjectEmp_Task` FOREIGN KEY (`ID_Task`) REFERENCES `tproject_task` (`ID_Task`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

/*Data for the table `tproject_employee` */

insert  into `tproject_employee`(`Uniq`,`ID_Task`,`ID_Employee`) values (15,10,3),(16,11,3),(17,12,3),(18,13,4),(19,14,4);

/*Table structure for table `tproject_log` */

DROP TABLE IF EXISTS `tproject_log`;

CREATE TABLE `tproject_log` (
  `ID_Log` bigint(10) NOT NULL AUTO_INCREMENT,
  `ID_Project` bigint(10) NOT NULL,
  `ID_Task` bigint(10) DEFAULT NULL,
  `ID_Employee` bigint(10) NOT NULL,
  `NM_Log` varchar(50) NOT NULL,
  `NM_ValueFrom` varchar(50) NOT NULL,
  `NM_ValueTo` varchar(50) NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_Log`),
  KEY `FK_Log_Project` (`ID_Project`),
  KEY `FK_Log_Task` (`ID_Task`),
  KEY `FK_Log_Employee` (`ID_Employee`),
  CONSTRAINT `FK_Log_Employee` FOREIGN KEY (`ID_Employee`) REFERENCES `memployee` (`ID_Employee`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_Log_Project` FOREIGN KEY (`ID_Project`) REFERENCES `tproject` (`ID_Project`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Log_Task` FOREIGN KEY (`ID_Task`) REFERENCES `tproject_task` (`ID_Task`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `tproject_log` */

insert  into `tproject_log`(`ID_Log`,`ID_Project`,`ID_Task`,`ID_Employee`,`NM_Log`,`NM_ValueFrom`,`NM_ValueTo`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (6,7,10,3,'Change Status','New','Ongoing','dev.partopitao','2019-10-08 16:34:08',NULL,NULL);

/*Table structure for table `tproject_task` */

DROP TABLE IF EXISTS `tproject_task`;

CREATE TABLE `tproject_task` (
  `ID_Task` bigint(10) NOT NULL AUTO_INCREMENT,
  `ID_Project` bigint(10) NOT NULL,
  `ID_Priority` bigint(10) NOT NULL,
  `ID_Status` bigint(10) NOT NULL,
  `NM_Task` varchar(50) NOT NULL,
  `NM_PlannedStart` date NOT NULL,
  `NM_PlannedEnd` date NOT NULL,
  `NM_Attachment` varchar(200) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_Task`),
  KEY `FK_Task_Project` (`ID_Project`),
  KEY `FK_Task_Priority` (`ID_Priority`),
  KEY `FK_Task_Stat` (`ID_Status`),
  CONSTRAINT `FK_Task_Priority` FOREIGN KEY (`ID_Priority`) REFERENCES `mpriority` (`ID_Priority`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_Task_Project` FOREIGN KEY (`ID_Project`) REFERENCES `tproject` (`ID_Project`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Task_Stat` FOREIGN KEY (`ID_Status`) REFERENCES `mstatus` (`ID_Status`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `tproject_task` */

insert  into `tproject_task`(`ID_Task`,`ID_Project`,`ID_Priority`,`ID_Status`,`NM_Task`,`NM_PlannedStart`,`NM_PlannedEnd`,`NM_Attachment`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (10,7,1,13,'Preparation','2019-10-08','2019-10-10',NULL,'dev.derseli','2019-10-08 16:20:46',NULL,NULL),(11,7,1,12,'Implementation','2019-10-14','2019-10-16',NULL,'dev.derseli','2019-10-08 16:21:23',NULL,NULL),(12,7,1,12,'UAT','2019-10-17','2019-10-17',NULL,'dev.derseli','2019-10-08 16:22:06',NULL,NULL),(13,8,1,12,'Task 1','2019-10-08','2019-10-09',NULL,'admin','2019-10-08 16:39:25',NULL,NULL),(14,8,1,12,'Task 2','2019-10-10','2019-10-11',NULL,'admin','2019-10-08 16:39:42',NULL,NULL);

/*Table structure for table `userinformation` */

DROP TABLE IF EXISTS `userinformation`;

CREATE TABLE `userinformation` (
  `UserName` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `CompanyID` varchar(200) DEFAULT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `IdentityNo` varchar(50) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `ReligionID` int(10) DEFAULT NULL,
  `Gender` tinyint(1) DEFAULT NULL,
  `Address` text,
  `PhoneNumber` varchar(50) DEFAULT NULL,
  `EducationID` int(10) DEFAULT NULL,
  `UniversityName` varchar(50) DEFAULT NULL,
  `FacultyName` varchar(50) DEFAULT NULL,
  `MajorName` varchar(50) DEFAULT NULL,
  `IsGraduated` tinyint(1) NOT NULL DEFAULT '0',
  `GraduatedDate` date DEFAULT NULL,
  `YearOfExperience` int(10) DEFAULT NULL,
  `RecentPosition` varchar(250) DEFAULT NULL,
  `RecentSalary` double DEFAULT NULL,
  `ExpectedSalary` double DEFAULT NULL,
  `CVFilename` varchar(250) DEFAULT NULL,
  `ImageFilename` varchar(250) DEFAULT NULL,
  `RegisteredDate` date DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `userinformation` */

insert  into `userinformation`(`UserName`,`Email`,`CompanyID`,`Name`,`IdentityNo`,`BirthDate`,`ReligionID`,`Gender`,`Address`,`PhoneNumber`,`EducationID`,`UniversityName`,`FacultyName`,`MajorName`,`IsGraduated`,`GraduatedDate`,`YearOfExperience`,`RecentPosition`,`RecentSalary`,`ExpectedSalary`,`CVFilename`,`ImageFilename`,`RegisteredDate`) values ('admin','yoelrolas@gmail.com',NULL,'Administrator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-17'),('dev.derseli','dev.derseli@partopitao.co','2','Derseli Marpaung',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-08'),('dev.partopitao','dev.partopitao@partopitao.co','3','Partopi Tao',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-08'),('dev.yoelrolas','dev.yoelrolas@partopitao.co','1','Yoel R Simanjuntak',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-01');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(1) unsigned NOT NULL,
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`UserName`,`Password`,`RoleID`,`IsSuspend`,`LastLogin`,`LastLoginIP`) values ('admin','e10adc3949ba59abbe56e057f20f883e',1,0,'2019-10-08 12:12:23','::1'),('dev.derseli','e10adc3949ba59abbe56e057f20f883e',2,0,'2019-10-08 16:13:57','::1'),('dev.partopitao','e10adc3949ba59abbe56e057f20f883e',4,0,'2019-10-08 16:33:31','::1'),('dev.yoelrolas','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-10-08 16:23:31','::1');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
