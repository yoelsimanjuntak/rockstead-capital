# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.36-MariaDB)
# Database: web_rockstead
# Generation Time: 2020-05-15 17:23:27 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table postcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `postcategories`;

CREATE TABLE `postcategories` (
  `PostCategoryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryName` varchar(50) NOT NULL,
  `PostCategoryLabel` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`PostCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `postcategories` WRITE;
/*!40000 ALTER TABLE `postcategories` DISABLE KEYS */;

INSERT INTO `postcategories` (`PostCategoryID`, `PostCategoryName`, `PostCategoryLabel`)
VALUES
	(1,'Berita','#f56954'),
	(2,'Artikel','#00a65a'),
	(3,'Kegiatan','#f39c12'),
	(4,'Galeri','#00c0ef'),
	(5,'Others','#3c8dbc');

/*!40000 ALTER TABLE `postcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table postimages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `postimages`;

CREATE TABLE `postimages` (
  `PostImageID` bigint(10) NOT NULL AUTO_INCREMENT,
  `PostID` bigint(10) NOT NULL,
  `FileName` varchar(250) NOT NULL,
  `Description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`PostImageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `PostID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryID` int(10) NOT NULL,
  `PostDate` date NOT NULL,
  `PostTitle` varchar(200) NOT NULL,
  `PostSlug` varchar(200) NOT NULL,
  `PostContent` longtext NOT NULL,
  `PostExpiredDate` date NOT NULL,
  `TotalView` int(11) NOT NULL DEFAULT '0',
  `LastViewDate` datetime DEFAULT NULL,
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '1',
  `FileName` varchar(250) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  PRIMARY KEY (`PostID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;

INSERT INTO `posts` (`PostID`, `PostCategoryID`, `PostDate`, `PostTitle`, `PostSlug`, `PostContent`, `PostExpiredDate`, `TotalView`, `LastViewDate`, `IsSuspend`, `FileName`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(4,5,'2019-06-26','Hubungi Kami','hubungi-kami','<p><strong>Dinas Komunikasi Dan Informatika Kabupaten Humbang Hasundutan</strong></p>\r\n\r\n<p>Jl. SM. Raja Kompleks Perkantoran Tano Tubu, Doloksanggul 22457</p>\r\n\r\n<table border=\"0\" cellpadding=\"1\" cellspacing=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Telp</td>\r\n			<td>:</td>\r\n			<td>&nbsp;(0633) 31555</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Email&nbsp;</td>\r\n			<td>:</td>\r\n			<td colspan=\"4\">&nbsp;diskominfo@humbanghasundutankab.go.id</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<hr />\r\n<p>&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><iframe frameborder=\"0\" height=\"450\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3986.6996793830294!2d98.76777421426368!3d2.265541658581495!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x302e3cb7ff0bcc73%3A0x2c1a5b16f34531dc!2sDinas+Komunikasi+dan+Informatika+Kab.+Humbang+Hasundutan!5e0!3m2!1sen!2sid!4v1561433370666!5m2!1sen!2sid\" style=\"border:0\" width=\"600\"></iframe></p>\r\n','2020-12-31',10,NULL,0,NULL,'admin','2019-06-26 08:27:46','admin','2019-06-26 08:27:46'),
	(5,5,'2019-10-16','Rockstead Fixed Income Fund','rockstead-fixed-income-fund','<p style=\"text-align:justify\">Rockstead Fixed Income Fund is a hedge fund investment launched in 2016. Through its portfolio distribution between Bonds and FX (using a proprietary algorithm) , the Fund is able to offer investors an opportunity to earn superior returns. Investors can choose between a guaranteed fixed return (Share B) or a &lsquo;high-risk-high-return&rdquo; investment (Share A). The Fund obeys a monthly settlement regime. Investment monies, monitoring and reporting are outsourced to a 3rd party Administrator (Crowe).The Fund has been registered as a S13CA fund and its investment gains are tax-exempt. Investors should however obtain their personal tax advice with regard to their personal tax exposure from dividends arising from their investment with the Fund.</p>\r\n','2021-10-16',11,NULL,0,NULL,'admin','2019-10-16 09:14:16','admin','2019-10-16 09:18:16'),
	(6,5,'2019-10-16','1ROCKSTEAD GIP FUND II PTE LTD','1rockstead-gip-fund-ii-pte-ltd','<p style=\"text-align:justify\">1Rockstead GIP Fund II Pte. Ltd. is a approved GIP fund under the Global Investor Programme (GIP). For more details on GIP programme, please refer to&nbsp;<a href=\"https://www.contactsingapore.sg/gip\">http://www.contactsingapore.sg/gip</a>&nbsp;for latest updates. The Fund was launched on 1 September 2013 and is currently in the midst of fund raising. In the meantime, we have already began to make strategic investments in Singapore and Asia.</p>\r\n\r\n<p style=\"text-align:justify\">The Fund&rsquo;s main objective is to invest in a diversified portfolio of investments across various geographies and sectors. The Fund Manager aims to invest via structures that will facilitate the ease of exits and investor redemptions. The Fund will target to invest in sectors within the Target Markets which will provide opportunities for attractive risk-adjusted returns. As a directional guide, the Fund intends to invest in the following geographies and sectors via the following investment strategy:</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p><u><strong>By Geography:</strong></u></p>\r\n\r\n<p>The Fund plans to diversify its investments geographically by investing at least 50% of its AUM in Singapore, or companies with operations (or with intention to bring/transfer its operations) in Singapore, including 10% in Singapore&rsquo;s early stage / start ups, with the balance to be invested in companies in the below mentioned Target Markets.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>By Sector:</strong></u></p>\r\n\r\n<p>&ndash; Clean energy / Environmental</p>\r\n\r\n<p>&ndash; Consumer products &amp; services / Lifestyle</p>\r\n\r\n<p>&ndash; Healthcare / Medical / Education</p>\r\n','2020-10-16',4,NULL,0,NULL,'admin','2019-10-16 09:21:18','admin','2019-10-16 09:22:03'),
	(7,5,'2019-10-16','1ROCKSTEAD GIP FUND LTD','1rockstead-gip-fund-ltd','<p style=\"text-align:justify\">The Fund is a GIP approved fund under the Global Investor Programme (GIP). For more details on GIP programme, please refer to&nbsp;<a href=\"https://www.contactsingapore.sg/gip\">http://www.contactsingapore.sg/gip</a>&nbsp;for latest updates. We have fully deployed our proceeds into carefully selected target companies as per industries stated in our mandate. For latest update on the performance of the Fund, please contact us via our enquiry page.</p>\r\n','2020-10-16',3,NULL,0,NULL,'admin','2019-10-16 09:22:53','admin','2019-10-16 09:23:22'),
	(8,5,'2019-10-16','RS CLEAN RESOURCES FUND','rs-clean-resources-fund','<p>The Fund was ended in 2010. It focused on investments in the clean energy and China growth sectors.</p>\r\n','2020-10-16',4,NULL,0,NULL,'admin','2019-10-16 09:24:04','admin','2019-10-16 09:24:04');

/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `RoleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(50) NOT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`RoleID`, `RoleName`)
VALUES
	(1,'Administrator'),
	(2,'Project Manager'),
	(3,'Project Manager Officer'),
	(4,'Employee');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `SettingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`SettingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;

INSERT INTO `settings` (`SettingID`, `SettingLabel`, `SettingName`, `SettingValue`)
VALUES
	(1,'SETTING_WEB_NAME','SETTING_WEB_NAME','Rockstead Capital'),
	(2,'SETTING_WEB_DESC','SETTING_WEB_DESC','Our Client, Our Commitment'),
	(3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','https://general-9.disqus.com/embed.js'),
	(4,'SETTING_ORG_NAME','SETTING_ORG_NAME','-'),
	(5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','Rockstead Capital Group, Singapore'),
	(6,'SETTING_ORG_LAT','SETTING_ORG_LAT',''),
	(7,'SETTING_ORG_LONG','SETTING_ORG_LONG',''),
	(8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','Singapore : +65 6228 0348'),
	(9,'SETTING_ORG_FAX','SETTING_ORG_FAX','-'),
	(10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','fund.admin@rockstead.com'),
	(11,'SETTING_WEB_API_FOOTERLINK','SETTING_WEB_API_FOOTERLINK','https://diskominfo.humbanghasundutankab.go.id/index.php/api/data_footer_link'),
	(12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','logo.png'),
	(13,'SETTING_WEB_SKIN_CLASS','SETTING_WEB_SKIN_CLASS','skin-green-light'),
	(14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','loader-128x/Ellipsis-3s-200px.gif'),
	(15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.0'),
	(16,'SETTING_WEB_ABOUT','SETTING_WEB_ABOUT','Rockstead Capital was established in 2005. We believe in building long-term relationships, aligning to our clients\' interests, and protecting the trust placed in us. By providing bespoke asset management and legacy planning services to individuals and families, we protect and grow their wealth. We hold a Capital Markets Services license as a Registered Fund Management Company (RFMC), issued by the Monetary Authority of Singapore'),
	(18,'SETTING_ORG_ADDRESS_DETAIL','SETTING_ORG_ADDRESS_DETAIL','Rockstead Capital Group\r\n138 Robinson Road\r\n#17-01, Oxley Tower\r\nSingapore 068906'),
	(19,'SETTING_ORG_ADDRESS_MAP','SETTING_ORG_ADDRESS_MAP','<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15955.299348970188!2d103.84842!3d1.278646!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x9cfea404c5d477c7!2sRockstead%20Capital%20Group!5e0!3m2!1sen!2sid!4v1585742612789!5m2!1sen!2sid\" width=\"800\" height=\"600\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>'),
	(20,'SETTING_WEB_SECT_OURVALUE','SETTING_WEB_SECT_OURVALUE','<div class=\"col-lg-3 col-md-3 col-sm-6\">\n                    <div class=\"single-service-content\">\n                        <div class=\"icon-heading\">\n                            <div class=\"icon\"><i class=\"fa fa-balance-scale\" style=\"background-color: white\"></i></div>\n                            <h6 style=\"font-size: 14.5pt\"><a href=\"javacript:void(0)\" style=\"cursor: default;\">Independent Thought and Action</a></h6>\n                        </div>\n                        <p>\n                            We source the most suitable products in the market for our clients and we think and act from their perspectives. We are not tied or obliged to any institution or partner.\n                        </p>\n                    </div>\n                </div>\n                <div class=\"col-lg-3 col-md-3 col-sm-6\">\n                    <div class=\"single-service-content\">\n                        <div class=\"icon-heading tran3s\">\n                            <div class=\"icon tran3s\"><i class=\"fa fa-users\" style=\"background-color: white\" aria-hidden=\"true\"></i></div>\n                            <h6 style=\"font-size: 14.5pt\"><a href=\"javacript:void(0)\" style=\"cursor: default;\">Personal Attention</a></h6>\n                        </div>\n                        <p>\n                            We bring the best of both worlds: services of a large firm, with the dedicated attention of a small firm.\n                        </p>\n                    </div>\n                </div>\n                <div class=\"col-lg-3 col-md-3 col-sm-6\">\n                    <div class=\"single-service-content\">\n                        <div class=\"icon-heading tran3s\">\n                            <div class=\"icon tran3s\"><i class=\"fa fa-thumbs-up\" style=\"background-color: white\" aria-hidden=\"true\"></i></div>\n                            <h6 style=\"font-size: 14.5pt\"><a href=\"javacript:void(0)\" style=\"cursor: default;\">Partnership</a></h6>\n                        </div>\n                        <p>\n                            We partner with our clients and business associates to minimize conflicts of interest and focus on the long-term.\n                        </p>\n                    </div>\n                </div>\n                <div class=\"col-lg-3 col-md-3 col-sm-6\">\n                    <div class=\"single-service-content\">\n                        <div class=\"icon-heading tran3s\">\n                            <div class=\"icon tran3s\"><i class=\"fa fa-history\" style=\"background-color: white\" aria-hidden=\"true\"></i></div>\n                            <h6 style=\"font-size: 14.5pt\"><a href=\"javacript:void(0)\" style=\"cursor: default;\">Long-Term Relationships</a></h6>\n                        </div>\n                        <p>\n                            We build long-term relationships with our clients, staff, and business associates.\n                        </p>\n                    </div>\n                </div>'),
	(21,'SETTING_WEB_SECT_OURBELIEF','SETTING_WEB_SECT_OURBELIEF','Our investors are more than just clients. They are our core capital, our motivation for growth and preservation of our legacy. Our commitment to this philosophy shapes our investment ethos. With clients at the heart of our investment strategies, our product portfolio is governed by the primary need to deliver investment strategies that maximizes risk-adjusted returns tailored for each investor.\r\n'),
	(22,'SETTING_WEB_SECT_OURTEAM','SETTING_WEB_SECT_OURTEAM','<div class=\"col-lg-4 col-md-4 col-sm-4\">\n                <div class=\"img\">\n                  <img src=\"http://localhost/web-rockstead-capital/assets/media/image/team-lester-tay.png\" style=\"height: 300px\" alt=\"Our Team\">\n                </div>\n              </div>\n              <div class=\"col-lg-8 col-md-8 col-sm-8\">\n                <p style=\"text-align: center; margin-bottom: 20px; font-weight: bold; font-style: italic\">\n                  \"Building time honored client relationship, with integrity is my guiding principle. Together with my investment team,  we succor to preserve client capital,  securing them a peace of mind and building a mark for their legacy.\"\n                </p>\n                <p style=\"text-align: center; margin-bottom: 20px\">\n                  MR LESTER TAY<br>\n                  ROCKSTEAD CAPITAL GROUP. AS THE FOUNDER OF THE COMPANY\n                </p>\n                <p style=\"text-align: justify\">\n                  With 22 years of investment and financial market experience, Mr. Lester Tay takes helm of the investment division of ROCKSTEAD CAPITAL group, creating several capital initiatives, including private equities and hedge funds products. Principally governed by capital preservation and risk management, this transcends into the company?s investment doctrines in formulating fund products.\n                </p>\n              </div>\n              <div class=\"clearfix\" style=\"margin-bottom: 40px\"></div>\n              <div class=\"col-lg-6 col-md-6 col-sm-6\" style=\"padding: 10px;\">\n                <div style=\"padding: 10px; background-color: #e4edff\">\n                  <h5 style=\"color: #1f386d; margin-bottom: 10px\">BACKGROUND</h5>\n                  <p>\n                    </p><ul class=\"ul-normal\" style=\"list-style-type: inherit\">\n                      <li>A certified public accountant (\"CPA\")</li>\n                      <li>Member of the Institute of Singapore Chartered Accountants (\"ISCA\") </li>\n                      <li>Member of Singapore Institute of Directors (\"SID\")</li>\n                    </ul>\n                  <p></p>\n                </div>\n              </div>\n              <div class=\"col-lg-6 col-md-6 col-sm-6\" style=\"padding: 10px;\">\n                <div style=\"padding: 10px; background-color: #e4edff\">\n                  <h5 style=\"color: #1f386d; margin-bottom: 10px\">ACCOLADES</h5>\n                  <p>\n                    </p><ul class=\"ul-normal\" style=\"list-style-type: inherit\">\n                      <li>Guest speaker at NTU \"High Flying CFOs in town\" forum</li>\n                      <li>Guest speaker at CCHs \"CFO roles in today?s economy\" forum</li>\n                      <li>Various Business Times and news bulletins and</li>\n                      <li>Finance committee member of the Singapore Armed Forces reservist association (\"SAFRA\")</li>\n                      <li>Awarded Nanyang Outstanding Young Alumni in 2007 by Nanyang Technological University (\"NTU\")</li>\n                    </ul>\n                  <p></p>\n                </div>\n              </div>'),
	(23,'SETTING_WEB_SECT_HOWWEWORK','SETTING_WEB_SECT_HOWWEWORK','<div class=\"theme-title\" style=\"margin-top: 0px\">\n              <h2 style=\"color: #fff !important;\">How We Work</h2>\n          </div>\n          <div class=\"container\">\n              <div class=\"row\">\n                  <div class=\"col-lg-6 col-md-6 col-sm-12\">\n                      <div class=\"single-about-content\">\n                          <div class=\"icon round-border tran3s\">\n                              <i class=\"fa fa-check\" aria-hidden=\"true\"></i>\n                          </div>\n                          <h5><a href=\"javacript:void(0)\" style=\"cursor: default;\">Our Promise</a></h5>\n                          <p style=\"color: #fff; text-align: justify\">\n                              Our clients\' financial objectives are at the core of all that we do.\n                              For our clients, this means less fees, greater trust, and better investment advice that fit their financial plans\n                          </p>\n                      </div>\n                  </div>\n\n                  <div class=\"col-lg-6 col-md-6 col-sm-12\">\n                      <div class=\"single-about-content\">\n                          <div class=\"icon round-border tran3s\">\n                              <i class=\"fa fa-check\" aria-hidden=\"true\"></i>\n                          </div>\n                          <h5><a href=\"javacript:void(0)\" style=\"cursor: default;\">Separation of Powers and Ownership</a></h5>\n                          <p style=\"color: #fff; text-align: justify\">\n                              Client always have custody of their assets, held in trust at their preferred private bank. We don\'t take sales commissions.\n                              Our independence lets us provide sound advice and effective investment decisions on behalf of our clients.\n                          </p>\n                      </div>\n                  </div>\n              </div>\n\n              <div class=\"theme-title\" style=\"margin-top: 100px\">\n                  <h5  style=\"color: #fff\">Strategic and Tactical Allocation</h5>\n                  <p style=\"text-align: justify\">\n                      We search for and build funds with different mandates and subscription structures. We then allocate capital according to our client\'s investment horizon, risk appetite, and preferences. For in-house funds, we adjust our positions in asset classes and instruments based on mandate, attractiveness, suitability, macro conditions, and market timing. These include equities, bonds, currency pairs, and even other external funds.\n                  </p>\n              </div>\n              <div class=\"row\" style=\"margin-top: 20px\">\n                  <div class=\"col-lg-4 col-md-4 col-sm-12\">\n                      <div class=\"single-service-content\" style=\"min-height: 240px\">\n                          <div class=\"icon-heading\" style=\"top: -5px !important;\">\n                              <div class=\"icon-2\"><i class=\"fa fa-shield\" style=\"background: rgb(102, 102, 102); color: #fff\"></i></div>\n                              <h6 style=\"font-size: 14.5pt\"><a href=\"javacript:void(0)\" style=\"cursor: default; color: #fff\">Conservative</a></h6>\n                          </div>\n                          <p>\n                              Invested in bonds, no lock-up, frequent dividends, stable moderate returns.\n                          </p>\n                      </div>\n                  </div>\n                  <div class=\"col-lg-4 col-md-4 col-sm-12\">\n                      <div class=\"single-service-content\" style=\"min-height: 240px\">\n                          <div class=\"icon-heading\" style=\"top: -5px !important;\">\n                              <div class=\"icon-2\"><i class=\"fa fa-star\" style=\"background: rgb(102, 102, 102); color: #fff\"></i></div>\n                              <h6 style=\"font-size: 14.5pt\"><a href=\"javacript:void(0)\" style=\"cursor: default; color: #fff\">Aggressive</a></h6>\n                          </div>\n                          <p>\n                              Traded in forex strategies, no lock-up, frequent dividends, risky high returns.\n                          </p>\n                      </div>\n                  </div>\n                  <div class=\"col-lg-4 col-md-4 col-sm-12\">\n                      <div class=\"single-service-content\" style=\"min-height: 240px\">\n                          <div class=\"icon-heading\" style=\"top: -5px !important;\">\n                              <div class=\"icon-2\"><i class=\"fa fa-hourglass\" style=\"background: rgb(102, 102, 102); color: #fff\"></i></div>\n                              <h6 style=\"font-size: 14.5pt\"><a href=\"javacript:void(0)\" style=\"cursor: default; color: #fff\">Long-Horizon</a></h6>\n                          </div>\n                          <p>\n                              Mixed allocation, long lock-up, occasional dividends, stable high returns\n                          </p>\n                      </div>\n                  </div>\n              </div>\n          </div>'),
	(24,'SETTING_WEB_DISCLAIMER','SETTING_WEB_DISCLAIMER','<p style=\"margin-top: 20px; text-align: justify\">\n        <strong>By entering this site, you agree to be bound by the following Terms and Conditions of Use.</strong>\n        <br />\n        Rockstead Capital Private Limited is a Registered Fund Management Company under the regime of the Monetary Authority of Singapore to provide Fund Management Services to individuals and corporates who are Accredited Investors.\n      </p>\n      <p style=\"margin-top: 20px; text-align: justify\">\n        <strong>Accredited & Professional Investors</strong>\n        <br />\n        By using this site you represent and warrant that you are an accredited and professional investor as defined under Section4A(1)(a) of the Securities and Futures Act, either being an individual with personal net assets in excess of S$2 million; financial assets (net of any related liabilities) exceed S$1 million; or income in the preceding 12 months of not less than S$300,000, or a company with net assets in excess of S$10 million. In using this site, the user represents that he/she is an accredited and/or professional investor and the use of this site is solely for their own information purposes only.\n      </p>\n      <p style=\"margin-top: 20px; text-align: justify\">\n        <strong>Information Purposes Only</strong>\n        <br />\n        The information published within this website is for information purposes only and shall not be construed as an offer, invitation, inducement or a solicitation to subscribe in the funds or products referred herewith. This site does not constitute an investment advice or counsel or solicitation for investment in any of the funds we manage. The information contained on this website is not directed at or intended for distribution to, or use by, any person in any jurisdiction or country where such use or distribution would be contrary to any applicable local law or regulation or would subject Rockstead Capital Group to any registration or licensing requirement in such jurisdiction. It is your responsibility to inform yourself of any applicable legal and regulatory restrictions and to ensure that your access and use of this information does not contravene any such restrictions and to observe all applicable laws and regulations of any relevant jurisdiction. Professional advice should be sought in cases of doubt, as any failure to do may constitute a breach of the securities laws in any such jurisdiction.\n        Any offer or solicitation will be made only upon execution of completed information memorandum, subscription application and relevant documentation, all of which must be read in their entirety. No offer to subscribe in shares will be made or accepted prior to receipt by the offeree documents and the completion of all appropriate documentation. Access to information about the funds is limited to investors who qualify as accredited investors only.The value of any investment made in the funds or otherwise and the income from such can fluctuate, and the investor may not get back the full amount invested. Any projections or forward-looking statements are not necessary indicative of future or likely performance. Past performance is no guarantee of future performance of a fund. Funds that invest in asset classes carrying greater risk, such as forex trading, high yield securities and securities of small capitalisation companies may have a higher risk of loss of capital.\n      </p>\n      <p style=\"margin-top: 20px; text-align: justify\">\n        <strong>Conditions of Use</strong>\n        <br />\n        I have read and accept the terms and conditions of use.\n      </p>\n      <hr />');

/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table userinformation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userinformation`;

CREATE TABLE `userinformation` (
  `UserName` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `CompanyID` varchar(200) DEFAULT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `IdentityNo` varchar(50) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `ReligionID` int(10) DEFAULT NULL,
  `Gender` tinyint(1) DEFAULT NULL,
  `Address` text,
  `PhoneNumber` varchar(50) DEFAULT NULL,
  `EducationID` int(10) DEFAULT NULL,
  `UniversityName` varchar(50) DEFAULT NULL,
  `FacultyName` varchar(50) DEFAULT NULL,
  `MajorName` varchar(50) DEFAULT NULL,
  `IsGraduated` tinyint(1) NOT NULL DEFAULT '0',
  `GraduatedDate` date DEFAULT NULL,
  `YearOfExperience` int(10) DEFAULT NULL,
  `RecentPosition` varchar(250) DEFAULT NULL,
  `RecentSalary` double DEFAULT NULL,
  `ExpectedSalary` double DEFAULT NULL,
  `CVFilename` varchar(250) DEFAULT NULL,
  `ImageFilename` varchar(250) DEFAULT NULL,
  `RegisteredDate` date DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `userinformation` WRITE;
/*!40000 ALTER TABLE `userinformation` DISABLE KEYS */;

INSERT INTO `userinformation` (`UserName`, `Email`, `CompanyID`, `Name`, `IdentityNo`, `BirthDate`, `ReligionID`, `Gender`, `Address`, `PhoneNumber`, `EducationID`, `UniversityName`, `FacultyName`, `MajorName`, `IsGraduated`, `GraduatedDate`, `YearOfExperience`, `RecentPosition`, `RecentSalary`, `ExpectedSalary`, `CVFilename`, `ImageFilename`, `RegisteredDate`)
VALUES
	('admin','yoelrolas@gmail.com',NULL,'Administrator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-17');

/*!40000 ALTER TABLE `userinformation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(1) unsigned NOT NULL,
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`UserName`, `Password`, `RoleID`, `IsSuspend`, `LastLogin`, `LastLoginIP`)
VALUES
	('admin','e10adc3949ba59abbe56e057f20f883e',1,0,'2020-05-08 18:29:52','::1');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table userschedule
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userschedule`;

CREATE TABLE `userschedule` (
  `KD_Schedule` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL,
  `KD_ScheduleType` varchar(10) DEFAULT NULL,
  `KD_ScheduleDay` bigint(10) unsigned DEFAULT NULL,
  `DATE_ScheduleDate` date DEFAULT NULL,
  `DATE_ScheduleTime` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`KD_Schedule`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table usertype
# ------------------------------------------------------------

DROP TABLE IF EXISTS `usertype`;

CREATE TABLE `usertype` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) DEFAULT NULL,
  `KD_Type` int(11) DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
