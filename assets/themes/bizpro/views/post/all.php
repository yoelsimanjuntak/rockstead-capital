<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- For Resposive Device -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?=!empty($title) ? $title.' | '.$this->setting_web_name : $this->setting_web_name?></title>

    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="56x56" href="<?=base_url()?>assets/themes/bizpro/images/fav-icon/icon.png">
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />

    <!-- Main style sheet -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/bizpro/css/style.css">
    <!-- responsive style sheet -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/bizpro/css/responsive.css">
    <link href="<?=base_url()?>assets/css/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">


    <!-- Fix Internet Explorer ______________________________________-->

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="<?=base_url()?>assets/themes/bizpro/vendor/html5shiv.js"></script>
    <script src="<?=base_url()?>assets/themes/bizpro/vendor/respond.js"></script>
    <![endif]-->

    <style>
    .blog-details-page table {
      background: #fff !important;
    }
    .blog-details-page table td, .blog-details-page table th {
      padding: 5px;
    }
    </style>
</head>

<body>
<div class="main-page-wrapper"  style="background-color: #f3f3f3">
  <header class="theme-main-header">
      <div class="container">
          <a href="<?=base_url()?>" class="logo float-left tran4s"><img src="<?=base_url()?>assets/themes/bizpro/images/logo/logo.png" alt="Logo" style="height: 48px"></a>
          <nav class="navbar float-right theme-main-menu one-page-menu">
              <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                      <span class="sr-only">Toggle navigation</span>
                      <!--Menu-->
                      <i class="fa fa-bars" aria-hidden="true"></i>
                  </button>
              </div>
              <div class="collapse navbar-collapse" id="navbar-collapse-1">
                  <ul class="nav navbar-nav">
                      <li class="active"><a href="#home">HOME</a></li>
                      <li><a style="border: 1px solid #6c757d;padding: 5px 8px;border-radius: 10px; margin: 2px 5px" href="<?=site_url('user/login')?>"><i class="fad fa-sign-in"></i> LOGIN</a></li>
                  </ul>
              </div>
          </nav>
      </div>
  </header>

    <section class="inner-page-banner" style="background: url(<?=MY_ASSETURL."/themes/bizpro/images/slider/slide-4.jpg"?>) no-repeat center !important">
        <div class="opacity">
            <div class="container">
                <h2>News</h2>
                <ul>
                    <li><a href="<?=site_url()?>">Home</a></li>
                    <li>/</li>
                    <li>News</li>
                </ul>
            </div>
        </div>
    </section>

    <article class="blog-details-page" style="padding-bottom: 100px">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-fix">
              <table style="width: 100%" border="1">
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>Date</th>
                    <th>By</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(count($res)>0) {
                    foreach ($res as $r) {
                      ?>
                      <tr>
                        <td><?=anchor(site_url('post/view/'.$r[COL_POSTSLUG]), $r[COL_POSTTITLE])?></td>
                        <td style="text-align: right"><?=$r[COL_CREATEDON]?></td>
                        <td><?=$r[COL_NAME]?></td>
                      </tr>
                      <?php
                    }
                  } else {
                    ?>
                    <tr>
                      <td colspan="3"><p style="font-style: italic">No data available</p></td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
        </div>
    </article>
    <footer style="background-color: #fff">
        <div class="container">
            <a href="<?=base_url()?>" class="logo"><img src="<?=base_url()?>assets/themes/bizpro/images/logo/logo.png" alt="Logo" style="width: 160px;"></a>
            <ul>
                <!--<li><a href="#" class="tran3s round-border"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#" class="tran3s round-border"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>-->
                <li><a href="#" class="tran3s round-border"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                <!--<li><a href="#" class="tran3s round-border"><i class="fa fa-rss" aria-hidden="true"></i></a></li>-->
            </ul>
            <p>Copyright <?=date('Y')?> All rights reserved <!--| Strongly designed by <a style="color: #fff; font-weight: bold" href="https://www.linkedin.com/in/yoelrolas/" target="_blank">Partopi Tao</a>.--></p>
        </div>
    </footer>
    <div id="loader-wrapper">
        <div id="preloader_1">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>

    <button class="scroll-top tran3s p-color-bg">
        <i class="fa fa-long-arrow-up" aria-hidden="true"></i>
    </button>

    <!-- j Query -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/jquery.2.2.3.min.js"></script>

    <!-- Bootstrap JS -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/bootstrap/bootstrap.min.js"></script>

    <!-- Vendor js _________ -->

    <!-- owl.carousel -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/owl-carousel/owl.carousel.min.js"></script>
    <!-- mixitUp -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/jquery.mixitup.min.js"></script>
    <!-- Progress Bar js -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/skills-master/jquery.skills.js"></script>
    <!-- Validation -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/contact-form/validate.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/contact-form/jquery.form.js"></script>
    <!-- Calendar js -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/monthly-master/js/monthly.js"></script>


    <!-- Theme js -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/js/theme.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/js/map-script.js"></script>

</div> <!-- /.main-page-wrapper -->
</body>
</html>
