<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 6/19/2019
 * Time: 10:26 AM
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- For Resposive Device -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?=!empty($title) ? $title.' | '.$this->setting_web_name : $this->setting_web_name?></title>

    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="56x56" href="<?=base_url()?>assets/themes/bizpro/images/fav-icon/icon.png">


    <!-- Main style sheet -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/bizpro/css/style.css">
    <!-- responsive style sheet -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/bizpro/css/responsive.css">

    <link href="<?=base_url()?>assets/css/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">


    <!-- Fix Internet Explorer ______________________________________-->

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="vendor/html5shiv.js"></script>
    <script src="vendor/respond.js"></script>
    <![endif]-->

</head>
<body>
<div class="main-page-wrapper">
    <header class="theme-main-header fixed-layout fixed">
        <div class="container">
            <a href="<?=base_url()?>" class="logo float-left tran4s"><img src="<?=base_url()?>assets/themes/bizpro/images/logo/logo.png" alt="Logo" style="height: 48px"></a>
            <nav class="navbar float-right theme-main-menu one-page-menu">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        Menu
                        <i class="fa fa-bars" aria-hidden="true"></i>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="<?=base_url()?>">HOME</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>

    <div class="page-middle-banner">
        <div class="opacity">
            <h3><?=$data[COL_POSTTITLE]?></h3>
            <div class="container"></div>
        </div>
    </div>

    <section id="about-us">
        <div class="container">
            <?php
            $files = $this->db->where(COL_POSTID, $data[COL_POSTID])->get(TBL_POSTIMAGES)->result_array();
            ?>
            <div class="col-sm-12" style="display: flex; flex-flow: column wrap; text-align: center; margin: 10px 0px;">
                <div class="lightBoxGallery" style="width: 100%">
                    <?php
                    if(count($files) > 1) {

                        foreach($files as $f) {
                            ?>
                            <a href="<?=MY_UPLOADURL.$f[COL_FILENAME]?>" title="<?=$data[COL_POSTTITLE]?>" data-gallery="">
                                <img src="<?=MY_UPLOADURL.$f[COL_FILENAME]?>" style="max-width: 50vh; margin: 5px; box-shadow: 0 1px 1px rgba(0,0,0,0.2)">
                            </a>
                        <?php
                        }
                    } else if(!empty($files[0])) {
                        ?>
                        <a href="<?=MY_UPLOADURL.$files[0][COL_FILENAME]?>" title="<?=$data[COL_POSTTITLE]?>" data-gallery="">
                            <img src="<?=!empty($files[0][COL_FILENAME])?MY_UPLOADURL.$files[0][COL_FILENAME]:MY_NOIMAGEURL?>" alt="<?=$data[COL_POSTTITLE]?>" style="width: 100%; padding: 5px" />
                        </a>
                    <?php
                    }
                    ?>
                </div>
            </div>

            <?=$data[COL_POSTCONTENT]?>
            <div class="small" style="text-align: right">
                <button class="btn btn-white btn-xs" type="button"><i class="fa fa-user"></i>&nbsp;&nbsp;<?=$data[COL_NAME]?></button>
                <button class="btn btn-white btn-xs" type="button"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;<?=date("d M Y H:i", strtotime($data[COL_CREATEDON]))?></button>
                <button class="btn btn-white btn-xs" type="button"><i class="fa fa-eye"></i>&nbsp;&nbsp;Viewed <b><?=number_format($data[COL_TOTALVIEW], 0)?></b> times</button>
            </div>
            <hr />
            <div id="disqus_thread"></div>
            <script>

                /**
                 *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                 *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                /*
                 */
                var disqus_config = function () {
                    this.page.url = '<?=site_url('post/view/'.$data[COL_POSTSLUG])?>';  // Replace PAGE_URL with your page's canonical URL variable
                    this.page.identifier = '<?=$data[COL_POSTSLUG]?>'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                };
                (function() { // DON'T EDIT BELOW THIS LINE
                    var d = document, s = d.createElement('script');
                    s.src = '<?=$this->setting_web_disqus_url?>';
                    s.setAttribute('data-timestamp', +new Date());
                    (d.head || d.body).appendChild(s);
                })();
            </script>
            <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
        </div>
    </section>

    <footer>
        <div class="container">
            <a href="<?=base_url()?>" class="logo"><img src="<?=base_url()?>assets/themes/bizpro/images/logo/logo.png" alt="Logo" style="width: 160px;"></a>

            <ul>
                <li><a href="#" class="tran3s round-border"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#" class="tran3s round-border"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="#" class="tran3s round-border"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                <li><a href="#" class="tran3s round-border"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
            </ul>
            <p>Copyright <?=date('Y')?> All rights reserved</p>
        </div>
    </footer>

    <div id="loader-wrapper">
        <div id="preloader_1">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>

    <!-- Scroll Top Button -->
    <button class="scroll-top tran3s p-color-bg">
        <i class="fa fa-long-arrow-up" aria-hidden="true"></i>
    </button>

    <div id="blueimp-gallery" class="blueimp-gallery">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>

    <!-- j Query -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/jquery.2.2.3.min.js"></script>

    <!-- Bootstrap JS -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/bootstrap/bootstrap.min.js"></script>

    <!-- revolution -->
    <script src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/jquery.themepunch.tools.min.js"></script>
    <script src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/revolution.extension.video.min.js"></script>

    <!-- owl.carousel -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/owl-carousel/owl.carousel.min.js"></script>
    <!-- mixitUp -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/jquery.mixitup.min.js"></script>
    <!-- Progress Bar js -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/skills-master/jquery.skills.js"></script>
    <!-- Validation -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/contact-form/validate.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/contact-form/jquery.form.js"></script>


    <!-- Theme js -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/js/theme.js"></script>

    <script src="<?=base_url()?>assets/js/blueimp/jquery.blueimp-gallery.min.js"></script>
</div> <!-- /.main-page-wrapper -->
</body>
</html>
