<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- For Resposive Device -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?=!empty($title) ? $title.' | '.$this->setting_web_name : $this->setting_web_name?></title>

    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="56x56" href="<?=base_url()?>assets/themes/bizpro/images/fav-icon/icon.png">
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />

    <!-- Main style sheet -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/bizpro/css/style.css">
    <!-- responsive style sheet -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/bizpro/css/responsive.css">
    <link href="<?=base_url()?>assets/css/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">


    <!-- Fix Internet Explorer ______________________________________-->

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="<?=base_url()?>assets/themes/bizpro/vendor/html5shiv.js"></script>
    <script src="<?=base_url()?>assets/themes/bizpro/vendor/respond.js"></script>
    <![endif]-->

</head>
<?php
$mimeImages = array(
  'image/png',
  'image/jpeg',
  'image/jpeg',
  'image/jpeg',
  'image/gif',
  'image/bmp',
  'image/tiff',
  'image/tiff',
  'image/svg+xml',
  'image/svg+xml'
)
?>
<body>
<div class="main-page-wrapper"  style="background-color: #f3f3f3">
  <header class="theme-main-header">
      <div class="container">
          <a href="<?=base_url()?>" class="logo float-left tran4s"><img src="<?=base_url()?>assets/themes/bizpro/images/logo/logo.png" alt="Logo" style="height: 48px"></a>
          <nav class="navbar float-right theme-main-menu one-page-menu">
              <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                      <span class="sr-only">Toggle navigation</span>
                      <!--Menu-->
                      <i class="fa fa-bars" aria-hidden="true"></i>
                  </button>
              </div>
              <div class="collapse navbar-collapse" id="navbar-collapse-1">
                  <ul class="nav navbar-nav">
                      <li class="active"><a href="<?=site_url()?>">HOME</a></li>
                      <li><a style="border: 1px solid #6c757d;padding: 5px 8px;border-radius: 10px; margin: 2px 5px" href="<?=site_url('user/login')?>"><i class="fad fa-sign-in"></i> LOGIN</a></li>
                  </ul>
              </div>
          </nav>
      </div>
  </header>

    <section class="inner-page-banner" style="background: url(<?=MY_ASSETURL."/themes/bizpro/images/slider/slide-4.jpg"?>) no-repeat center !important">
        <div class="opacity">
            <div class="container">
                <h2><?=$data[COL_POSTTITLE]?></h2>
                <ul>
                    <li><a href="<?=site_url()?>">Home</a></li>
                    <li>/</li>
                    <li><?=$data[COL_POSTTITLE]?></li>
                </ul>
            </div>
        </div>
    </section>

    <article class="blog-details-page" style="padding-bottom: 100px">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-fix">
                <div class="blog-details-post-wrapper">
                    <?php
                    $files = $this->db->where(COL_POSTID, $data[COL_POSTID])->get(TBL_POSTIMAGES)->result_array();
                    ?>
                    <div class="lightBoxGallery" style="width: 100%; text-align: center">
                        <?php
                        if(count($files) > 1) {
                            foreach($files as $f) {
                              $mimeType = mime_content_type(MY_UPLOADPATH.$f[COL_FILENAME]);
                              if(!in_array($mimeType, $mimeImages)) {
                                continue;
                              }
                                ?>
                                <a href="<?=MY_UPLOADURL.$f[COL_FILENAME]?>" title="<?=$data[COL_POSTTITLE]?>" data-gallery="">
                                    <img src="<?=MY_UPLOADURL.$f[COL_FILENAME]?>" style="max-width: 50vh; margin: 5px; box-shadow: 0 1px 1px rgba(0,0,0,0.2)">
                                </a>
                            <?php
                            }
                        } else if(!empty($files[0])) {
                          $mimeType = mime_content_type(MY_UPLOADPATH.$files[0][COL_FILENAME]);
                          if(in_array($mimeType, $mimeImages)) {
                            ?>
                            <a href="<?=MY_UPLOADURL.$files[0][COL_FILENAME]?>" title="<?=$data[COL_POSTTITLE]?>" data-gallery="">
                                <img src="<?=!empty($files[0][COL_FILENAME])?MY_UPLOADURL.$files[0][COL_FILENAME]:MY_NOIMAGEURL?>" alt="<?=$data[COL_POSTTITLE]?>" style="width: 100%; padding: 5px" />
                            </a>
                            <?php
                          }
                        }
                        ?>
                    </div>

                    <!--<div class="post-heading">
                        <h4><?=$data[COL_POSTTITLE]?></h4>
                        <span>Posted by <?=$data[COL_NAME]?> at <?=date("d M Y H:i", strtotime($data[COL_CREATEDON]))?></span>
                    </div>-->
                    <?=$data[COL_POSTCONTENT]?>

                    <!--<div class="clear-fix post-share-area" style="padding-bottom: 0px; margin-top: 20px; border-bottom: none; border-top: 1px solid #dfdfdf">
                        <ul class="share float-left">
                            <li>Posted by <b><?=$data[COL_NAME]?></b> at <b><?=date("d M Y H:i", strtotime($data[COL_CREATEDON]))?></b></li>
                        </ul>
                    </div>

                    <div class="post-comment" style="margin-top: 80px">
                        <div id="disqus_thread"></div>
                        <script>

                            /**
                             *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                             *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                            /*
                             */
                            var disqus_config = function () {
                                this.page.url = '<?=site_url('post/view/'.$data[COL_POSTSLUG])?>';  // Replace PAGE_URL with your page's canonical URL variable
                                this.page.identifier = '<?=$data[COL_POSTSLUG]?>'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                            };
                            (function() { // DON'T EDIT BELOW THIS LINE
                                var d = document, s = d.createElement('script');
                                s.src = '<?=$this->setting_web_disqus_url?>';
                                s.setAttribute('data-timestamp', +new Date());
                                (d.head || d.body).appendChild(s);
                            })();
                        </script>
                        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                    </div>-->
                    <?php
                    $files = $this->db->where(COL_POSTID, $data[COL_POSTID])->get(TBL_POSTIMAGES)->result_array();
                    ?>
                    <div>
                        <?php
                        if(count($files) > 1) {
                            foreach($files as $f) {
                              $mimeType = mime_content_type(MY_UPLOADPATH.$f[COL_FILENAME]);
                              if(in_array($mimeType, $mimeImages)) {
                                continue;
                              }
                                ?>
                                <a href="<?=MY_UPLOADURL.$f[COL_FILENAME]?>" target="_blank"><i class="fa fa-paperclip"></i>&nbsp;<?=$f[COL_FILENAME]?></a><br />
                            <?php
                            }
                        } else if(!empty($files[0])) {
                          $mimeType = mime_content_type(MY_UPLOADPATH.$files[0][COL_FILENAME]);
                          if(!in_array($mimeType, $mimeImages)) {
                            ?>
                            <a href="<?=MY_UPLOADURL.$files[0][COL_FILENAME]?>" target="_blank"><i class="fa fa-paperclip"></i>&nbsp;<?=$files[0][COL_FILENAME]?></a>
                            <?php
                          }
                        }
                        ?>
                    </div>
                </div>
            </div>

            <!--<div class="col-lg-4 col-md-4 col-sm-8 col-xs-12 p-fix">
                <aside>
                    <div class="sidebar-calendar">
                        <h6>Calendar</h6>
                        <div class="monthly" id="blog-calendar"></div>
                    </div>
                </aside>
            </div>-->
        </div>
    </article>
    <footer style="background-color: #fff">
        <div class="container">
            <a href="<?=base_url()?>" class="logo"><img src="<?=base_url()?>assets/themes/bizpro/images/logo/logo.png" alt="Logo" style="width: 160px;"></a>
            <ul>
                <!--<li><a href="#" class="tran3s round-border"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#" class="tran3s round-border"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>-->
                <li><a href="#" class="tran3s round-border"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                <!--<li><a href="#" class="tran3s round-border"><i class="fa fa-rss" aria-hidden="true"></i></a></li>-->
            </ul>
            <p>Copyright <?=date('Y')?> All rights reserved <!--| Strongly designed by <a style="color: #fff; font-weight: bold" href="https://www.linkedin.com/in/yoelrolas/" target="_blank">Partopi Tao</a>.--></p>
        </div>
    </footer>
    <div id="loader-wrapper">
        <div id="preloader_1">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>

    <button class="scroll-top tran3s p-color-bg">
        <i class="fa fa-long-arrow-up" aria-hidden="true"></i>
    </button>

    <!-- j Query -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/jquery.2.2.3.min.js"></script>

    <!-- Bootstrap JS -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/bootstrap/bootstrap.min.js"></script>

    <!-- Vendor js _________ -->

    <!-- owl.carousel -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/owl-carousel/owl.carousel.min.js"></script>
    <!-- mixitUp -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/jquery.mixitup.min.js"></script>
    <!-- Progress Bar js -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/skills-master/jquery.skills.js"></script>
    <!-- Validation -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/contact-form/validate.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/contact-form/jquery.form.js"></script>
    <!-- Calendar js -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/monthly-master/js/monthly.js"></script>


    <!-- Theme js -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/js/theme.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/js/map-script.js"></script>

</div> <!-- /.main-page-wrapper -->
</body>
</html>
