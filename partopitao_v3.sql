/*
SQLyog Community v12.09 (64 bit)
MySQL - 10.1.19-MariaDB : Database - db_partopitao_v3
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `postcategories` */

DROP TABLE IF EXISTS `postcategories`;

CREATE TABLE `postcategories` (
  `PostCategoryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryName` varchar(50) NOT NULL,
  `PostCategoryLabel` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`PostCategoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `postcategories` */

insert  into `postcategories`(`PostCategoryID`,`PostCategoryName`,`PostCategoryLabel`) values (1,'Berita','#f56954'),(2,'Artikel','#00a65a'),(3,'Kegiatan','#f39c12'),(4,'Galeri','#00c0ef'),(5,'Others','#3c8dbc');

/*Table structure for table `postimages` */

DROP TABLE IF EXISTS `postimages`;

CREATE TABLE `postimages` (
  `PostImageID` bigint(10) NOT NULL AUTO_INCREMENT,
  `PostID` bigint(10) NOT NULL,
  `FileName` varchar(250) NOT NULL,
  `Description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`PostImageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `postimages` */

/*Table structure for table `posts` */

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `PostID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryID` int(10) NOT NULL,
  `PostDate` date NOT NULL,
  `PostTitle` varchar(200) NOT NULL,
  `PostSlug` varchar(200) NOT NULL,
  `PostContent` longtext NOT NULL,
  `PostExpiredDate` date NOT NULL,
  `TotalView` int(11) NOT NULL DEFAULT '0',
  `LastViewDate` datetime DEFAULT NULL,
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '1',
  `FileName` varchar(250) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  PRIMARY KEY (`PostID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `posts` */

insert  into `posts`(`PostID`,`PostCategoryID`,`PostDate`,`PostTitle`,`PostSlug`,`PostContent`,`PostExpiredDate`,`TotalView`,`LastViewDate`,`IsSuspend`,`FileName`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (4,5,'2019-06-26','Hubungi Kami','hubungi-kami','<p><strong>Dinas Komunikasi Dan Informatika Kabupaten Humbang Hasundutan</strong></p>\r\n\r\n<p>Jl. SM. Raja Kompleks Perkantoran Tano Tubu, Doloksanggul 22457</p>\r\n\r\n<table border=\"0\" cellpadding=\"1\" cellspacing=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Telp</td>\r\n			<td>:</td>\r\n			<td>&nbsp;(0633) 31555</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Email&nbsp;</td>\r\n			<td>:</td>\r\n			<td colspan=\"4\">&nbsp;diskominfo@humbanghasundutankab.go.id</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<hr />\r\n<p>&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><iframe frameborder=\"0\" height=\"450\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3986.6996793830294!2d98.76777421426368!3d2.265541658581495!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x302e3cb7ff0bcc73%3A0x2c1a5b16f34531dc!2sDinas+Komunikasi+dan+Informatika+Kab.+Humbang+Hasundutan!5e0!3m2!1sen!2sid!4v1561433370666!5m2!1sen!2sid\" style=\"border:0\" width=\"600\"></iframe></p>\r\n','2020-12-31',10,NULL,0,NULL,'admin','2019-06-26 08:27:46','admin','2019-06-26 08:27:46'),(5,5,'2019-10-16','Rockstead Fixed Income Fund','rockstead-fixed-income-fund','<p style=\"text-align:justify\">Rockstead Fixed Income Fund is a hedge fund investment launched in 2016. Through its portfolio distribution between Bonds and FX (using a proprietary algorithm) , the Fund is able to offer investors an opportunity to earn superior returns. Investors can choose between a guaranteed fixed return (Share B) or a &lsquo;high-risk-high-return&rdquo; investment (Share A). The Fund obeys a monthly settlement regime. Investment monies, monitoring and reporting are outsourced to a 3rd party Administrator (Crowe).The Fund has been registered as a S13CA fund and its investment gains are tax-exempt. Investors should however obtain their personal tax advice with regard to their personal tax exposure from dividends arising from their investment with the Fund.</p>\r\n','2021-10-16',11,NULL,0,NULL,'admin','2019-10-16 09:14:16','admin','2019-10-16 09:18:16'),(6,5,'2019-10-16','1ROCKSTEAD GIP FUND II PTE LTD','1rockstead-gip-fund-ii-pte-ltd','<p style=\"text-align:justify\">1Rockstead GIP Fund II Pte. Ltd. is a approved GIP fund under the Global Investor Programme (GIP). For more details on GIP programme, please refer to&nbsp;<a href=\"https://www.contactsingapore.sg/gip\">http://www.contactsingapore.sg/gip</a>&nbsp;for latest updates. The Fund was launched on 1 September 2013 and is currently in the midst of fund raising. In the meantime, we have already began to make strategic investments in Singapore and Asia.</p>\r\n\r\n<p style=\"text-align:justify\">The Fund&rsquo;s main objective is to invest in a diversified portfolio of investments across various geographies and sectors. The Fund Manager aims to invest via structures that will facilitate the ease of exits and investor redemptions. The Fund will target to invest in sectors within the Target Markets which will provide opportunities for attractive risk-adjusted returns. As a directional guide, the Fund intends to invest in the following geographies and sectors via the following investment strategy:</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p><u><strong>By Geography:</strong></u></p>\r\n\r\n<p>The Fund plans to diversify its investments geographically by investing at least 50% of its AUM in Singapore, or companies with operations (or with intention to bring/transfer its operations) in Singapore, including 10% in Singapore&rsquo;s early stage / start ups, with the balance to be invested in companies in the below mentioned Target Markets.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>By Sector:</strong></u></p>\r\n\r\n<p>&ndash; Clean energy / Environmental</p>\r\n\r\n<p>&ndash; Consumer products &amp; services / Lifestyle</p>\r\n\r\n<p>&ndash; Healthcare / Medical / Education</p>\r\n','2020-10-16',4,NULL,0,NULL,'admin','2019-10-16 09:21:18','admin','2019-10-16 09:22:03'),(7,5,'2019-10-16','1ROCKSTEAD GIP FUND LTD','1rockstead-gip-fund-ltd','<p style=\"text-align:justify\">The Fund is a GIP approved fund under the Global Investor Programme (GIP). For more details on GIP programme, please refer to&nbsp;<a href=\"https://www.contactsingapore.sg/gip\">http://www.contactsingapore.sg/gip</a>&nbsp;for latest updates. We have fully deployed our proceeds into carefully selected target companies as per industries stated in our mandate. For latest update on the performance of the Fund, please contact us via our enquiry page.</p>\r\n','2020-10-16',3,NULL,0,NULL,'admin','2019-10-16 09:22:53','admin','2019-10-16 09:23:22'),(8,5,'2019-10-16','RS CLEAN RESOURCES FUND','rs-clean-resources-fund','<p>The Fund was ended in 2010. It focused on investments in the clean energy and China growth sectors.</p>\r\n','2020-10-16',3,NULL,0,NULL,'admin','2019-10-16 09:24:04','admin','2019-10-16 09:24:04');

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `RoleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(50) NOT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `roles` */

insert  into `roles`(`RoleID`,`RoleName`) values (1,'Administrator'),(2,'Project Manager'),(3,'Project Manager Officer'),(4,'Employee');

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `SettingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`SettingID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `settings` */

insert  into `settings`(`SettingID`,`SettingLabel`,`SettingName`,`SettingValue`) values (1,'SETTING_WEB_NAME','SETTING_WEB_NAME','Rockstead Capital'),(2,'SETTING_WEB_DESC','SETTING_WEB_DESC','Bridging Business, Connecting Capital'),(3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','https://general-9.disqus.com/embed.js'),(4,'SETTING_ORG_NAME','SETTING_ORG_NAME','Dinas Pertanian Kabupaten Humbang Hasundutan'),(5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','Jl. Sidikalang Km. 3.5 Simpang Sitapongan Desa Simangaronsang, Doloksanggul'),(6,'SETTING_ORG_LAT','SETTING_ORG_LAT',''),(7,'SETTING_ORG_LONG','SETTING_ORG_LONG',''),(8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','(0633) 31101, (0633) 31104'),(9,'SETTING_ORG_FAX','SETTING_ORG_FAX','(0633) 31103, (0633) 31103'),(10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','distan@humbanghasundutankab.go.id'),(11,'SETTING_WEB_API_FOOTERLINK','SETTING_WEB_API_FOOTERLINK','https://diskominfo.humbanghasundutankab.go.id/index.php/api/data_footer_link'),(12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','logo.png'),(13,'SETTING_WEB_SKIN_CLASS','SETTING_WEB_SKIN_CLASS','skin-green-light'),(14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','loader-128x/Ellipsis-3s-200px.gif'),(15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.0');

/*Table structure for table `userinformation` */

DROP TABLE IF EXISTS `userinformation`;

CREATE TABLE `userinformation` (
  `UserName` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `CompanyID` varchar(200) DEFAULT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `IdentityNo` varchar(50) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `ReligionID` int(10) DEFAULT NULL,
  `Gender` tinyint(1) DEFAULT NULL,
  `Address` text,
  `PhoneNumber` varchar(50) DEFAULT NULL,
  `EducationID` int(10) DEFAULT NULL,
  `UniversityName` varchar(50) DEFAULT NULL,
  `FacultyName` varchar(50) DEFAULT NULL,
  `MajorName` varchar(50) DEFAULT NULL,
  `IsGraduated` tinyint(1) NOT NULL DEFAULT '0',
  `GraduatedDate` date DEFAULT NULL,
  `YearOfExperience` int(10) DEFAULT NULL,
  `RecentPosition` varchar(250) DEFAULT NULL,
  `RecentSalary` double DEFAULT NULL,
  `ExpectedSalary` double DEFAULT NULL,
  `CVFilename` varchar(250) DEFAULT NULL,
  `ImageFilename` varchar(250) DEFAULT NULL,
  `RegisteredDate` date DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `userinformation` */

insert  into `userinformation`(`UserName`,`Email`,`CompanyID`,`Name`,`IdentityNo`,`BirthDate`,`ReligionID`,`Gender`,`Address`,`PhoneNumber`,`EducationID`,`UniversityName`,`FacultyName`,`MajorName`,`IsGraduated`,`GraduatedDate`,`YearOfExperience`,`RecentPosition`,`RecentSalary`,`ExpectedSalary`,`CVFilename`,`ImageFilename`,`RegisteredDate`) values ('admin','yoelrolas@gmail.com',NULL,'Administrator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-17');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(1) unsigned NOT NULL,
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`UserName`,`Password`,`RoleID`,`IsSuspend`,`LastLogin`,`LastLoginIP`) values ('admin','e10adc3949ba59abbe56e057f20f883e',1,0,'2020-02-09 11:57:35','::1');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
