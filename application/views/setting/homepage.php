<?php $this->load->view('header') ?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?= $title ?> <small> Form</small></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <?php
        if ($this->input->get('error') == 1) {
            ?>
          <div class="callout callout-danger">
            <span class="text-danger"><i class="fa fa-ban"></i>&nbsp;&nbsp;Data gagal disimpan, silahkan coba kembali.</span>
          </div>
          <?php
        }
        if (validation_errors()) {
            ?>
          <div class="callout callout-danger">
            <span class="text-danger"><i class="fa fa-ban"></i>&nbsp;&nbsp;<?=validation_errors()?></span>
          </div>
          <?php
        }
        if (isset($err)) {
            ?>
          <div class="callout callout-danger">
            <span class="text-danger"><i class="fa fa-ban"></i>&nbsp;&nbsp;<?=$err['code'].' : '.$err['message']?></span>
          </div>
          <?php
        }
        if (!empty($upload_errors)) {
            ?>
          <div class="callout callout-danger">
            <span class="text-danger"><?=$upload_errors?></span>
          </div>
          <?php
        }
        ?>
      </div>
      <div class="col-sm-12">
        <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
        <div class="card card-outline-primary">
          <div class="card-header">
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;SAVE SETTING</button>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label class="control-label">Video URL 1</label>
              <div class="col-sm-12">
                <input type="text" class="form-control" name="<?=SETTING_WEB_VIDEO1?>" value="<?=$this->setting_web_video1?>" placeholder="Website Name" required />
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">Video URL 2</label>
              <div class="col-sm-12">
                <input type="text" class="form-control" name="<?=SETTING_WEB_VIDEO2?>" value="<?=$this->setting_web_video2?>" placeholder="Website Name" required />
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">DISCLAIMER</label>
              <div class="col-sm-12">
                <textarea id="<?=SETTING_WEB_DISCLAIMER?>" name="<?=SETTING_WEB_DISCLAIMER?>"><?=GetSetting(SETTING_WEB_DISCLAIMER)?></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">OUR BELIEF</label>
              <div class="col-sm-12">
                <textarea id="<?=SETTING_WEB_SECT_OURBELIEF?>" name="<?=SETTING_WEB_SECT_OURBELIEF?>"><?=GetSetting(SETTING_WEB_SECT_OURBELIEF)?></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">OUR VALUES</label>
              <div class="col-sm-12">
                <textarea id="<?=SETTING_WEB_SECT_OURVALUE?>" name="<?=SETTING_WEB_SECT_OURVALUE?>"><?=GetSetting(SETTING_WEB_SECT_OURVALUE)?></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">OUR TEAM</label>
              <div class="col-sm-12">
                <textarea id="<?=SETTING_WEB_SECT_OURTEAM?>" name="<?=SETTING_WEB_SECT_OURTEAM?>"><?=GetSetting(SETTING_WEB_SECT_OURTEAM)?></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label">HOW WE WORK</label>
              <div class="col-sm-12">
                <textarea id="<?=SETTING_WEB_SECT_HOWWEWORK?>" name="<?=SETTING_WEB_SECT_HOWWEWORK?>"><?=GetSetting(SETTING_WEB_SECT_HOWWEWORK)?></textarea>
              </div>
            </div>
          </div>
        </div>
        <?=form_close()?>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('loadjs') ?>
<script>
$(document).ready(function() {
  CKEDITOR.config.height = 300;
  CKEDITOR.replace("<?=SETTING_WEB_DISCLAIMER?>");
  CKEDITOR.replace("<?=SETTING_WEB_SECT_OURBELIEF?>");
  CKEDITOR.replace("<?=SETTING_WEB_SECT_OURVALUE?>");
  CKEDITOR.replace("<?=SETTING_WEB_SECT_OURTEAM?>");
  CKEDITOR.replace("<?=SETTING_WEB_SECT_HOWWEWORK?>");
});
function getBase64(el, successCb, errCb) {
  var file =  document.querySelector(el).files[0];
  if(file) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      successCb(reader.result);
    };
    reader.onerror = function (error) {
      errCb();
    };
  }
}
</script>
<?php $this->load->view('footer') ?>
