<?php $this->load->view('header') ?>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><?= $title ?> <small> Form</small></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="breadcrumb-item"><a href="<?=site_url('user/index')?>"> Users</a></li>
                        <li class="breadcrumb-item active"><?=$edit?'Edit':'Add'?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card card-primary">
                        <div class="card-body">
                            <div style="display: none" class="alert alert-danger errorBox">
                                <i class="fa fa-ban"></i> Error :
                                <span class="errorMsg"></span>
                            </div>
                            <?php
                            if($this->input->get('error') == 1){
                                ?>
                                <div class="alert alert-danger alert-dismissible">
                                    <i class="fa fa-ban"></i>
                                    <span class="">Data gagal disimpan, silahkan coba kembali</span>
                                </div>
                            <?php
                            }
                            if(validation_errors()){
                                ?>
                                <div class="alert alert-danger alert-dismissible">
                                    <i class="fa fa-ban"></i>
                                    <?=validation_errors()?>
                                </div>
                            <?php
                            }
                            if(!empty($upload_errors)) {
                                ?>
                                <div class="alert alert-danger alert-dismissible">
                                    <i class="fa fa-ban"></i>
                                    <?=$upload_errors?>
                                </div>
                            <?php
                            }
                            ?>

                            <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'userForm','class'=>'form-horizontal'))?>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <label class="control-label col-sm-4">Username</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="<?=COL_USERNAME?>" value="<?= $edit ? $data[COL_USERNAME] : ""?>" <?=$edit?"disabled":""?> required>
                                        </div>
                                    </div>
                                    <?php
                                    if(!$edit) {
                                        ?>
                                        <div class="form-group row">
                                            <label class="control-label col-sm-4">Password</label>
                                            <div class="col-sm-7">
                                                <input type="password" class="form-control" name="<?=COL_PASSWORD?>" >
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label col-sm-4">Confirm Password</label>
                                            <div class="col-sm-7">
                                                <input type="password" class="form-control" name="RepeatPassword" >
                                            </div>
                                        </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <label class="control-label col-sm-3">Full Name</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="<?=COL_NAME?>" value="<?= $edit ? $data[COL_NAME] : ""?>" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-3">Email</label>
                                        <div class="col-sm-8">
                                            <input type="email" class="form-control" name="<?=COL_EMAIL?>" value="<?= $edit ? $data[COL_EMAIL] : ""?>" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-sm-3">Role</label>
                                        <div class="col-sm-8">
                                            <select name="<?=COL_ROLEID?>" class="form-control" required>
                                                <option value="">Select Role</option>
                                                <?=GetCombobox("SELECT * FROM roles", COL_ROLEID, COL_ROLENAME, (!empty($data[COL_ROLEID]) ? $data[COL_ROLEID] : null))?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                </div>
                            </div>
                            <?=form_close()?>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>

<?php $this->load->view('loadjs') ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $("[name^=CompanyID]").val(('<?=$edit?$data[COL_COMPANYID]:""?>').split(',')).trigger("change");
        });
        $("[name=RoleID]").change(function() {
            var role = $(this).val();
            if(role && role != <?=ROLEADMIN?>) {
                $(".row-emp").show().find("select").select2({ width: 'resolve', theme: 'bootstrap4' }).attr("disabled", false).attr("required", true);
            } else {
                $(".row-emp").hide();
            }
        }).trigger("change");
    </script>
<?php $this->load->view('footer') ?>