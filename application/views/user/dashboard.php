<?php
$this->load->view('header');
$ruser = GetLoggedUser();
?>
<style>
    .info-box-icon {
        height: 70px !important;
        line-height: 70px !important;
    }
    .info-box-icon-right {
        border-top-right-radius: .25rem;
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
        border-bottom-right-radius: .25rem;
        display: block;
        float: right;
        height: 70px !important;
        width: 24px;
        text-align: center;
        font-size: 12px;
        line-height: 70px;
        #background: rgba(0,0,0,0.1);
    }
    .info-box-icon-right:hover {
        background: rgba(0,0,0,0.15);
    }
    .info-box-icon-right>.small-box-footer {
        color: rgba(255,255,255,0.8);
    }
    .info-box-icon-right>.small-box-footer:hover {
        color: #fff;
    }
    .info-box .info-box-content {
        padding: 3px 10px !important;
    }
</style>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Dashboard</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fad fa-tachometer-alt"></i> Home</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-outline card-secondary">
          <div class="card-header">
            <h1 class="card-title">Main Setting</h1>
          </div>
          <?=form_open_multipart(site_url('setting/main'), array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12">
                <?php
                if ($this->input->get('error') == 1) {
                    ?>
                  <div class="callout callout-danger">
                    <span class="text-danger"><i class="fa fa-ban"></i>&nbsp;&nbsp;Data gagal disimpan, silahkan coba kembali.</span>
                  </div>
                  <?php
                }
                if (validation_errors()) {
                    ?>
                  <div class="callout callout-danger">
                    <span class="text-danger"><i class="fa fa-ban"></i>&nbsp;&nbsp;<?=validation_errors()?></span>
                  </div>
                  <?php
                }
                if (isset($err)) {
                    ?>
                  <div class="callout callout-danger">
                    <span class="text-danger"><i class="fa fa-ban"></i>&nbsp;&nbsp;<?=$err['code'].' : '.$err['message']?></span>
                  </div>
                  <?php
                }
                if (!empty($upload_errors)) {
                    ?>
                  <div class="callout callout-danger">
                    <span class="text-danger"><?=$upload_errors?></span>
                  </div>
                  <?php
                }
                ?>
              </div>
              <div class="col-sm-12">
                <div class="form-group row">
                  <label class="control-label col-sm-2">Website Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="<?=SETTING_WEB_NAME?>" value="<?=$this->setting_web_name?>" placeholder="Website Name" required />
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-sm-2">Website Description</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="<?=SETTING_WEB_DESC?>" value="<?=$this->setting_web_desc?>" placeholder="Website Name" required />
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-sm-2">Website Logo</label>
                  <div class="col-sm-2">
                    <img src="<?=MY_IMAGEURL.$this->setting_web_logo?>" style="height: 80px"/>
                  </div>
                  <div class="col-sm-4">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fad fa-image"></i></span>
                      </div>
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" name="logo" accept="image/*">
                        <label class="custom-file-label" for="userfile">Browse..</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-sm-2">About Text</label>
                  <div class="col-sm-10">
                    <textarea rows="5" name="<?=SETTING_WEB_ABOUT?>" class="form-control" placeholder="About"><?=GetSetting(SETTING_WEB_ABOUT)?></textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-sm-2">Email</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="<?=SETTING_ORG_MAIL?>" value="<?=GetSetting(SETTING_ORG_MAIL)?>" placeholder="Email" required />
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-sm-2">Phone No.</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="<?=SETTING_ORG_PHONE?>" value="<?=GetSetting(SETTING_ORG_PHONE)?>" placeholder="Phone No." required />
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-sm-2">Address</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="<?=SETTING_ORG_ADDRESS?>" value="<?=GetSetting(SETTING_ORG_ADDRESS)?>" placeholder="Address" required />
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-sm-2">Address Detail</label>
                  <div class="col-sm-10">
                    <textarea rows="5" name="<?=SETTING_ORG_ADDRESS_DETAIL?>" class="form-control" placeholder="Address Detail"><?=GetSetting(SETTING_ORG_ADDRESS_DETAIL)?></textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-sm-2">Address Map</label>
                  <div class="col-sm-10">
                    <textarea rows="5" name="<?=SETTING_ORG_ADDRESS_MAP?>" class="form-control" placeholder="Embeed Google Maps HTML (<iframe .... ></iframe>)"><?=GetSetting(SETTING_ORG_ADDRESS_MAP)?></textarea>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer text-center">
            <a href="<?=current_url()?>" class="btn btn-default"><i class="fas fa-refresh"></i>&nbsp;REVERT</a>
            <button type="submit" class="btn btn-primary"><i class="fad fa-save"></i>&nbsp;SAVE CHANGES</button>
          </div>
          <?=form_close()?>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('loadjs') ?>
<?php $this->load->view('footer') ?>
