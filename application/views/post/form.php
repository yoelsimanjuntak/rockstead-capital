<?php $this->load->view('header') ?>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><?= $title ?> <small> Form</small></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="breadcrumb-item"><a href="<?=site_url('post/index')?>"> Posts</a></li>
                        <li class="breadcrumb-item active"><?=$edit?'Edit':'Add'?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            <div clas="row">
                <div class="col-sm-12">
                    <div class="card card-default">
                        <div class="card-body">
                            <?php if(validation_errors()){ ?>
                                <div class="alert alert-danger alert-dismissible">
                                    <i class="fa fa-ban"></i>
                                    <?= validation_errors() ?>
                                </div>
                            <?php } ?>

                            <?php  if($this->input->get('success')){ ?>
                                <div class="form-group alert alert-success alert-dismissible">
                                    <i class="fa fa-check"></i>
                                    Update post berhasil.
                                </div>
                            <?php } ?>

                            <?php  if($this->input->get('error')){ ?>
                                <div class="form-group alert alert-danger alert-dismissible">
                                    <i class="fa fa-ban"></i>
                                    Gagal mengupdate post, silahkan coba kembali
                                </div>
                            <?php }
                            if(!empty($upload_errors)) {
                                ?>
                                <div class="alert alert-danger alert-dismissible">
                                    <i class="fa fa-ban"></i>
                                    <?=$upload_errors?>
                                </div>
                            <?php
                            }
                            ?>

                            <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'post'))?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="<?=COL_POSTTITLE?>" value="<?=!empty($data[COL_POSTTITLE]) ? $data[COL_POSTTITLE] : ''?>" placeholder="Post Title" required>
                                            <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fa fa-info"></i>
                                        </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <select name="<?=COL_POSTCATEGORYID?>" class="form-control" required>
                                                <option value="">Select Category</option>
                                                <?=GetCombobox("SELECT * FROM postcategories ORDER BY PostCategoryName", COL_POSTCATEGORYID, COL_POSTCATEGORYNAME, (!empty($data[COL_POSTCATEGORYID]) ? $data[COL_POSTCATEGORYID] : null))?>
                                            </select>
                                            <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fa fa-eyedropper"></i>
                                        </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                            </div>
                                            <input type="text" class="form-control datepicker" name="<?=COL_POSTEXPIREDDATE?>" value="<?=!empty($data[COL_POSTEXPIREDDATE]) ? date('Y-m-d', strtotime($data[COL_POSTEXPIREDDATE])) : ''?>" placeholder="Expired Date" required>
                                            <div class="input-group-append">
                                        <span class="input-group-text">
                                            <label class="mb-0"><input type="checkbox" name="<?=COL_ISSUSPEND?>" <?=!empty($data[COL_ISSUSPEND]) && $data[COL_ISSUSPEND]?"checked":""?> value="1" /> Suspend</label>
                                        </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <?php
                                        if(!empty($data[COL_POSTID])) {
                                            $files_ = $this->db->where(COL_POSTID, $data[COL_POSTID])->get(TBL_POSTIMAGES)->result_array();
                                            ?>
                                            <div style="margin: 5px 0px">
                                                <?php
                                                foreach($files_ as $f) {
                                                    ?>
                                                    <img src="<?=MY_UPLOADURL.$f[COL_FILENAME]?>" alt="<?=$f[COL_DESCRIPTION]?>" style="height: 20vh; border: 1.5px solid #dedede; box-shadow: 3px 4px 5px rgba(0,0,0,0.2); margin: 10px" />
                                                <?php
                                                }
                                                ?>
                                            </div>
                                            <br />
                                        <?php } ?>
                                        <label class="label-control">Image / Attachment (Optional, Multiple, Max 50MB)</label><br />
                                        <input type="file" name="userfile[]" accept="image/*, application/pdf" multiple />
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Content</label>
                                        <textarea id="content_editor" class="form-control" rows="4" placeholder="Post Content" name="<?=COL_POSTCONTENT?>"><?=!empty($data[COL_POSTCONTENT]) ? $data[COL_POSTCONTENT] : ''?></textarea>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat pull-right">Simpan</button>
                                    </div>
                                </div>
                                <?=form_close()?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">

                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs') ?>
    <script>
        $(document).ready(function() {
            $("[name=PostTitle]").change(function() {
                var title = $(this).val().toLowerCase();
                var slug = title.replace(/\s/g, "-");
                $("[name=PostSlug]").val(slug);
            }).trigger("change");

            CKEDITOR.config.height = 400;
            CKEDITOR.replace("content_editor");
        });
    </script>
<?php $this->load->view('footer') ?>
