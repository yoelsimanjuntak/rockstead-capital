<?php $data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d[COL_POSTID] . '" />',
        $d[COL_ISSUSPEND] ? '<small class="badge pull-left badge-danger">Suspend</small>' : (strtotime($d[COL_POSTEXPIREDDATE]) >= strtotime(date('Y-m-d')) ? '<small class="badge pull-left badge-success">Active</small>' : '<small class="badge pull-left badge-warning">Expired</small>'),
        anchor('post/edit/'.$d[COL_POSTID],$d[COL_POSTTITLE]),
        $d[COL_POSTCATEGORYNAME],
        '<a href="'.site_url('post/view/'.$d[COL_POSTSLUG]).'" target="_blank">'.site_url('post/view/'.$d[COL_POSTSLUG]).'</a>',
        date('d M Y', strtotime($d[COL_POSTDATE])),
        $d[COL_TOTALVIEW]
    );
    $i++;
}
$data = json_encode($res);
$user = GetLoggedUser();
?>

<?php $this->load->view('header')
?>
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><?= $title ?> <small>Data</small></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="breadcrumb-item active">Posts</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <p>
                        <?=anchor('post/delete','<i class="fa fa-trash-o"></i> Remove',array('class'=>'cekboxaction btn btn-danger btn-sm','confirm'=>'Apa anda yakin?'))?>
                        <?php if($user[COL_ROLEID] == ROLEADMIN) { ?>
                            <?=anchor('post/activate','<i class="fa fa-check"></i> Activate',array('class'=>'cekboxaction btn btn-success btn-sm','confirm'=>'Apa anda yakin?'))?>
                            <?=anchor('post/activate/1','<i class="fa fa-warning"></i> Suspend',array('class'=>'cekboxaction btn btn-warning btn-sm','confirm'=>'Apa anda yakin?'))?>
                        <?php } ?>
                        <?=anchor('post/add','<i class="fa fa-plus"></i> Create',array('class'=>'btn btn-primary btn-sm'))?>
                    </p>
                    <div class="card card-default">
                        <div class="card-body">
                            <form id="dataform" method="post" action="#">
                                <table id="datalist" class="table table-bordered table-hover">

                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

<?php $this->load->view('loadjs')?>
    <script type="text/javascript">
        $(document).ready(function() {
            var dataTable = $('#datalist').dataTable({
                //"sDom": "Rlfrtip",
                "aaData": <?=$data?>,
                //"bJQueryUI": true,
                //"aaSorting" : [[5,'desc']],
                "scrollY" : '40vh',
                "scrollX": "120%",
                "iDisplayLength": 100,
                "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
                "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
                "aoColumns": [
                    {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />",bSortable:false},
                    {"sTitle": "Status", "width": "10%"},
                    {"sTitle": "Title", "width": "25%"},
                    {"sTitle": "Category"},
                    {"sTitle": "URL", "width": "35%"},
                    {"sTitle": "Date", "width": "15%"},
                    {"sTitle": "Total View", "width": "10%"}
                ]
            });
            $('#cekbox').click(function(){
                if($(this).is(':checked')){
                    $('.cekbox').prop('checked',true);
                    console.log('clicked');
                }else{
                    $('.cekbox').prop('checked',false);
                }
            });
        });
    </script>

<?php $this->load->view('footer')
?>