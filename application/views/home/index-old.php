<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 6/19/2019
 * Time: 10:26 AM
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- For Resposive Device -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?=!empty($title) ? $title.' | '.$this->setting_web_name : $this->setting_web_name?></title>

    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="56x56" href="<?=base_url()?>assets/themes/bizpro/images/fav-icon/icon.png">

    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />

    <!-- Main style sheet -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/bizpro/css/style.css">
    <!-- responsive style sheet -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/bizpro/css/responsive.css">

    <!-- SLICK -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/bizpro/vendor/slick/slick/slick.css">

    <!-- Fix Internet Explorer ______________________________________-->

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="vendor/html5shiv.js"></script>
    <script src="vendor/respond.js"></script>
    <![endif]-->
<style>
    .blinking{
        animation: blinkingText 1s linear infinite;
    }
    @keyframes blinkingText{
        0%{opacity: 0;}
        50%{opacity: .5;}
        100%{opacity: 1;}
    }
    .tp-caption-sub {
        top: 300px !important;
    }

    .slick-slide, .slick-slide::before, .caption {
        -webkit-backface-visibility: hidden;
        backface-visibility: hidden;
    }
    .slick-slide::before {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    .main-slider {
        position: relative;
        width: 100%;
        height: 38vw;
        min-height: 8vw;
        margin-bottom: 50px;
        opacity: 0;
        visibility: hidden;
        -webkit-transition: all 1.2s ease;
        transition: all 1.2s ease;
    }
    .main-slider.slick-initialized {
        opacity: 1;
        visibility: visible;
    }

    .slick-slide {
        position: relative;
        height: 38vw;
        padding: 0px 400px;
    }
    .slick-slide::before {
        background-color: #000;
        opacity: .3;
        z-index: 1;
    }
    .slick-slide video {
        display: block;
        position: absolute;
        top: 50%;
        left: 50%;
        min-width: 100%;
        min-height: 100%;
        width: auto;
        height: auto;
        -webkit-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
    }
    .slick-slide iframe {
        position: relative;
        pointer-events: none;
    }
    .slick-slide figure {
        position: relative;
        height: 100%;
    }
    .slick-slide .slide-image {
        opacity: 0;
        height: 100%;
        background-size: cover;
        background-position: center;
        -webkit-transition: all .8s ease;
        transition: all .8s ease;
    }
    .slick-slide .slide-image.show {
        opacity: 1;
    }
    .slick-slide .image-entity {
        width: 100%;
        opacity: 0;
        visibility: hidden;
    }
    .slick-slide .loading {
        position: absolute;
        top: 44%;
        left: 0;
        width: 100%;
    }
    .slick-slide .slide-media {
        -webkit-animation: slideOut 0.4s cubic-bezier(0.4, 0.29, 0.01, 1);
        animation: slideOut 0.4s cubic-bezier(0.4, 0.29, 0.01, 1);
    }
    .slick-slide.slick-active {
        z-index: 1;
    }
    .slick-slide.slick-active .slide-media {
        -webkit-animation: slideIn 2.4s cubic-bezier(0.4, 0.29, 0.01, 1);
        animation: slideIn 2.4s cubic-bezier(0.4, 0.29, 0.01, 1);
    }
    .slick-slide.slick-active .caption {
        opacity: 1;
        -webkit-transform: translateY(0);
        transform: translateY(0);
        -webkit-transition: all 0.7s cubic-bezier(0.32, 0.34, 0, 1.62) 0.6s;
        transition: all 0.7s cubic-bezier(0.32, 0.34, 0, 1.62) 0.6s;
    }

    .caption {
        position: relative;
        top: 44%;
        left: 0;
        text-align: center;
        padding: 20px;
        border: 3px solid;
        color: #fff;
        margin: auto;
        font-size: 40px;
        font-weight: bold;
        letter-spacing: .02em;
        opacity: 0;
        z-index: 1;
        -webkit-transition: all .3s ease;
        transition: all .3s ease;
        -webkit-transform: translateY(100px);
        transform: translateY(100px);
        line-height: 1;
    }

    .slick-dots {
        text-align: center;
        padding: 15px 0px;
        background-color: #2d3042;
    }
    .slick-dots li {
        display: inline-block;
        vertical-align: top;
        margin: 0 8px;
    }
    .slick-dots li button {
        width: 16px;
        height: 16px;
        border: none;
        cursor: pointer;
        border-radius: 50%;
        border: 2px solid #fff;
        box-shadow: 0 0 0 0 transparent;
        vertical-align: middle;
        color: #fff;
        background-color: #fff;
        -webkit-transition: all .3s ease;
        transition: all .3s ease;
        opacity: .4;
        font-size: 2pt;
    }
    .slick-dots li button:focus {
        outline: none;
    }
    .slick-dots li button:hover {
        opacity: 1;
    }
    .slick-dots li.slick-active button {
        border-color: #2d3042;
        box-shadow: 0 0 0 2px #fff;
        opacity: 1;
    }

    @-webkit-keyframes slideIn {
        from {
            -webkit-filter: blur(15px);
            filter: blur(15px);
        }
        to {
            -webkit-filter: blur(0);
            filter: blur(0);
        }
    }

    @keyframes slideIn {
        from {
            -webkit-filter: blur(15px);
            filter: blur(15px);
        }
        to {
            -webkit-filter: blur(0);
            filter: blur(0);
        }
    }
    @-webkit-keyframes slideOut {
        from {
            -webkit-filter: blur(0);
            filter: blur(0);
        }
        to {
            -webkit-filter: blur(15px);
            filter: blur(15px);
        }
    }
    @keyframes slideOut {
        from {
            -webkit-filter: blur(0);
            filter: blur(0);
        }
        to {
            -webkit-filter: blur(15px);
            filter: blur(15px);
        }
    }
    @media (max-width: 430px) {
        .slick-slide {
            padding: 0px 10px !important;
        }
    }

    #pricing-section iframe {
      width: 100% !important;
    }
</style>
</head>
<body>
<div class="main-page-wrapper">
    <header class="theme-main-header">
        <div class="container">
            <a href="<?=base_url()?>" class="logo float-left tran4s"><img src="<?=base_url()?>assets/themes/bizpro/images/logo/logo.png" alt="Logo" style="height: 48px"></a>
            <nav class="navbar float-right theme-main-menu one-page-menu">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <!--Menu-->
                        <i class="fa fa-bars" aria-hidden="true"></i>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#home">HOME</a></li>
                        <li><a href="#about-us">ABOUT US</a></li>
                        <!--<li class="dropdown-holder">
                            <a href="#about-us">ABOUT US</a>
                            <ul class="sub-menu">
                                <li><a href="#about-us" class="tran3s">Rockstead Capital Group</a></li>
                                <li><a href="#service-section" class="tran3s">Our Values</a></li>
                                <li><a href="#team-section" class="tran3s">Our Team</a></li>
                            </ul>
                            <i class="fa fa-angle-down"></i>
                        </li>-->
                        <!--<li class="dropdown-holder">
                            <a href="#investment-approach">HOW WE WORK</a>
                            <ul class="sub-menu">
                                <li><a href="#investment-approach" class="tran3s">Our Promise</a></li>
                                <li><a href="#investment-approach" class="tran3s">Separation of Powers and Ownership</a></li>
                                <li><a href="#investment-approach" class="tran3s">Strategic Allocation</a></li>
                                <li><a href="#skill-section" class="tran3s">Tactical Allocation</a></li>
                                <li><a href="#skill-section" class="tran3s">Regular Reports and Revisions</a></li>
                            </ul>
                            <i class="fa fa-angle-down"></i>
                        </li>-->
                        <li><a href="#investment-approach">HOW WE WORK</a></li>
                        <!--<li><a href="#blog-section">NEWS</a></li>-->
                        <li><a style="border: 1px solid #d6973d;padding-right: 15px;border-radius: 10px;" href="#contact-section"><span>CONTACT</span></a></li>
                        <li><a style="border: 1px solid #6c757d;padding-right: 15px;border-radius: 10px; margin-left: 10px" href="<?=site_url('user/login')?>"><i class="fad fa-sign-in"></i> LOGIN</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>

    <!--<section id="home" class="main-slider">
        <div class="item video">
            <span class="loading">Loading...</span>
            <video class="slide-video slide-media" loop muted preload="metadata" poster="https://drive.google.com/uc?export=view&id=0B_koKn2rKOkLSXZCakVGZWhOV00">
                <source src="<?=base_url()?>assets/themes/bizpro/images/slider/drone-view-1080.mp4" type="video/mp4" />
            </video>
            <p class="caption"><span style="text-transform: uppercase"><?=$this->setting_web_name?></span><br /><span style="text-transform: uppercase; font-size: 12pt !important;"><?=$this->setting_web_desc?></span></p>
        </div>
        <div class="item video">
            <span class="loading">Loading...</span>
            <video class="slide-video slide-media" loop muted preload="metadata" poster="https://drive.google.com/uc?export=view&id=0B_koKn2rKOkLSXZCakVGZWhOV00">
                <source src="<?=base_url()?>assets/themes/bizpro/images/slider/drone-view-2-1080.mp4" type="video/mp4" />
            </video>
            <p class="caption"><span style="text-transform: uppercase"><?=$this->setting_web_name?></span><br /><span style="text-transform: uppercase; font-size: 12pt !important;"><?=$this->setting_web_desc?></span></p>
        </div>
    </section>-->

    <section id="about-us">
        <div class="container">
            <div class="theme-title">
                <h2>About Us</h2>
                <!--<p style="line-height: 44px; margin-bottom: 60px; margin-top: 100px">
                    Rockstead Capital Group is a leading Asian private equity and hedge fund firm.
                    Established in 1999, Rockstead Capital Group currently manages an investment portfolio in excess of US$100 million across multiple funds.
                    The Group employs a diverse range of investment and operational professionals who are seasoned investors in international markets as well as having in-depth local knowledge in countries that it invests in.
                    The Group’s primary investment objective is to achieve superior risk adjusted returns, while seeking to limit risk through sophisticated deal structuring, extensive due diligence and investing into growth companies with stable cashflow business models.
                    Rockstead Capital Group has a Registered Fund Management Companies ( RFMC ) license required by Monetary Authority of Singapore.
                </p>-->
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="single-about-content">
                        <p style="text-align: center">
                          <?=nl2br(htmlspecialchars(GetSetting(SETTING_WEB_ABOUT)))?>
                        </p>
                    </div>
                </div>
            </div>

            <div class="theme-title" style="margin-top: 10px">
                <h5>Our Belief</h5>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12" style="padding-top: 0px !important">
                  <div class="single-about-content">
                      <p style="text-align: center">
                        <?=nl2br(htmlspecialchars(GetSetting(SETTING_WEB_SECT_OURBELIEF)))?>
                      </p>
                  </div>
              </div>
            </div>

            <div class="theme-title" style="margin-top: 10px">
                <h5>Our Values</h5>
            </div>
            <div class="row" style="margin-bottom: 60px">
              <?=GetSetting(SETTING_WEB_SECT_OURVALUE)?>
            </div>

            <div class="theme-title" style="margin-top: 10px">
                <h5>Our Team</h5>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12" style="padding-top: 0px !important">
                  <div class="single-about-content" style="margin-bottom: 0px !important">
                      <p style="text-align: center">
                        Our investors are more than just clients. They are our core capital, our motivation for growth and preservation of our legacy. Our commitment to this philosophy shapes our investment ethos. With clients at the heart of our investment strategies, our product portfolio is governed by the primary need to deliver investment strategies that maximizes risk-adjusted returns tailored for each investor.<br>
                      </p>
                  </div>
              </div>
            </div>
        </div>
    </section>

    <!--<div id="service-section">
        <div class="container">
          <div class="theme-title" style="margin-top: 10px">
              <h5>Our Values</h5>
          </div>
          <div class="row">
              <div class="col-lg-3 col-md-3 col-sm-6">
                  <div class="single-service-content">
                      <div class="icon-heading">
                          <div class="icon"><i class="fa fa-balance-scale" style="background-color: white"></i></div>
                          <h6 style="font-size: 14.5pt"><a href="javacript:void(0)" style="cursor: default;">Independent Thought and Action</a></h6>
                      </div>
                      <p>
                          We source the most suitable products in the market for our clients and we think and act from their perspectives. We are not tied or obliged to any institution or partner.
                      </p>
                  </div>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-6">
                  <div class="single-service-content">
                      <div class="icon-heading tran3s">
                          <div class="icon tran3s"><i class="fa fa-users" style="background-color: white" aria-hidden="true"></i></div>
                          <h6 style="font-size: 14.5pt"><a href="javacript:void(0)" style="cursor: default;">Personal Attention</a></h6>
                      </div>
                      <p>
                          We bring the best of both worlds: services of a large firm, with the dedicated attention of a small firm.
                      </p>
                  </div>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-6">
                  <div class="single-service-content">
                      <div class="icon-heading tran3s">
                          <div class="icon tran3s"><i class="fa fa-thumbs-up" style="background-color: white" aria-hidden="true"></i></div>
                          <h6 style="font-size: 14.5pt"><a href="javacript:void(0)" style="cursor: default;">Partnership</a></h6>
                      </div>
                      <p>
                          We partner with our clients and business associates to minimize conflicts of interest and focus on the long-term.
                      </p>
                  </div>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-6">
                  <div class="single-service-content">
                      <div class="icon-heading tran3s">
                          <div class="icon tran3s"><i class="fa fa-history" style="background-color: white" aria-hidden="true"></i></div>
                          <h6 style="font-size: 14.5pt"><a href="javacript:void(0)" style="cursor: default;">Long-Term Relationships</a></h6>
                      </div>
                      <p>
                          We build long-term relationships with our clients, staff, and business associates.
                      </p>
                  </div>
              </div>
          </div>
        </div>
    </div>-->

    <!--<div id="team-section">
        <div class="container">
            <div class="theme-title" style="margin-top: 10px">
                <h5>Our Team</h5>
                <p>Our team has over 40 years of experience between them, in the Southeast Asian commercial and financial industry.</p>
            </div>

            <div class="clear-fix team-member-wrapper">
                <div class="float-left">
                    <div class="single-team-member">
                        <div class="img">
                            <img src="<?=MY_IMAGEURL?>user.jpg" alt="Our Team">
                            <div class="opacity tran4s">
                                <p>Integer sit amet aliquam neque, id fringilla quam. Nulla eget diam arcu. Phasellus porta in augue id sollicitudin. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nec dignissim felis. Curabitur aliquet, ex ac hendrerit volutpat, erat felis pretium arcu, eu egestas.</p>
                            </div>
                        </div>
                        <div class="member-name">
                            <h6>Lester Tay</h6>
                            <p>CEO</p>
                            <p>
                                <a href="#" class="more read-bio" onclick="javascript:return false">Read Bio</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="float-left">
                    <div class="single-team-member">
                        <div class="img">
                            <img src="<?=MY_IMAGEURL?>user.jpg" alt="Our Team">
                            <div class="opacity tran4s">
                                <p>Morbi auctor risus augue, ullamcorper sodales elit tristique eget. Mauris pulvinar libero at viverra pulvinar. Integer consectetur tortor a rutrum posuere. Morbi a mauris ultricies, vulputate nulla ac, consequat mi. In hac habitasse platea dictumst. Morbi et dapibus nulla. Vivamus porttitor feugiat.</p>
                            </div>
                        </div>
                        <div class="member-name">
                            <h6>Justinus Nurman</h6>
                            <p>COO</p>
                            <p>
                                <a href="#" class="more read-bio" onclick="javascript:return false">Read Bio</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="float-left">
                    <div class="single-team-member">
                        <div class="img">
                            <img src="<?=MY_IMAGEURL?>user.jpg" alt="Our Team">
                            <div class="opacity tran4s">
                                <p>Vivamus in mi tempus, facilisis lectus id, porttitor purus. Sed venenatis diam vitae est ultrices rutrum. Nunc est odio, sollicitudin in dictum imperdiet, pellentesque non enim. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec vel cursus est.</p>
                            </div>
                        </div>
                        <div class="member-name">
                            <h6>Audrey Koh</h6>
                            <p>CFO</p>
                            <p>
                                <a href="#" class="more read-bio" onclick="javascript:return false">Read Bio</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="float-left">
                    <div class="single-team-member">
                        <div class="img">
                            <img src="<?=MY_IMAGEURL?>user.jpg" alt="Our Team">
                            <div class="opacity tran4s">
                                <p>Nulla ut pulvinar metus, id ullamcorper purus. Cras ut dolor magna. Fusce commodo diam magna, sit amet vehicula mi feugiat at. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas eu nisi porttitor, lobortis leo sit amet.</p>
                            </div>
                        </div>
                        <div class="member-name">
                            <h6>Kevin Tan</h6>
                            <p>Representative</p>
                            <p>
                                <a href="#" class="more read-bio" onclick="javascript:return false">Read Bio</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>-->

    <div id="investment-approach" class="page-middle-banner" style="color: #fff !important;">
      <div class="opacity">
        <?=GetSetting(SETTING_WEB_SECT_HOWWEWORK)?>
      </div>
    </div>

    <!--<div id="skill-section" style="background: rgba(0,0,0,0.6); color: #fff !important;">
        <div class="container">
            <div class="theme-title" style="margin-top: 10px">
                <h5  style="color: #fff">Strategic and Tactical Allocation</h5>
                <p style="text-align: justify">
                    We search for and build funds with different mandates and subscription structures. We then allocate capital according to our client's investment horizon, risk appetite, and preferences. For in-house funds, we adjust our positions in asset classes and instruments based on mandate, attractiveness, suitability, macro conditions, and market timing. These include equities, bonds, currency pairs, and even other external funds.
                </p>
            </div>
            <div class="row" style="margin-top: 20px">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="single-service-content" style="min-height: 240px">
                        <div class="icon-heading" style="top: -5px !important;">
                            <div class="icon"><i class="fa fa-shield" style="background: rgb(102, 102, 102); color: #fff"></i></div>
                            <h6 style="font-size: 14.5pt"><a href="javacript:void(0)" style="cursor: default; color: #fff">Conservative</a></h6>
                        </div>
                        <p>
                            Invested in bonds, no lock-up, frequent dividends, stable moderate returns.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="single-service-content" style="min-height: 240px">
                        <div class="icon-heading" style="top: -5px !important;">
                            <div class="icon"><i class="fa fa-star" style="background: rgb(102, 102, 102); color: #fff"></i></div>
                            <h6 style="font-size: 14.5pt"><a href="javacript:void(0)" style="cursor: default; color: #fff">Aggressive</a></h6>
                        </div>
                        <p>
                            Traded in forex strategies, no lock-up, frequent dividends, risky high returns.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="single-service-content" style="min-height: 240px">
                        <div class="icon-heading" style="top: -5px !important;">
                            <div class="icon"><i class="fa fa-hourglass" style="background: rgb(102, 102, 102); color: #fff"></i></div>
                            <h6 style="font-size: 14.5pt"><a href="javacript:void(0)" style="cursor: default; color: #fff">Long-Horizon</a></h6>
                        </div>
                        <p>
                            Mixed allocation, long lock-up, occasional dividends, stable high returns
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>-->

    <!--<div id="project-section">
        <div class="container">
            <div class="theme-title" style="margin-bottom: 10px">
                <h2>Portfolio</h2>
                <p>What We Do</p>
            </div>

            <div class="project-gallery clear-fix">
                <div class="mix grid-item">
                    <div class="single-img">
                        <img src="<?=base_url()?>assets/themes/bizpro/images/project/1.jpg" alt="Image">
                        <div class="opacity">
                            <div class="border-shape">
                                <div>
                                    <div>
                                        <h6><a href="#">Energy and Environment</a></h6>
                                        <ul>
                                            <li>Asia Power Corp Ltd</li>
                                            <li>Asia Water Technology Ltd</li>
                                            <li>Devotion Energy Group Ltd</li>
                                            <li>China Northeast Petroleum Ltd</li>
                                            <li>London Asia Capital</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mix grid-item">
                    <div class="single-img">
                        <img src="<?=base_url()?>assets/themes/bizpro/images/project/2.jpg" alt="Image">
                        <div class="opacity">
                            <div class="border-shape">
                                <div>
                                    <div>
                                        <h6><a href="#">Technology</a></h6>
                                        <ul>
                                            <li>clickTRUE</li>
                                            <li>Global Voice Group</li>
                                            <li>Videocon Industries</li>
                                            <li>Nanjing Grid Power Automation</li>
                                            <li>Southern Cross Marine Supplies</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mix grid-item">
                    <div class="single-img">
                        <img src="<?=base_url()?>assets/themes/bizpro/images/project/3.jpg" alt="Image">
                        <div class="opacity">
                            <div class="border-shape">
                                <div>
                                    <div>
                                        <h6><a href="#">Consumer / Healthcare / Others</a></h6>
                                        <ul>
                                            <li>PT Cardig International</li>
                                            <li>PT Sona Topas Tbk</li>
                                            <li>Mayapada Hospital</li>
                                            <li>Chaswood Resources Sdn Bhd</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="partner-section" style="margin: 0px">
                <div class="container">
                    <div id="partner_logo" class="owl-carousel owl-theme">
                        <div class="item"><img src="<?=base_url()?>assets/themes/bizpro/images/logo/p1.png" alt="logo"></div>
                        <div class="item"><img src="<?=base_url()?>assets/themes/bizpro/images/logo/p2.png" alt="logo"></div>
                        <div class="item"><img src="<?=base_url()?>assets/themes/bizpro/images/logo/p3.png" alt="logo"></div>
                        <div class="item"><img src="<?=base_url()?>assets/themes/bizpro/images/logo/p4.png" alt="logo"></div>
                        <div class="item"><img src="<?=base_url()?>assets/themes/bizpro/images/logo/p5.png" alt="logo"></div>
                        <div class="item"><img src="<?=base_url()?>assets/themes/bizpro/images/logo/p2.png" alt="logo"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>-->


    <!--<div id="investment-approach" class="page-middle-banner">
        <div class="opacity">
            <h3>Investment <span class="p-color">Risk</span> Management</h3>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="single-about-content">
                            <div class="icon round-border tran3s">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </div>
                            <h5><a href="#" class="tran3s">Deal Sourcing</a></h5>
                            <ul style="list-style-type: square; text-align: left">
                                <li>Strong & Oustanding Management Team</li>
                                <li>Unique IP & Real Market Growth Potential</li>
                                <li>High VA Companies with Potential of Generating Long Term. Sustainable Returns</li>
                                <li>Favorable Industry Conditions</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="single-about-content">
                            <div class="icon round-border tran3s">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </div>
                            <h5><a href="#" class="tran3s">Preliminary Appraisal</a></h5>
                            <ul style="list-style-type: square; text-align: left">
                                <li>Value Proposition & Differentiation</li>
                                <li>Business Plan / Strategy</li>
                                <li>Financial Projections</li>
                                <li>Management Team</li>
                                <li>Exit Startegy</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="single-about-content">
                            <div class="icon round-border tran3s">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </div>
                            <h5><a href="#" class="tran3s">Due Intelligence</a></h5>
                            <ul style="list-style-type: square; text-align: left">
                                <li>Details Products & Services</li>
                                <li>CVs of Core Team, References, Interviews</li>
                                <li>Competitive Analysis</li>
                                <li>Market Trends</li>
                                <li>Financial / Legal / Operatioal Due Dilligence</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="single-about-content">
                            <div class="icon round-border tran3s">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </div>
                            <h5><a href="#" class="tran3s">Investment Comm / BOD</a></h5>
                            <ul style="list-style-type: square; text-align: left">
                                <li>Propose Investment for Recommendation to BOD</li>
                                <li>Monthly Updates to BOD</li>
                            </ul>
                            <h5><a href="#" class="tran3s">Post Investment Monitoring</a></h5>
                            <ul style="list-style-type: square; text-align: left">
                                <li>Board Seat</li>
                                <li>Periodic Financial Reports / Review</li>
                                <li>Sharing of Global Knowledge, Business Networks, Ideas, etc</li>
                                <li>Periodic Review of Investment Strategy</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>-->

    <!--<div id="team-section">
        <div class="container">
            <div class="theme-title">
                <h2>OUR TEAM</h2>
                <p>Our team has over 40 years of experience between them, in the Southeast Asian commercial and financial industry.</p>
            </div>

            <div class="client-slider">
                <div class="item">
                    <img src="<?=MY_IMAGEURL?>user.jpg" alt="Our Team" style="width: 200px">
                    <h6 style="margin-top: 10px;">- LESTER TAY -</h6>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    </p>
                </div>
                <div class="item">
                    <img src="<?=MY_IMAGEURL?>user.jpg" alt="Our Team" style="width: 200px">
                    <h6 style="margin-top: 10px;">- JUSTINUS NURMAN -</h6>
                    <p>
                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    </p>
                </div>
                <div class="item">
                    <img src="<?=MY_IMAGEURL?>user.jpg" alt="Our Team" style="width: 200px">
                    <h6 style="margin-top: 10px;">- AUDREY KOH -</h6>
                    <p>
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                    </p>
                </div>
                <div class="item">
                    <img src="<?=MY_IMAGEURL?>user.jpg" alt="Our Team" style="width: 200px">
                    <h6 style="margin-top: 10px;">- KEVIN TAN -</h6>
                    <p>
                        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </p>
                </div>
            </div>
        </div>
    </div>-->
    <div id="blog-section" style="display: none">
        <div class="container">
            <div class="theme-title">
                <h2>OUR LATEST INFO</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p>
            </div>
            <div class="clear-fix">
            <?php
            $news = $this->mpost->search(3,"",1);
            if(count($news) > 0) {
                foreach($news as $n) {
                    ?>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="single-news-item">
                            <div class="img" style="text-align: center">
                                <img src="<?=!empty($n[COL_FILENAME])?MY_UPLOADURL.$n[COL_FILENAME]:MY_IMAGEURL."no-image.png"?>" alt="News">
                                <a href="<?=site_url('post/view/'.$n[COL_POSTSLUG])?>" class="opacity tran4s"><i class="fa fa-link" aria-hidden="true"></i></a>
                            </div>

                            <div class="post">
                                <h6><a href="<?=site_url('post/view/'.$n[COL_POSTSLUG])?>" class="tran3s"><?=$n[COL_POSTTITLE]?></a></h6>
                                <a href="<?=site_url('post/view/'.$n[COL_POSTSLUG])?>">Posted by <span class="p-color"><?=$n[COL_NAME]?></span>  at <?=date('d M Y h:i', strtotime($n[COL_CREATEDON]))?></a>
                                <?php
                                $strippedcontent = strip_tags($n[COL_POSTCONTENT]);
                                ?>
                                <p><?=strlen($strippedcontent) > 200 ? substr($strippedcontent, 0, 200) . "..." : $strippedcontent ?></p>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
            </div>
        </div>
    </div>
    <div id="contact-section">
        <div class="container">
            <div class="clear-fix contact-address-content">
                <div class="col-md-12" style="text-align: center">
                    <div class="theme-title">
                        <h2>Contact Us</h2>
                    </div>
                </div>
                <div class="clearfix"></div><br />
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="left-side">
                        <p>We need time to understand you, and your financial needs and objectives. Please contact us, tell us your story, and we will make it worth your time.</p>
                        <ul>
                            <li>
                                <div class="icon tran3s round-border p-color-bg"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                                <!--<h6>Address</h6>-->
                                <p>
                                  <?=nl2br(htmlspecialchars(GetSetting(SETTING_ORG_ADDRESS_DETAIL)))?>
                                </p>
                                <!--<ul style="margin-top: 20px; margin-left:15px; list-style-type: square; text-align: justify">
                                    <li style="margin-bottom: 10px; padding-left: 5px">
                                        Rockstead Capital Group, Singapore
                                        138, Robinson Road #17-01
                                        Oxley Tower
                                        Singapore 068906
                                    </li>
                                    <li style="margin-bottom: 10px;padding-left: 5px">
                                        Rockstead Capital Group, Shenzhen
                                        3rd Fuhua Road, Room 3304
                                        Zuoyue Business District Centre (Excellence Commerce)
                                        Futian District, Shenzhen City
                                        China 510048
                                    </li>
                                </ul>-->
                            </li>
                            <li>
                                <div class="icon tran3s round-border p-color-bg"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                <!--<h6>Phone</h6>-->
                                <p>
                                  <?=nl2br(htmlspecialchars(GetSetting(SETTING_ORG_PHONE)))?>
                                    <!--Singapore : +65 6228 0348<br />
                                    China : +86 755 2391 0258-->
                                </p>
                            </li>
                            <li>
                                <div class="icon tran3s round-border p-color-bg"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                                <!--<h6>Email</h6>-->
                                <p><a href="mailto:fund.admin@rockstead.com" target="_top"><?=GetSetting(SETTING_ORG_MAIL)?></a></p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <div class="send-message">
                        <form action="<?=site_url('home/contact-form')?>" class="form-validation" autocomplete="off" method="post" novalidate="novalidate">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                                    <div class="single-input">
                                        <input type="text" placeholder="Name*" name="Fname">
                                    </div>
                                </div>
                                <!--<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="single-input">
                                        <input type="text" placeholder="Last Name*" name="Lname">
                                    </div>
                                </div>-->
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="single-input">
                                        <input type="email" placeholder="Email*" name="email">
                                    </div>
                                </div>
                            </div>
                            <div class="single-input">
                                <input type="text" placeholder="Phone" name="phone">
                            </div>
                            <div class="single-input">
                                <input type="text" placeholder="Subject" name="sub">
                            </div>
                            <textarea placeholder="Write Message" name="message"></textarea>
                            <button class="tran3s p-color-bg">Send Message</button>
                        </form>
                        <div class="alert-wrapper" id="alert-success">
                            <div id="success">
                                <button class="closeAlert"><i class="fa fa-times" aria-hidden="true"></i></button>
                                <div class="wrapper">
                                    <p>Your message was sent successfully.</p>
                                </div>
                            </div>
                        </div>
                        <div class="alert-wrapper" id="alert-error">
                            <div id="error">
                                <button class="closeAlert"><i class="fa fa-times" aria-hidden="true"></i></button>
                                <div class="wrapper">
                                    <p>Sorry!Something Went Wrong.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="pricing-section">
        <div class="container">
            <div class="clear-fix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="single-price-table hvr-float-shadow">
                        <strong class="color1"><?=GetSetting(SETTING_ORG_ADDRESS)?></strong>
                        <?=GetSetting(SETTING_ORG_ADDRESS_MAP)?>
                        <!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.8248289102908!2d103.84623091426556!3d1.2786513621574398!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da190ec81a4111%3A0x9cfea404c5d477c7!2sRockstead%20Capital%20Group!5e0!3m2!1sen!2sid!4v1571195981494!5m2!1sen!2sid" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen=""></iframe>-->
                        <p>
                          <?=nl2br(htmlspecialchars(GetSetting(SETTING_ORG_ADDRESS_DETAIL)))?>
                        </p>
                    </div>
                </div>
                <!--<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="single-price-table hvr-float-shadow">
                        <strong class="color2">Rockstead Capital Group, Shenzhen</strong>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14743.554980894532!2d114.044371!3d22.508357!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8e2da9e1bc241aae!2sExcellence!5e0!3m2!1sen!2sin!4v1571196281772!5m2!1sen!2sin" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                        <p>
                            3rd Fuhua Road, Room 3304<br />
                            Zuoyue Business District Centre (Excellence Commerce)<br />
                            Shenzhen City, China 510048<br />
                        </p>
                    </div>
                </div>-->
            </div>
        </div>
    </div>

    <footer style="background-color: #fff">
        <div class="container">
            <a href="<?=base_url()?>" class="logo"><img src="<?=base_url()?>assets/themes/bizpro/images/logo/logo.png" alt="Logo" style="width: 160px;"></a>
            <ul>
                <!--<li><a href="#" class="tran3s round-border"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#" class="tran3s round-border"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>-->
                <li><a href="#" class="tran3s round-border"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                <!--<li><a href="#" class="tran3s round-border"><i class="fa fa-rss" aria-hidden="true"></i></a></li>-->
            </ul>
            <p>Copyright <?=date('Y')?> All rights reserved <!--| Strongly designed by <a style="color: #fff; font-weight: bold" href="https://www.linkedin.com/in/yoelrolas/" target="_blank">Partopi Tao</a>.--></p>
        </div>
    </footer>

    <div id="loader-wrapper">
        <div id="preloader_1">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>

    <!-- Scroll Top Button -->
    <button class="scroll-top tran3s p-color-bg">
        <i class="fa fa-long-arrow-up" aria-hidden="true"></i>
    </button>

    <!-- j Query -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/jquery.2.2.3.min.js"></script>

    <!-- Bootstrap JS -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/bootstrap/bootstrap.min.js"></script>

    <!-- revolution -->
    <script src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/jquery.themepunch.tools.min.js"></script>
    <script src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/revolution.extension.video.min.js"></script>

    <!-- Google map js -->
    <!--<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZ8VrXgGZ3QSC-0XubNhuB2uKKCwqVaD0&callback=goMap" type="text/javascript"></script>
    <script src="<?=base_url()?>assets/themes/bizpro/vendor/gmaps.min.js"></script>-->
    <!-- owl.carousel -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/owl-carousel/owl.carousel.min.js"></script>
    <!-- mixitUp -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/jquery.mixitup.min.js"></script>
    <!-- Progress Bar js -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/skills-master/jquery.skills.js"></script>
    <!-- Validation -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/contact-form/validate.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/contact-form/jquery.form.js"></script>

    <!--SLICK-->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/slick/slick/slick.min.js"></script>


    <!-- Theme js -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/js/theme.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/js/map-script.js"></script>
    <script  type="text/javascript">
        $('.read-bio').click(function() {
            $(".single-team-member").removeClass("hover");
            $(this).closest(".single-team-member").addClass("hover");
        });
        var slideWrapper = $(".main-slider"),
            iframes = slideWrapper.find('.embed-player'),
            lazyImages = slideWrapper.find('.slide-image'),
            lazyCounter = 0;

        // When the slide is changing
        function playPauseVideo(slick, control){
            var currentSlide, slideType, startTime, player, video;

            currentSlide = slick.find(".slick-current");
            slideType = currentSlide.attr("class").split(" ")[1];
            player = currentSlide.find("iframe").get(0);
            startTime = currentSlide.data("video-start");

            video = currentSlide.children("video").get(0);
            if (video != null) {
                if (control === "play"){
                    video.play();
                } else {
                    video.pause();
                }
            }
        }

        // DOM Ready
        $(function() {
            // Initialize
            slideWrapper.on("init", function(slick){
                slick = $(slick.currentTarget);
                setTimeout(function(){
                    playPauseVideo(slick,"play");
                }, 1000);
                //resizePlayer(iframes, 16/9);
            });
            slideWrapper.on("beforeChange", function(event, slick) {
                slick = $(slick.$slider);
                playPauseVideo(slick,"pause");
            });
            slideWrapper.on("afterChange", function(event, slick) {
                slick = $(slick.$slider);
                playPauseVideo(slick,"play");
            });
            slideWrapper.on("lazyLoaded", function(event, slick, image, imageSource) {
                lazyCounter++;
                if (lazyCounter === lazyImages.length){
                    lazyImages.addClass('show');
                    // slideWrapper.slick("slickPlay");
                }
            });

            //start the slider
            slideWrapper.slick({
                // fade:true,
                autoplaySpeed:4000,
                lazyLoad:"progressive",
                speed:600,
                arrows:false,
                dots:true,
                cssEase:"cubic-bezier(0.87, 0.03, 0.41, 0.9)"
            });

            $('.main-slider, .slick-slide').css('height', $(window).height()-40 + 'px');
        });

        // Resize event
        $(window).on("resize.slickVideoPlayer", function(){
            //resizePlayer(iframes, 16/9);
        });
    </script>
</div> <!-- /.main-page-wrapper -->
</body>
</html>
