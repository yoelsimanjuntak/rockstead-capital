<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 6/19/2019
 * Time: 10:26 AM
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- For Resposive Device -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?=!empty($title) ? $title.' | '.$this->setting_web_name : $this->setting_web_name?></title>

    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="56x56" href="<?=base_url()?>assets/themes/bizpro/images/fav-icon/icon.png">

    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />

    <!-- Main style sheet -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/bizpro/css/style.css">
    <!-- responsive style sheet -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/bizpro/css/responsive.css">

    <!-- SLICK -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/bizpro/vendor/slick/slick/slick.css">

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/jquery.modal.min.css" />

    <!-- Fix Internet Explorer ______________________________________-->

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="vendor/html5shiv.js"></script>
    <script src="vendor/respond.js"></script>
    <![endif]-->
<style>
    body.modal-open {
        overflow: hidden !important;
    }
    .blinking{
        animation: blinkingText 1s linear infinite;
    }
    @keyframes blinkingText{
        0%{opacity: 0;}
        50%{opacity: .5;}
        100%{opacity: 1;}
    }
    .tp-caption-sub {
        top: 300px !important;
    }

    .slick-slide, .slick-slide::before, .caption {
        -webkit-backface-visibility: hidden;
        backface-visibility: hidden;
    }
    .slick-slide::before {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    .main-slider {
        position: relative;
        width: 100%;
        height: 38vw;
        min-height: 8vw;
        margin-bottom: 50px;
        opacity: 0;
        visibility: hidden;
        -webkit-transition: all 1.2s ease;
        transition: all 1.2s ease;
    }
    .main-slider.slick-initialized {
        opacity: 1;
        visibility: visible;
    }

    .slick-slide {
        position: relative;
        height: 38vw;
        padding: 0px 400px;
    }
    .slick-slide::before {
        background-color: #000;
        opacity: .3;
        z-index: 1;
    }
    .slick-slide video {
        display: block;
        position: absolute;
        top: 50%;
        left: 50%;
        min-width: 100%;
        min-height: 100%;
        width: auto;
        height: auto;
        -webkit-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
    }
    .slick-slide iframe {
        position: relative;
        pointer-events: none;
    }
    .slick-slide figure {
        position: relative;
        height: 100%;
    }
    .slick-slide .slide-image {
        opacity: 0;
        height: 100%;
        background-size: cover;
        background-position: center;
        -webkit-transition: all .8s ease;
        transition: all .8s ease;
    }
    .slick-slide .slide-image.show {
        opacity: 1;
    }
    .slick-slide .image-entity {
        width: 100%;
        opacity: 0;
        visibility: hidden;
    }
    .slick-slide .loading {
        position: absolute;
        top: 44%;
        left: 0;
        width: 100%;
    }
    .slick-slide .slide-media {
        -webkit-animation: slideOut 0.4s cubic-bezier(0.4, 0.29, 0.01, 1);
        animation: slideOut 0.4s cubic-bezier(0.4, 0.29, 0.01, 1);
    }
    .slick-slide.slick-active {
        z-index: 1;
    }
    .slick-slide.slick-active .slide-media {
        -webkit-animation: slideIn 2.4s cubic-bezier(0.4, 0.29, 0.01, 1);
        animation: slideIn 2.4s cubic-bezier(0.4, 0.29, 0.01, 1);
    }
    .slick-slide.slick-active .caption {
        opacity: 1;
        -webkit-transform: translateY(0);
        transform: translateY(0);
        -webkit-transition: all 0.7s cubic-bezier(0.32, 0.34, 0, 1.62) 0.6s;
        transition: all 0.7s cubic-bezier(0.32, 0.34, 0, 1.62) 0.6s;
    }

    .caption {
        position: relative;
        top: 44%;
        left: 0;
        text-align: center;
        padding: 20px;
        border: 3px solid;
        color: #fff;
        margin: auto;
        font-size: 40px;
        font-weight: bold;
        letter-spacing: .02em;
        opacity: 0;
        z-index: 1;
        -webkit-transition: all .3s ease;
        transition: all .3s ease;
        -webkit-transform: translateY(100px);
        transform: translateY(100px);
        line-height: 1;
    }

    .slick-dots {
        text-align: center;
        padding: 15px 0px;
        background-color: #2d3042;
    }
    .slick-dots li {
        display: inline-block;
        vertical-align: top;
        margin: 0 8px;
    }
    .slick-dots li button {
        width: 16px;
        height: 16px;
        border: none;
        cursor: pointer;
        border-radius: 50%;
        border: 2px solid #fff;
        box-shadow: 0 0 0 0 transparent;
        vertical-align: middle;
        color: #fff;
        background-color: #fff;
        -webkit-transition: all .3s ease;
        transition: all .3s ease;
        opacity: .4;
        font-size: 2pt;
    }
    .slick-dots li button:focus {
        outline: none;
    }
    .slick-dots li button:hover {
        opacity: 1;
    }
    .slick-dots li.slick-active button {
        border-color: #2d3042;
        box-shadow: 0 0 0 2px #fff;
        opacity: 1;
    }

    @-webkit-keyframes slideIn {
        from {
            -webkit-filter: blur(15px);
            filter: blur(15px);
        }
        to {
            -webkit-filter: blur(0);
            filter: blur(0);
        }
    }

    @keyframes slideIn {
        from {
            -webkit-filter: blur(15px);
            filter: blur(15px);
        }
        to {
            -webkit-filter: blur(0);
            filter: blur(0);
        }
    }
    @-webkit-keyframes slideOut {
        from {
            -webkit-filter: blur(0);
            filter: blur(0);
        }
        to {
            -webkit-filter: blur(15px);
            filter: blur(15px);
        }
    }
    @keyframes slideOut {
        from {
            -webkit-filter: blur(0);
            filter: blur(0);
        }
        to {
            -webkit-filter: blur(15px);
            filter: blur(15px);
        }
    }
    @media (max-width: 430px) {
        .slick-slide {
            padding: 0px 10px !important;
        }
    }

    #pricing-section iframe {
      width: 100% !important;
    }
    .blocker {
      z-index: 10000 !important;
    }

    #blog-section .more {
    width: 140px;
    line-height: 40px;
    text-align: center;
    color: #fff;
    text-transform: uppercase;
    border-width: 1px;
    border-color: /*#d73e4d;*/#d6a719;
    border-style: solid;
  }

  #blog-section .more:hover {
    background: #fff;
    color: #d6a719;
  }
</style>
</head>
<body>
<div class="main-page-wrapper">
    <header class="theme-main-header">
        <div class="container">
            <a href="<?=base_url()?>" class="logo float-left tran4s"><img src="<?=base_url()?>assets/themes/bizpro/images/logo/logo.png" alt="Logo" style="height: 48px"></a>
            <nav class="navbar float-right theme-main-menu one-page-menu">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <!--Menu-->
                        <i class="fa fa-bars" aria-hidden="true"></i>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#home">HOME</a></li>
                        <li><a href="#about-us">ABOUT US</a></li>
                        <!--<li class="dropdown-holder">
                            <a href="#about-us">ABOUT US</a>
                            <ul class="sub-menu">
                                <li><a href="#about-us" class="tran3s">Rockstead Capital Group</a></li>
                                <li><a href="#service-section" class="tran3s">Our Values</a></li>
                                <li><a href="#team-section" class="tran3s">Our Team</a></li>
                            </ul>
                            <i class="fa fa-angle-down"></i>
                        </li>-->
                        <!--<li class="dropdown-holder">
                            <a href="#investment-approach">HOW WE WORK</a>
                            <ul class="sub-menu">
                                <li><a href="#investment-approach" class="tran3s">Our Promise</a></li>
                                <li><a href="#investment-approach" class="tran3s">Separation of Powers and Ownership</a></li>
                                <li><a href="#investment-approach" class="tran3s">Strategic Allocation</a></li>
                                <li><a href="#skill-section" class="tran3s">Tactical Allocation</a></li>
                                <li><a href="#skill-section" class="tran3s">Regular Reports and Revisions</a></li>
                            </ul>
                            <i class="fa fa-angle-down"></i>
                        </li>-->
                        <li><a href="#investment-approach">HOW WE WORK</a></li>
                        <li><a href="#blog-section">NEWS</a></li>
                        <li><a style="border: 1px solid #d6973d;padding: 5px 8px;border-radius: 10px; margin: 2px 5px" href="#contact-section"><span>CONTACT</span></a></li>
                        <li><a style="border: 1px solid #6c757d;padding: 5px 8px;border-radius: 10px; margin: 2px 5px" href="<?=site_url('user/login')?>"><i class="fad fa-sign-in"></i> LOGIN</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>

    <section id="home" class="main-slider">
        <div class="item video">
            <span class="loading">Loading...</span>
            <video class="slide-video slide-media" loop muted preload="metadata" poster="https://drive.google.com/uc?export=view&id=0B_koKn2rKOkLSXZCakVGZWhOV00">
                <source src="<?=$this->setting_web_video1?>" type="video/mp4" />
            </video>
            <p class="caption"><span style="text-transform: uppercase"><?=$this->setting_web_name?></span><br /><span style="text-transform: uppercase; font-size: 12pt !important;"><?=$this->setting_web_desc?></span></p>
        </div>
        <div class="item video">
            <span class="loading">Loading...</span>
            <video class="slide-video slide-media" loop muted preload="metadata" poster="https://drive.google.com/uc?export=view&id=0B_koKn2rKOkLSXZCakVGZWhOV00">
                <source src="<?=$this->setting_web_video2?>" type="video/mp4" />
            </video>
            <p class="caption"><span style="text-transform: uppercase"><?=$this->setting_web_name?></span><br /><span style="text-transform: uppercase; font-size: 12pt !important;"><?=$this->setting_web_desc?></span></p>
        </div>
    </section>

    <section id="about-us">
        <div class="container">
            <div class="theme-title">
                <h2>About Us</h2>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="single-about-content">
                        <p style="text-align: center">
                          <?=nl2br(htmlspecialchars(GetSetting(SETTING_WEB_ABOUT)))?>
                        </p>
                    </div>
                </div>
            </div>

            <div class="theme-title" style="margin-top: 10px">
                <h5>Our Belief</h5>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12" style="padding-top: 0px !important">
                  <div class="single-about-content">
                      <p style="text-align: center">
                        <?=nl2br(htmlspecialchars(GetSetting(SETTING_WEB_SECT_OURBELIEF)))?>
                      </p>
                  </div>
              </div>
            </div>

            <div class="theme-title" style="margin-top: 10px">
                <h5>Our Values</h5>
            </div>
            <div class="row" style="margin-bottom: 60px">
              <?=GetSetting(SETTING_WEB_SECT_OURVALUE)?>
            </div>

            <div class="theme-title" style="margin-top: 10px">
                <h5>Our Team</h5>
            </div>
            <div class="row">
              <?=GetSetting(SETTING_WEB_SECT_OURTEAM)?>
            </div>
        </div>
    </section>

    <div id="investment-approach" class="page-middle-banner" style="color: #fff !important;">
      <div class="opacity">
        <?=GetSetting(SETTING_WEB_SECT_HOWWEWORK)?>
      </div>
    </div>
    <!--<div>
      <div class="theme-title" style="margin-top: 10px">
          <h2>News</h2>
      </div>
      <div class="row">
        <div class="container">
          <div class="news-events__event__list">
            <div class="news-events__events__list__item">
              <div class="news-events__events__list__item__inner">
                <div class="news-events__events__list__item__image">

                </div>
                <div class="news-events__events__list__item__content">
                  <a class="news-events__events__list__item__link" href="https://www.crowe.com/sg/news/temporary-closure-of-crowe-singapore-office">Temporary Closure of Crowe Singapore's Office</a>
                </div>
                <div class="news-events__news__list__item__date news-events__news__list__item__date--desktop">
                  21/04/2020
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>-->
    <?php
    $news = $this->mpost->search(6,"",1);
    if(count($news) > 0) {
      ?>
      <div id="blog-section">
          <div class="container">
              <div class="theme-title" style="margin-bottom: 20px !important">
                <h2>NEWS</h2>
              </div>
              <div class="clear-fix">
              <?php
              foreach($news as $n) {
                  ?>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      <div class="single-news-item" style="padding-bottom: 20px; border-bottom: 1px solid #f4f4f4; margin-top: 20px !important">
                          <!--<div class="img" style="text-align: center">
                              <img src="<?=!empty($n[COL_FILENAME])?MY_UPLOADURL.$n[COL_FILENAME]:MY_IMAGEURL."logo.png"?>" alt="News" style="margin: auto">
                              <a href="<?=site_url('post/view/'.$n[COL_POSTSLUG])?>" class="opacity tran4s"><i class="fa fa-link" aria-hidden="true"></i></a>
                          </div>-->

                          <div class="post">
                              <h6><a href="<?=site_url('post/view/'.$n[COL_POSTSLUG])?>" class="tran3s" style="color: #d6a83b !important"><?=$n[COL_POSTTITLE]?></a></h6>
                              <a href="<?=site_url('post/view/'.$n[COL_POSTSLUG])?>"><!--Posted by <span class="p-color"><?=$n[COL_NAME]?></span>  at --><?=date('d M Y', strtotime($n[COL_CREATEDON]))?></a>
                              <?php
                              $strippedcontent = strip_tags($n[COL_POSTCONTENT]);
                              ?>
                              <p><?=strlen($strippedcontent) > 200 ? substr($strippedcontent, 0, 200) . "..." : $strippedcontent ?></p>
                          </div>
                      </div>
                  </div>
                  <?php
              }
              ?>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px; text-align: center">
                <p>
                  <a href="<?=site_url('post/archive/1')?>" class="tran3s p-color-bg more" style="margin: auto !important">MORE</a>
                </p>
              </div>
            </div>
          </div>
      </div>
      <?php
    }
    ?>

    <div id="contact-section">
        <div class="container">
            <div class="clear-fix contact-address-content">
                <div class="col-md-12" style="text-align: center">
                    <div class="theme-title">
                        <h2>Contact Us</h2>
                    </div>
                </div>
                <div class="clearfix"></div><br />
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="left-side">
                        <p>We need time to understand you, and your financial needs and objectives. Please contact us, tell us your story, and we will make it worth your time.</p>
                        <ul>
                            <li>
                                <div class="icon tran3s round-border p-color-bg"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                                <p>
                                  <?=nl2br(htmlspecialchars(GetSetting(SETTING_ORG_ADDRESS_DETAIL)))?>
                                </p>
                            </li>
                            <li>
                                <div class="icon tran3s round-border p-color-bg"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                <p>
                                  <?=nl2br(htmlspecialchars(GetSetting(SETTING_ORG_PHONE)))?>
                                </p>
                            </li>
                            <li>
                                <div class="icon tran3s round-border p-color-bg"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                                <p><a href="mailto:fund.admin@rockstead.com" target="_top"><?=GetSetting(SETTING_ORG_MAIL)?></a></p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <div class="send-message">
                        <form action="<?=site_url('home/contact-form')?>" class="form-validation" autocomplete="off" method="post" novalidate="novalidate">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                                    <div class="single-input">
                                        <input type="text" placeholder="Name*" name="Fname">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="single-input">
                                        <input type="email" placeholder="Email*" name="email">
                                    </div>
                                </div>
                            </div>
                            <div class="single-input">
                                <input type="text" placeholder="Phone" name="phone">
                            </div>
                            <div class="single-input">
                                <input type="text" placeholder="Subject" name="sub">
                            </div>
                            <textarea placeholder="Write Message" name="message"></textarea>
                            <button class="tran3s p-color-bg">Send Message</button>
                        </form>
                        <div class="alert-wrapper" id="alert-success">
                            <div id="success">
                                <button class="closeAlert"><i class="fa fa-times" aria-hidden="true"></i></button>
                                <div class="wrapper">
                                    <p>Your message was sent successfully.</p>
                                </div>
                            </div>
                        </div>
                        <div class="alert-wrapper" id="alert-error">
                            <div id="error">
                                <button class="closeAlert"><i class="fa fa-times" aria-hidden="true"></i></button>
                                <div class="wrapper">
                                    <p>Sorry!Something Went Wrong.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="pricing-section">
        <div class="container">
            <div class="clear-fix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="single-price-table hvr-float-shadow">
                        <strong class="color1"><?=GetSetting(SETTING_ORG_ADDRESS)?></strong>
                        <?=GetSetting(SETTING_ORG_ADDRESS_MAP)?>
                        <p>
                          <?=nl2br(htmlspecialchars(GetSetting(SETTING_ORG_ADDRESS_DETAIL)))?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer style="background-color: #fff">
        <div class="container">
            <a href="<?=base_url()?>" class="logo"><img src="<?=base_url()?>assets/themes/bizpro/images/logo/logo.png" alt="Logo" style="width: 160px;"></a>
            <ul>
                <!--<li><a href="#" class="tran3s round-border"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#" class="tran3s round-border"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>-->
                <li><a href="#" class="tran3s round-border"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                <!--<li><a href="#" class="tran3s round-border"><i class="fa fa-rss" aria-hidden="true"></i></a></li>-->
            </ul>
            <p>Copyright <?=date('Y')?> All rights reserved <!--| Strongly designed by <a style="color: #fff; font-weight: bold" href="https://www.linkedin.com/in/yoelrolas/" target="_blank">Partopi Tao</a>.--></p>
        </div>
    </footer>

    <div id="loader-wrapper">
        <div id="preloader_1">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>

    <!-- Scroll Top Button -->
    <button class="scroll-top tran3s p-color-bg">
        <i class="fa fa-long-arrow-up" aria-hidden="true"></i>
    </button>
    <div id="disclaimer" class="modal" style="max-width: 80vw !important; max-height: 100%; overflow-y: scroll;">
      <div style="text-align: center; margin-bottom: 20px">
        <img style="height: 48px; display: initial" src="<?=base_url()?>assets/themes/bizpro/images/logo/logo.png" alt="Logo" />
      </div>
      <?=GetSetting(SETTING_WEB_DISCLAIMER)?>
      <div class="row" style="padding-bottom: 20px">
        <div class="col-sm-12">
          <p style="text-align: center">
            <a href="#" rel="modal:close" style="color: #d6a719; border: 2px solid #d6a719; font-weight: bold; padding: 5px 10px; margin: 2px">I ACCEPT</a>
            <a href="#" id="disclaimer-decline" style="color: #6f6f6f; border: 2px solid #6f6f6f; font-weight: bold; padding: 5px 10px; margin: 2px">I DECLINE</a>
          </p>
        </div>
      </div>

    </div>

    <!-- j Query -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/jquery.2.2.3.min.js"></script>

    <!-- Bootstrap JS -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/bootstrap/bootstrap.min.js"></script>

    <!-- revolution -->
    <script src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/jquery.themepunch.tools.min.js"></script>
    <script src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/revolution.extension.video.min.js"></script>

    <!-- Google map js -->
    <!--<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZ8VrXgGZ3QSC-0XubNhuB2uKKCwqVaD0&callback=goMap" type="text/javascript"></script>
    <script src="<?=base_url()?>assets/themes/bizpro/vendor/gmaps.min.js"></script>-->
    <!-- owl.carousel -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/owl-carousel/owl.carousel.min.js"></script>
    <!-- mixitUp -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/jquery.mixitup.min.js"></script>
    <!-- Progress Bar js -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/skills-master/jquery.skills.js"></script>
    <!-- Validation -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/contact-form/validate.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/contact-form/jquery.form.js"></script>

    <!--SLICK-->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/slick/slick/slick.min.js"></script>


    <!-- Theme js -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/js/theme.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/js/map-script.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.modal.min.js"></script>
    <script  type="text/javascript">
        $('.read-bio').click(function() {
            $(".single-team-member").removeClass("hover");
            $(this).closest(".single-team-member").addClass("hover");
        });
        var slideWrapper = $(".main-slider"),
            iframes = slideWrapper.find('.embed-player'),
            lazyImages = slideWrapper.find('.slide-image'),
            lazyCounter = 0;

        // When the slide is changing
        function playPauseVideo(slick, control){
            var currentSlide, slideType, startTime, player, video;

            currentSlide = slick.find(".slick-current");
            slideType = currentSlide.attr("class").split(" ")[1];
            player = currentSlide.find("iframe").get(0);
            startTime = currentSlide.data("video-start");

            video = currentSlide.children("video").get(0);
            if (video != null) {
                if (control === "play"){
                    video.play();
                } else {
                    video.pause();
                }
            }
        }

        function setCookie(cname, cvalue, exdays) {
          var d = new Date();
          d.setTime(d.getTime() + (exdays*24*60*60*1000));
          var expires = "expires="+ d.toUTCString();
          document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }

        function getCookie(cname) {
          var name = cname + "=";
          var decodedCookie = decodeURIComponent(document.cookie);
          var ca = decodedCookie.split(';');
          for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
              c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
              return c.substring(name.length, c.length);
            }
          }
          return "";
        }

        function checkCookie() {
          var cname = getCookie("disclaimerOFF");
          if (cname != "") {

          } else {
            setCookie("disclaimerOFF", "true", 2);

            $("body").addClass("modal-open");
            $(document).bind("contextmenu",function(e){
              return false;
            });
            $("#disclaimer").modal({
              escapeClose: false,
              clickClose: false,
              showClose: false,
              fadeDuration: 100,
              fadeDelay: 0.50
            });
          }
        }

        // DOM Ready
        $(function() {
            // Initialize
            slideWrapper.on("init", function(slick){
                slick = $(slick.currentTarget);
                setTimeout(function(){
                    playPauseVideo(slick,"play");
                }, 1000);
                //resizePlayer(iframes, 16/9);
            });
            slideWrapper.on("beforeChange", function(event, slick) {
                slick = $(slick.$slider);
                playPauseVideo(slick,"pause");
            });
            slideWrapper.on("afterChange", function(event, slick) {
                slick = $(slick.$slider);
                playPauseVideo(slick,"play");
            });
            slideWrapper.on("lazyLoaded", function(event, slick, image, imageSource) {
                lazyCounter++;
                if (lazyCounter === lazyImages.length){
                    lazyImages.addClass('show');
                    // slideWrapper.slick("slickPlay");
                }
            });

            //start the slider
            slideWrapper.slick({
                // fade:true,
                autoplaySpeed:4000,
                lazyLoad:"progressive",
                speed:600,
                arrows:false,
                dots:true,
                cssEase:"cubic-bezier(0.87, 0.03, 0.41, 0.9)"
            });

            $('.main-slider, .slick-slide').css('height', $(window).height()-40 + 'px');
        });

        // Resize event
        $(window).on("resize.slickVideoPlayer", function(){
            //resizePlayer(iframes, 16/9);
        });

        $(document).ready(function() {
          checkCookie();
          $('#disclaimer').on($.modal.CLOSE, function(event, modal) {
            $("body").removeClass("modal-open");
            $(document).unbind("contextmenu");
          });
          $('#disclaimer-decline', $('#disclaimer')).click(function() {
            var alrt = 'Thank you for visiting Rockstead Capital\n\nFor more information, please contact us at:\nT: <?=$this->setting_org_phone?>\nE: <?=$this->setting_org_mail?>\nA: Rockstead Capital Group, 138 Robinson Road, #17-01, Oxley Tower, Singapore 068906\n\nPlease close this window to exit.';
            alert(alrt);
            return false;
          });
        });
    </script>
</div> <!-- /.main-page-wrapper -->
</body>
</html>
