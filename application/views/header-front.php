<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 6/19/2019
 * Time: 10:26 AM
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- For Resposive Device -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?=!empty($title) ? $title.' | '.$this->setting_web_name : $this->setting_web_name?>AA</title>

    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="56x56" href="<?=base_url()?>assets/themes/bizpro/images/fav-icon/icon.png">


    <!-- Main style sheet -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/bizpro/css/style.css">
    <!-- responsive style sheet -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/bizpro/css/responsive.css">


    <!-- Fix Internet Explorer ______________________________________-->

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="vendor/html5shiv.js"></script>
    <script src="vendor/respond.js"></script>
    <![endif]-->
<style>

</style>
</head>
<body>
<div class="main-page-wrapper">
    <header class="theme-main-header">
        <div class="container">
            <a href="index.html" class="logo float-left tran4s"><img src="<?=base_url()?>assets/themes/bizpro/images/logo/logo.png" alt="Logo" style="height: 48px"></a>
            <nav class="navbar float-right theme-main-menu one-page-menu">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        Menu
                        <i class="fa fa-bars" aria-hidden="true"></i>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#home">HOME</a></li>
                        <li><a href="#about-us">ABOUT</a></li>
                        <li><a href="#service-section">FUNDS</a></li>
                        <li><a href="#project-section">PORTFOLIO</a></li>
                        <li><a href="#investment-approach">INVESTMENT APPROACH</a></li>
                        <li><a href="#team-section">TEAM</a></li>
                        <!--<li class="dropdown-holder"><a href="#blog-section">BLOG</a>
                            <ul class="sub-menu">
                                   <li><a href="blog-details.html" class="tran3s">Blog Details</a></li>
                               </ul>
                        </li>-->
                        <li><a href="#contact-section">CONTACT</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>

    <div id="home" class="banner">
        <div class="rev_slider_wrapper">
            <!-- START REVOLUTION SLIDER 5.0.7 auto mode -->
            <div id="main-banner-slider" class="rev_slider video-slider" data-version="5.0.7">
                <ul>
                    <!--<li data-index="rs-280" data-transition="fade" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default"  data-title="Title Goes Here" data-description="">
                        <img src="images/slider/slide-1.jpg"  alt="image" class="rev-slidebg" data-bgparallax="3" data-bgposition="center center" data-duration="20000" data-ease="Linear.easeNone" data-kenburns="on" data-no-retina="" data-offsetend="0 0" data-offsetstart="0 0" data-rotateend="0" data-rotatestart="0" data-scaleend="100" data-scalestart="140">
                        <div class="tp-caption"
                            data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                            data-y="['middle','middle','middle','middle']" data-voffset="['-58','-58','0','-50']"
                            data-width="none"
                            data-height="none"
                            data-whitespace="nowrap"
                            data-transform_idle="o:1;"

                            data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                            data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                            data-mask_in="x:0px;y:[100%];"
                            data-mask_out="x:inherit;y:inherit;"
                            data-start="1000"
                            data-splitin="none"
                            data-splitout="none"
                            data-responsive_offset="on"
                            style="z-index: 6; white-space: nowrap;">
                            <h1>HELLO WE'RE BizPro</h1>
                        </div>
                        <div class="tp-caption"
                            data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                            data-y="['middle','middle','middle','middle']" data-voffset="['-05','-05','63','0']"
                            data-width="none"
                            data-height="none"
                            data-whitespace="nowrap"
                            data-transform_idle="o:1;"

                            data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                            data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                            data-mask_in="x:0px;y:[100%];"
                            data-mask_out="x:inherit;y:inherit;"
                            data-start="2000"
                            data-splitin="none"
                            data-splitout="none"
                            data-responsive_offset="on"
                            style="z-index: 6; white-space: nowrap;">
                            <h6>Sub Head, Motto or Mission subtitle</h6>
                        </div>
                        <div class="tp-caption"
                            data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                            data-y="['middle','middle','middle','middle']" data-voffset="['52','52','125','80']"
                            data-transform_idle="o:1;"

                            data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                            data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                            data-mask_in="x:0px;y:[100%];"
                            data-mask_out="x:inherit;y:inherit;"
                            data-start="3000"
                            data-splitin="none"
                            data-splitout="none"
                            data-responsive_offset="on">
                            <a href="contact-us.html" class="project-button hvr-bounce-to-right">See Our Projects</a>
                        </div>
                    </li>-->
                    <li data-index="rs-20" data-transition="fade" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="<?=base_url()?>assets/themes/bizpro/images/slider/slide-1.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Rockstead Capital" data-description="">
                        <img src="<?=base_url()?>assets/themes/bizpro/images/slider/slide-1.jpg"  alt="Rockstead Capital" class="rev-slidebg" data-bgparallax="3" data-bgposition="center center" data-duration="20000" data-ease="Linear.easeNone" data-kenburns="on" data-no-retina="" data-offsetend="0 0" data-offsetstart="0 0" data-rotateend="0" data-rotatestart="0" data-scaleend="100" data-scalestart="140">
                        <div class="rs-background-video-layer"
                             data-forcerewind="on"
                             data-volume="mute"
                             data-videowidth="100%"
                             data-videoheight="100%"
                             data-videomp4="<?=base_url()?>assets/themes/bizpro/images/slider/video-2.mp4"
                             data-videopreload="preload"
                             data-videoloop="loopandnoslidestop"
                             data-forceCover="1"
                             data-aspectratio="16:9"
                             data-autoplay="true"
                             data-autoplayonlyfirsttime="false"
                            ></div>
                        <div class="tp-caption"
                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                             data-y="['middle','middle','middle','middle']" data-voffset="['-58','-33','-33','-100']"
                             data-width="none"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;"

                             data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                             data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                             data-mask_in="x:0px;y:[100%];"
                             data-mask_out="x:inherit;y:inherit;"
                             data-start="1000"
                             data-splitin="none"
                             data-splitout="none"
                             data-responsive_offset="on"
                             style="z-index: 6; white-space: nowrap;">
                            <h1>Rockstead Capital</h1>
                        </div>
                        <div class="tp-caption"
                             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                             data-y="['middle','middle','middle','middle']" data-voffset="['-05','93','93','20']"
                             data-width="none"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;"

                             data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                             data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                             data-mask_in="x:0px;y:[100%];"
                             data-mask_out="x:inherit;y:inherit;"
                             data-start="2000"
                             data-splitin="none"
                             data-splitout="none"
                             data-responsive_offset="on"
                             style="z-index: 6; white-space: nowrap;">
                            <h6>Bridging Business, Connecting Capital</h6>
                        </div>
                    </li>
                    <!--<li data-index="rs-18" data-transition="fade" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="images/slider/slide-3.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Title Goes Here" data-description="">
                        <img src="images/slider/slide-3.jpg"  alt="image" class="rev-slidebg" data-bgparallax="3" data-bgposition="center center" data-duration="20000" data-ease="Linear.easeNone" data-kenburns="on" data-no-retina="" data-offsetend="0 0" data-offsetstart="0 0" data-rotateend="0" data-rotatestart="0" data-scaleend="100" data-scalestart="140">
                        <div class="tp-caption"
                            data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                            data-y="['middle','middle','middle','middle']" data-voffset="['-58','-33','-33','-100']"
                            data-width="none"
                            data-height="none"
                            data-whitespace="nowrap"
                            data-transform_idle="o:1;"

                            data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                            data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                            data-mask_in="x:0px;y:[100%];"
                            data-mask_out="x:inherit;y:inherit;"
                            data-start="1000"
                            data-splitin="none"
                            data-splitout="none"
                            data-responsive_offset="on"
                            style="z-index: 6; white-space: nowrap;">
                            <h1>HELLO WE'RE BizPro</h1>
                        </div>
                        <div class="tp-caption"
                            data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                            data-y="['middle','middle','middle','middle']" data-voffset="['-05','93','93','20']"
                            data-width="none"
                            data-height="none"
                            data-whitespace="nowrap"
                            data-transform_idle="o:1;"

                            data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                            data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                            data-mask_in="x:0px;y:[100%];"
                            data-mask_out="x:inherit;y:inherit;"
                            data-start="2000"
                            data-splitin="none"
                            data-splitout="none"
                            data-responsive_offset="on"
                            style="z-index: 6; white-space: nowrap;">
                            <h6>Sub Head, Motto or Mission subtitle</h6>
                        </div>
                        <div class="tp-caption"
                            data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                            data-y="['middle','middle','middle','middle']" data-voffset="['52','185','185','105']"
                            data-transform_idle="o:1;"

                            data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                            data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                            data-mask_in="x:0px;y:[100%];"
                            data-mask_out="x:inherit;y:inherit;"
                            data-start="3000"
                            data-splitin="none"
                            data-splitout="none"
                            data-responsive_offset="on">
                            <a href="contact-us.html" class="project-button hvr-bounce-to-right">See Our Projects</a>
                        </div>
                    </li>-->
                </ul>
            </div>
        </div><!-- END REVOLUTION SLIDER -->
    </div>

    <section id="about-us">
        <div class="container">
            <div class="theme-title">
                <h2>Rockstead Capital Group</h2>
                <p style="line-height: 44px; margin-bottom: 160px; margin-top: 100px">
                    Rockstead Capital Group is a leading Asian private equity and hedge fund firm.
                    Established in 1999, Rockstead Capital Group currently manages an investment portfolio in excess of US$100 million across multiple funds.
                    The Group employs a diverse range of investment and operational professionals who are seasoned investors in international markets as well as having in-depth local knowledge in countries that it invests in.
                    The Group’s primary investment objective is to achieve superior risk adjusted returns, while seeking to limit risk through sophisticated deal structuring, extensive due diligence and investing into growth companies with stable cashflow business models.
                    Rockstead Capital Group has a Registered Fund Management Companies ( RFMC ) license required by Monetary Authority of Singapore.
                </p>
            </div>
        </div>
    </section>

    <div id="service-section">
        <div class="container">
            <div class="theme-title">
                <h2>Our Funds</h2>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="single-service-content">
                        <div class="icon-heading tran3s">
                            <div class="icon tran3s"><i class="fa fa-thumbs-up" aria-hidden="true"></i></div>
                            <h6><a href="#" class="tran3s">Rockstead Fix Income Fund</a></h6>
                        </div>
                        <p>Rockstead Fixed Income Fund is a hedge fund investment launched in 2016. Through its portfolio distribution between Bonds and FX…</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="single-service-content">
                        <div class="icon-heading tran3s">
                            <div class="icon tran3s"><i class="fa fa-money" aria-hidden="true"></i></div>
                            <h6><a href="#" class="tran3s">Rockstead GIP Fund</a></h6>
                        </div>
                        <p>The Fund is a GIP approved fund under the Global Investor Programme (GIP). For more details on GIP programme, please…</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="single-service-content">
                        <div class="icon-heading tran3s">
                            <div class="icon tran3s"><i class="fa fa-money" aria-hidden="true"></i></div>
                            <h6><a href="#" class="tran3s">Rockstead GIP Fund II</a></h6>
                        </div>
                        <p>The Fund is a GIP approved fund under the Global Investor Programme (GIP). For more details on GIP programme, please…</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="single-service-content">
                        <div class="icon-heading tran3s">
                            <div class="icon tran3s"><i class="fa fa-industry" aria-hidden="true"></i></div>
                            <h6><a href="#" class="tran3s">RS Clean Resources Fund</a></h6>
                        </div>
                        <p>The Fund is a GIP approved fund under the Global Investor Programme (GIP). For more details on GIP programme, please…</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="project-section">
        <div class="container">
            <div class="theme-title" style="margin-bottom: 10px">
                <h2>Portfolio</h2>
                <p>What We Do</p>
            </div>

            <div class="project-gallery clear-fix">
                <div class="mix grid-item">
                    <div class="single-img">
                        <img src="<?=base_url()?>assets/themes/bizpro/images/project/1.jpg" alt="Image">
                        <div class="opacity">
                            <div class="border-shape">
                                <div>
                                    <div>
                                        <h6><a href="#">Energy and Environment</a></h6>
                                        <ul>
                                            <li>Asia Power Corp Ltd</li>
                                            <li>Asia Water Technology Ltd</li>
                                            <li>Devotion Energy Group Ltd</li>
                                            <li>China Northeast Petroleum Ltd</li>
                                            <li>London Asia Capital</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mix grid-item">
                    <div class="single-img">
                        <img src="<?=base_url()?>assets/themes/bizpro/images/project/2.jpg" alt="Image">
                        <div class="opacity">
                            <div class="border-shape">
                                <div>
                                    <div>
                                        <h6><a href="#">Technology</a></h6>
                                        <ul>
                                            <li>clickTRUE</li>
                                            <li>Global Voice Group</li>
                                            <li>Videocon Industries</li>
                                            <li>Nanjing Grid Power Automation</li>
                                            <li>Southern Cross Marine Supplies</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mix grid-item">
                    <div class="single-img">
                        <img src="<?=base_url()?>assets/themes/bizpro/images/project/3.jpg" alt="Image">
                        <div class="opacity">
                            <div class="border-shape">
                                <div>
                                    <div>
                                        <h6><a href="#">Consumer / Healthcare / Others</a></h6>
                                        <ul>
                                            <li>PT Cardig International</li>
                                            <li>PT Sona Topas Tbk</li>
                                            <li>Mayapada Hospital</li>
                                            <li>Chaswood Resources Sdn Bhd</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="partner-section" style="margin: 0px">
                <div class="container">
                    <div id="partner_logo" class="owl-carousel owl-theme">
                        <div class="item"><img src="<?=base_url()?>assets/themes/bizpro/images/logo/p1.png" alt="logo"></div>
                        <div class="item"><img src="<?=base_url()?>assets/themes/bizpro/images/logo/p2.png" alt="logo"></div>
                        <div class="item"><img src="<?=base_url()?>assets/themes/bizpro/images/logo/p3.png" alt="logo"></div>
                        <div class="item"><img src="<?=base_url()?>assets/themes/bizpro/images/logo/p4.png" alt="logo"></div>
                        <div class="item"><img src="<?=base_url()?>assets/themes/bizpro/images/logo/p5.png" alt="logo"></div>
                        <div class="item"><img src="<?=base_url()?>assets/themes/bizpro/images/logo/p2.png" alt="logo"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="investment-approach" class="page-middle-banner">
        <div class="opacity">
            <h3>Investment <span class="p-color">Risk</span> Management</h3>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="single-about-content">
                            <div class="icon round-border tran3s">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </div>
                            <h5><a href="#" class="tran3s">Deal Sourcing</a></h5>
                            <ul style="list-style-type: square; text-align: left">
                                <li>Strong & Oustanding Management Team</li>
                                <li>Unique IP & Real Market Growth Potential</li>
                                <li>High VA Companies with Potential of Generating Long Term. Sustainable Returns</li>
                                <li>Favorable Industry Conditions</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="single-about-content">
                            <div class="icon round-border tran3s">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </div>
                            <h5><a href="#" class="tran3s">Preliminary Appraisal</a></h5>
                            <ul style="list-style-type: square; text-align: left">
                                <li>Value Proposition & Differentiation</li>
                                <li>Business Plan / Strategy</li>
                                <li>Financial Projections</li>
                                <li>Management Team</li>
                                <li>Exit Startegy</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="single-about-content">
                            <div class="icon round-border tran3s">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </div>
                            <h5><a href="#" class="tran3s">Due Intelligence</a></h5>
                            <ul style="list-style-type: square; text-align: left">
                                <li>Details Products & Services</li>
                                <li>CVs of Core Team, References, Interviews</li>
                                <li>Competitive Analysis</li>
                                <li>Market Trends</li>
                                <li>Financial / Legal / Operatioal Due Dilligence</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="single-about-content">
                            <div class="icon round-border tran3s">
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </div>
                            <h5><a href="#" class="tran3s">Investment Comm / BOD</a></h5>
                            <ul style="list-style-type: square; text-align: left">
                                <li>Propose Investment for Recommendation to BOD</li>
                                <li>Monthly Updates to BOD</li>
                            </ul>
                            <h5><a href="#" class="tran3s">Post Investment Monitoring</a></h5>
                            <ul style="list-style-type: square; text-align: left">
                                <li>Board Seat</li>
                                <li>Periodic Financial Reports / Review</li>
                                <li>Sharing of Global Knowledge, Business Networks, Ideas, etc</li>
                                <li>Periodic Review of Investment Strategy</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="team-section">
        <div class="container">
            <div class="theme-title">
                <h2>OUR TEAM</h2>
            </div>

            <div class="client-slider">
                <div class="item">
                    <img src="<?=base_url()?>assets/themes/bizpro/images/home/c1.jpg" alt="Our Team">
                    <h6 style="margin-top: 10px;">- VICTOR NG FOOK AI -</h6>
                    <p>
                        Mr. Ng Fook Ai Victor is the Chairman of the Fund. He is currently the founder and Executive Chairman of New Asia Assets Pte. Ltd, an investment company headquartered in Asia focusing on investments in Greater China.
                    </p>
                </div>
                <div class="item">
                    <img src="<?=base_url()?>assets/themes/bizpro/images/home/c2.jpg" alt="Our Team">
                    <h6 style="margin-top: 10px;">- RICHARD TAN TEW HAN -</h6>
                    <p>
                        Mr. Tan Tew Han, Richard served as an Executive Vice President of Investment Banking & Corporate Finance for Overseas Union Bank Ltd. (Overseas Union Securities Ltd), since 1993 until 2001. Mr. Tan holds a distinguished banking career spanning more than 25 years during which he held a number of senior posts in various foreign banks.
                    </p>
                </div>
                <div class="item">
                    <img src="<?=base_url()?>assets/themes/bizpro/images/home/c3.jpg" alt="Our Team">
                    <h6 style="margin-top: 10px;">- SUNNY SHA -</h6>
                    <p>
                        Mr. Sha has been involved in investments and fund management for nearly 20 years. He had sat on the board of several listed companies in Singapore and London. He is currently the Chairman of the Board of Directors at Asia Power Corporation Ltd (listed in Singapore), a company specialising in investments in power plants.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div id="contact-section">
        <div class="container">
            <div class="clear-fix contact-address-content">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="left-side">
                        <h2>Contact Info</h2>
                        <ul>
                            <li>
                                <div class="icon tran3s round-border p-color-bg"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                                <h6>Address</h6>
                                <ul style="margin-top: 20px; margin-left:15px; list-style-type: square; text-align: justify">
                                    <li style="margin-bottom: 10px; padding-left: 5px">
                                        Rockstead Capital Group, Singapore
                                        138, Robinson Road #17-01
                                        Oxley Tower
                                        Singapore 068906
                                    </li>
                                    <li style="margin-bottom: 10px;padding-left: 5px">
                                        Rockstead Capital Group, Shenzhen
                                        3rd Fuhua Road, Room 3304
                                        Zuoyue Business District Centre (Excellence Commerce)
                                        Futian District, Shenzhen City
                                        China 518048
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <div class="icon tran3s round-border p-color-bg"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                <h6>Phone</h6>
                                <p>
                                    Singapore : +65 6228 0348<br />
                                    China : +86 755 2391 0258
                                </p>
                            </li>
                            <li>
                                <div class="icon tran3s round-border p-color-bg"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                                <h6>Email</h6>
                                <p><a href="mailto:fund.admin@rockstead.com" target="_top">fund.admin@rockstead.com</a></p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="map-area">
                        <h2>Our Location</h2>
                        <div style="text-align: center">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.8248289102908!2d103.84623091426556!3d1.2786513621574398!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da190ec81a4111%3A0x9cfea404c5d477c7!2sRockstead%20Capital%20Group!5e0!3m2!1sen!2sid!4v1571195981494!5m2!1sen!2sid" width="360" height="400" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                            &nbsp;
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14743.554980894532!2d114.044371!3d22.508357!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8e2da9e1bc241aae!2sExcellence!5e0!3m2!1sen!2sin!4v1571196281772!5m2!1sen!2sin" width="360" height="400" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <div class="container">
            <a href="index.html" class="logo"><img src="<?=base_url()?>assets/themes/bizpro/images/logo/logo.png" alt="Logo" style="width: 160px;"></a>

            <ul>
                <li><a href="#" class="tran3s round-border"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#" class="tran3s round-border"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="#" class="tran3s round-border"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                <li><a href="#" class="tran3s round-border"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
            </ul>
            <p>Copyright @2019 All rights reserved | Strongly designed by <a style="color: #fff; font-weight: bold" href="https://www.linkedin.com/in/yoelrolas/" target="_blank">Partopi Tao</a>.</p>
        </div>
    </footer>

    <div id="loader-wrapper">
        <div id="preloader_1">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>

    <!-- Scroll Top Button -->
    <button class="scroll-top tran3s p-color-bg">
        <i class="fa fa-long-arrow-up" aria-hidden="true"></i>
    </button>

    <!-- Js File_________________________________ -->

    <!-- j Query -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/jquery.2.2.3.min.js"></script>

    <!-- Bootstrap JS -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/bootstrap/bootstrap.min.js"></script>

    <!-- Vendor js _________ -->

    <!-- revolution -->
    <script src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/jquery.themepunch.tools.min.js"></script>
    <script src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/revolution/revolution.extension.video.min.js"></script>

    <!-- Google map js -->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZ8VrXgGZ3QSC-0XubNhuB2uKKCwqVaD0&callback=goMap" type="text/javascript"></script> <!-- Gmap Helper -->
    <script src="<?=base_url()?>assets/themes/bizpro/vendor/gmaps.min.js"></script>
    <!-- owl.carousel -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/owl-carousel/owl.carousel.min.js"></script>
    <!-- mixitUp -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/jquery.mixitup.min.js"></script>
    <!-- Progress Bar js -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/skills-master/jquery.skills.js"></script>
    <!-- Validation -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/contact-form/validate.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/vendor/contact-form/jquery.form.js"></script>


    <!-- Theme js -->
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/js/theme.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/bizpro/js/map-script.js"></script>
</div> <!-- /.main-page-wrapper -->
</body>
</html>
