<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 04/10/2018
 * Time: 09:02
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends MY_Controller {
    function __construct() {
        parent::__construct();
        /*if(!IsLogin() || GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }*/
        $this->load->model('mpost');
        ini_set('upload_max_filesize', '50M');
        ini_set('post_max_size', '50M');
    }

    function index() {
        if(!IsLogin() || GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }
        $data['title'] = "Posts";
        $data['res'] = $this->mpost->getall();
        $this->load->view('post/index', $data);
    }

    function add() {
        if(!IsLogin() || GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }
        $user = GetLoggedUser();
        $data['title'] = "Post";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $data['data'] = $_POST;
            $rules = $this->mpost->rules();
            $this->form_validation->set_rules($rules);
            if($this->form_validation->run()){
                $id = GetLastID(TBL_POSTS, COL_POSTID) + 1;

                $config['upload_path'] = MY_UPLOADPATH;
                $config['allowed_types'] = "gif|jpg|jpeg|png|pdf";
                $config['max_size']	= 512000;
                $config['max_width']  = 4000;
                $config['max_height']  = 4000;
                $config['overwrite'] = FALSE;

                $this->load->library('upload',$config);
                $filesCount = count($_FILES['userfile']['name']);
                for($i = 0; $i < $filesCount; $i++) {
                    if(!empty($_FILES['userfile']['name'][$i])) {
                        $_FILES['file']['name'] = $_FILES['userfile']['name'][$i];
                        $_FILES['file']['type'] = $_FILES['userfile']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['userfile']['tmp_name'][$i];
                        $_FILES['file']['error'] = $_FILES['userfile']['error'][$i];
                        $_FILES['file']['size'] = $_FILES['userfile']['size'][$i];

                        // Upload file to server
                        if($this->upload->do_upload('file')){
                            // Uploaded file data
                            $fileData = $this->upload->data();
                            $uploadData[$i][COL_FILENAME] = $fileData['file_name'];
                        }
                        else {
                            $data['upload_errors'] = $this->upload->display_errors();
                            $this->load->view('post/form', $data);
                            return;
                        }
                    }
                }

                $data = array(
                    COL_POSTID => $id,
                    COL_POSTCATEGORYID => $this->input->post(COL_POSTCATEGORYID),
                    COL_POSTDATE => date('Y-m-d'),
                    COL_POSTTITLE => $this->input->post(COL_POSTTITLE),
                    COL_POSTSLUG => slugify($this->input->post(COL_POSTTITLE)),
                    COL_POSTCONTENT => $this->input->post(COL_POSTCONTENT),
                    COL_POSTEXPIREDDATE => date('Y-m-d', strtotime($this->input->post(COL_POSTEXPIREDDATE))),
                    COL_ISSUSPEND => ($user[COL_ROLEID] == ROLEADMIN ? ($this->input->post(COL_ISSUSPEND) ? $this->input->post(COL_ISSUSPEND) : false) : true),
                    COL_CREATEDBY => $user[COL_USERNAME],
                    COL_CREATEDON => date('Y-m-d H:i:s'),
                    COL_UPDATEDBY => $user[COL_USERNAME],
                    COL_UPDATEDON => date('Y-m-d H:i:s')
                );
                $res = $this->db->insert(TBL_POSTS, $data);
                if($res) {
                    if(!empty($uploadData)){
                        $id = $this->db->insert_id();
                        for($i = 0; $i < count($uploadData); $i++) {
                            $uploadData[$i][COL_POSTID] = $id;
                        }

                        $this->db->insert_batch(TBL_POSTIMAGES, $uploadData);
                    }
                    redirect('post/index');
                } else {
                    redirect(current_url()."?error=1");
                }
            }
            else {
                $this->load->view('post/form', $data);
            }
        }
        else {
            $this->load->view('post/form', $data);
        }
    }

    function edit($id) {
        if(!IsLogin() || GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }
        $user = GetLoggedUser();
        $data['title'] = "Post";
        $data['edit'] = TRUE;
        $data['data'] = $edited = $this->db->where(COL_POSTID, $id)->get(TBL_POSTS)->row_array();
        if(empty($edited)){
            show_404();
            return;
        }

        if(!empty($_POST)){
            $data['data'] = $_POST;
            $rules = $this->mpost->rules(false);
            $this->form_validation->set_rules($rules);
            if($this->form_validation->run()){
                $config['upload_path'] = MY_UPLOADPATH;
                $config['allowed_types'] = "gif|jpg|jpeg|png|pdf";
                $config['max_size']	= 512000;
                $config['max_width']  = 4000;
                $config['max_height']  = 4000;
                $config['overwrite'] = FALSE;

                $this->load->library('upload',$config);
                $filesCount = count($_FILES['userfile']['name']);
                $files = $this->db->where(COL_POSTID, $id)->get(TBL_POSTIMAGES)->result_array();
                if($filesCount > 0 && !empty($_FILES['userfile']['name'][0])) {
                    for($i = 0; $i < $filesCount; $i++) {
                        $_FILES['file']['name'] = $_FILES['userfile']['name'][$i];
                        $_FILES['file']['type'] = $_FILES['userfile']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['userfile']['tmp_name'][$i];
                        $_FILES['file']['error'] = $_FILES['userfile']['error'][$i];
                        $_FILES['file']['size'] = $_FILES['userfile']['size'][$i];

                        // Upload file to server
                        if($this->upload->do_upload('file')){
                            // Uploaded file data
                            $fileData = $this->upload->data();
                            $uploadData[$i][COL_FILENAME] = $fileData['file_name'];
                            $uploadData[$i][COL_POSTID] = $id;
                        }
                        else {
                            $data['upload_errors'] = $this->upload->display_errors();
                            $this->load->view('post/form', $data);
                            return;
                        }
                    }

                    foreach($files as $f) {
                        unlink(MY_UPLOADPATH.$f[COL_FILENAME]);
                    }
                }

                $data = array(
                    COL_POSTCATEGORYID => $this->input->post(COL_POSTCATEGORYID),
                    COL_POSTDATE => date('Y-m-d'),
                    COL_POSTTITLE => $this->input->post(COL_POSTTITLE),
                    //COL_POSTSLUG => $this->input->post(COL_POSTSLUG) ? $this->input->post(COL_POSTSLUG) : str_replace(" ", "-", strtolower($this->input->post(COL_POSTTITLE))),
                    COL_POSTSLUG => slugify($this->input->post(COL_POSTTITLE)),
                    COL_POSTCONTENT => $this->input->post(COL_POSTCONTENT),
                    COL_POSTEXPIREDDATE => date('Y-m-d', strtotime($this->input->post(COL_POSTEXPIREDDATE))),
                    COL_ISSUSPEND => ($user[COL_ROLEID] == ROLEADMIN ? ($this->input->post(COL_ISSUSPEND) ? $this->input->post(COL_ISSUSPEND) : false) : true),
                    COL_UPDATEDBY => $user[COL_USERNAME],
                    COL_UPDATEDON => date('Y-m-d H:i:s')
                );

                $reg = $this->db->where(COL_POSTID, $id)->update(TBL_POSTS, $data);
                if($reg) {
                    if(!empty($uploadData)){
                        $this->db->insert_batch(TBL_POSTIMAGES, $uploadData);
                        foreach($files as $f) {
                            $this->db->delete(TBL_POSTIMAGES, array(COL_POSTIMAGEID => $f[COL_POSTIMAGEID]));
                        }
                    }

                    redirect(site_url('post/index'));
                }
                else redirect(current_url().'?error=1');
            }
            else {
                $this->load->view('post/form', $data);
            }
        }
        else {
            $this->load->view('post/form', $data);
        }
    }

    function delete(){
        if(!IsLogin() || GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_POSTS, array(COL_POSTID => $datum));
            $deleted++;

            $files = $this->db->where(COL_POSTID, $datum)->get(TBL_POSTIMAGES)->result_array();
            foreach($files as $f) {
                unlink(MY_UPLOADPATH.$f[COL_FILENAME]);
                $this->db->delete(TBL_POSTIMAGES, array(COL_POSTIMAGEID => $f[COL_POSTIMAGEID]));
            }
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function activate($suspend=false) {
        if(!IsLogin() || GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        if(!IsLogin()) {
            ShowJsonError('Silahkan login terlebih dahulu');
            return;
        }
        $loginuser = GetLoggedUser();
        if(!$loginuser || $loginuser[COL_ROLEID] != ROLEADMIN) {
            ShowJsonError('Anda tidak memiliki akses terhadap modul ini.');
            return;
        }
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            if($this->db->where(COL_POSTID, $datum)->update(TBL_POSTS, array(COL_ISSUSPEND=>$suspend))) {
                $deleted++;
            }
        }
        if($deleted){
            ShowJsonSuccess($deleted." data diubah");
        }else{
            ShowJsonError("Tidak ada data yang diubah");
        }
    }

    function all($catid) {
        $rcat = $this->db->where(COL_POSTCATEGORYID, $catid)->get(TBL_POSTCATEGORIES)->row_array();
        if(!$rcat) {
            show_404();
            return false;
        }
        $data['title'] = $rcat[COL_POSTCATEGORYNAME];
        /*$data['news'] = $this->mpost->search(10,"",POSTCATEGORY_NEWS);
        $data['blogs'] = $this->mpost->search(10,"",POSTCATEGORY_BLOG);
        $data['events'] = $this->mpost->search(10,"",POSTCATEGORY_EVENT);*/
        $data['data'] = $this->mpost->search(0,"",$rcat[COL_POSTCATEGORYID]);
        $this->load->view('post/all', $data);
    }

    function view($slug) {
        $this->db->join(TBL_POSTCATEGORIES,TBL_POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL_POSTS.".".COL_POSTCATEGORYID,"inner");
        $this->db->join(TBL_USERINFORMATION,TBL_USERINFORMATION.'.'.COL_USERNAME." = ".TBL_POSTS.".".COL_CREATEDBY,"inner");
        $this->db->where("(".TBL_POSTS.".".COL_POSTSLUG." = '".$slug."' OR ".TBL_POSTS.".".COL_POSTID." = '".$slug."')");
        $rpost = $this->db->get(TBL_POSTS)->row_array();
        if(!$rpost) {
            show_404();
            return false;
        }

        $this->db->where(COL_POSTID, $rpost[COL_POSTID]);
        $this->db->set(COL_TOTALVIEW, COL_TOTALVIEW."+1", FALSE);
        $this->db->update(TBL_POSTS);

        $data['title'] = $rpost[COL_POSTTITLE];
        $data['data'] = $rpost;

        $this->load->view('post/view', $data);
    }

    function view_partial($slug) {
        $this->db->join(TBL_POSTCATEGORIES,TBL_POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL_POSTS.".".COL_POSTCATEGORYID,"inner");
        $this->db->join(TBL_USERINFORMATION,TBL_USERINFORMATION.'.'.COL_USERNAME." = ".TBL_POSTS.".".COL_CREATEDBY,"inner");
        $this->db->where("(".TBL_POSTS.".".COL_POSTSLUG." = '".$slug."' OR ".TBL_POSTS.".".COL_POSTID." = '".$slug."')");
        $rpost = $this->db->get(TBL_POSTS)->row_array();
        if(!$rpost) {
            show_404();
            return false;
        }

        $this->db->where(COL_POSTID, $rpost[COL_POSTID]);
        $this->db->set(COL_TOTALVIEW, COL_TOTALVIEW."+1", FALSE);
        $this->db->update(TBL_POSTS);

        $data['title'] = $rpost[COL_POSTTITLE];
        $data['data'] = $rpost;

        $this->load->view('post/view_partial', $data);
    }

    function search() {
        $data['title'] = "Posts";
        $cat = $this->input->post(COL_POSTCATEGORYID);
        $keyword = $this->input->post("Keyword");
        $data['data'] = $this->mpost->search(0,$keyword,$cat);
        $data['datapost'] = array(
            "Keyword" => $keyword,
            COL_POSTCATEGORYID => $cat
        );
        $this->load->view('post/all', $data);
    }

    function archive() {
        $data['title'] = "News";
        $cat = $this->input->post(COL_POSTCATEGORYID);
        $keyword = $this->input->post("Keyword");
        //$data['data'] = $this->mpost->search(0,$keyword,$cat);
        $data['res'] = $this->mpost->search(0,$keyword,1);
        $data['datapost'] = array(
            "Keyword" => $keyword,
            COL_POSTCATEGORYID => $cat
        );
        $this->load->view('post/archive', $data);
    }

    function gallery() {
        $data['title'] = "Galeri";
        $cat = $this->input->post(COL_POSTCATEGORYID);
        $keyword = $this->input->post("Keyword");
        //$data['data'] = $this->mpost->search(0,$keyword,$cat);
        $data['res'] = $this->mpost->search(0,$keyword,4);
        $data['datapost'] = array(
            "Keyword" => $keyword,
            COL_POSTCATEGORYID => $cat
        );
        $this->load->view('post/archive', $data);
    }

    function event() {
        $data['title'] = "Kegiatan";
        $cat = $this->input->post(COL_POSTCATEGORYID);
        $keyword = $this->input->post("Keyword");
        //$data['data'] = $this->mpost->search(0,$keyword,$cat);
        $data['data'] = $this->mpost->search(0,$keyword,3);
        $data['datapost'] = array(
            "Keyword" => $keyword,
            COL_POSTCATEGORYID => $cat
        );
        $this->load->view('post/gallery', $data);
    }

    public function page($id) {
        $this->load->view('post/custompage', array('id'=>$id));
    }
}
