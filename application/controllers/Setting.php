<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 20/07/2019
 * Time: 09:18
 */
class Setting extends MY_Controller {

    function __construct() {
        parent::__construct();
        if(!IsLogin() || GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }
    }

    function Common() {
        $user = GetLoggedUser();
        $data['title'] = 'Pengaturan Umum';
        if(!empty($_POST)) {
            $data['data'] = $_POST;
            SetSetting(SETTING_ORG_NAME, $this->input->post(SETTING_ORG_NAME));
            SetSetting(SETTING_ORG_ADDRESS, $this->input->post(SETTING_ORG_ADDRESS));
            SetSetting(SETTING_ORG_PHONE, $this->input->post(SETTING_ORG_PHONE));
            SetSetting(SETTING_ORG_FAX, $this->input->post(SETTING_ORG_FAX));
            SetSetting(SETTING_ORG_MAIL, $this->input->post(SETTING_ORG_MAIL));
            redirect('setting/common');

        }else {
            $this->load->view('setting/common', $data);
        }
    }

    function web() {
        $user = GetLoggedUser();
        $data['title'] = 'Pengaturan Web';
        if(!empty($_POST)) {
            $data['data'] = $_POST;
            SetSetting(SETTING_WEB_NAME, $this->input->post(SETTING_WEB_NAME));
            SetSetting(SETTING_WEB_DESC, $this->input->post(SETTING_WEB_DESC));
            SetSetting(SETTING_WEB_DISQUS_URL, $this->input->post(SETTING_WEB_DISQUS_URL));
            SetSetting(SETTING_WEB_API_FOOTERLINK, $this->input->post(SETTING_WEB_API_FOOTERLINK));
            SetSetting(SETTING_WEB_LOGO, $this->input->post(SETTING_WEB_LOGO));
            SetSetting(SETTING_WEB_SKIN_CLASS, $this->input->post(SETTING_WEB_SKIN_CLASS));
            SetSetting(SETTING_WEB_PRELOADER, $this->input->post(SETTING_WEB_PRELOADER));
            SetSetting(SETTING_WEB_VERSION, $this->input->post(SETTING_WEB_VERSION));
            redirect('setting/web');

        }else {
            $this->load->view('setting/web', $data);
        }
    }

    public function main() {
      if(!empty($_POST)) {
          SetSetting(SETTING_WEB_NAME, $this->input->post(SETTING_WEB_NAME));
          SetSetting(SETTING_WEB_DESC, $this->input->post(SETTING_WEB_DESC));
          SetSetting(SETTING_WEB_ABOUT, $this->input->post(SETTING_WEB_ABOUT));
          SetSetting(SETTING_ORG_MAIL, $this->input->post(SETTING_ORG_MAIL));
          SetSetting(SETTING_ORG_PHONE, $this->input->post(SETTING_ORG_PHONE));
          SetSetting(SETTING_ORG_ADDRESS, $this->input->post(SETTING_ORG_ADDRESS));
          SetSetting(SETTING_ORG_ADDRESS_DETAIL, $this->input->post(SETTING_ORG_ADDRESS_DETAIL));
          SetSetting(SETTING_ORG_ADDRESS_MAP, $this->input->post(SETTING_ORG_ADDRESS_MAP));

          $config['upload_path'] = MY_IMAGEPATH;
          $config['allowed_types'] = "gif|jpg|jpeg|png";
          $config['max_size']	= 512000;
          $config['max_width']  = 4000;
          $config['max_height']  = 4000;
          $config['overwrite'] = TRUE;
          $this->load->library('upload',$config);

          if($this->upload->do_upload('logo')){
            $fileData = $this->upload->data();
            SetSetting(SETTING_WEB_LOGO, $fileData['file_name']);
          }
      }

      redirect('user/dashboard');
    }

    public function homepage() {
      $data['title'] = 'Homepage Setting';
      if(!empty($_POST)) {
        SetSetting(SETTING_WEB_DISCLAIMER, $this->input->post(SETTING_WEB_DISCLAIMER));
        SetSetting(SETTING_WEB_SECT_OURBELIEF, $this->input->post(SETTING_WEB_SECT_OURBELIEF));
        SetSetting(SETTING_WEB_SECT_OURVALUE, $this->input->post(SETTING_WEB_SECT_OURVALUE));
        SetSetting(SETTING_WEB_SECT_OURTEAM, $this->input->post(SETTING_WEB_SECT_OURTEAM));
        SetSetting(SETTING_WEB_SECT_HOWWEWORK, $this->input->post(SETTING_WEB_SECT_HOWWEWORK));
        SetSetting(SETTING_WEB_VIDEO1, $this->input->post(SETTING_WEB_VIDEO1));
        SetSetting(SETTING_WEB_VIDEO2, $this->input->post(SETTING_WEB_VIDEO2));
      }

      $this->load->view('setting/homepage', $data);
    }
}
