<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('mpost');
    }

    public function index()
    {
        /*if(!IsLogin()) {
            redirect('user/login');
        }
        redirect('user/dashboard');
        $data['title'] = 'Beranda';
        $data['news'] = $this->mpost->search(5,"",1);
        $data['gallery'] = $this->mpost->search(5,"",4);*/
        $data['title'] = 'Home';
        $this->load->view('home/index', $data);
    }

    public function index_2() {
        $data['title'] = 'Beranda';
        $data['news'] = $this->mpost->search(4,"",1);
        $data['gallery'] = $this->mpost->search(4,"",4);
        $this->load->view('home/index_2', $data);
    }

    function mail_test() {
        $this->load->library('Mailer');
        $mail = $this->mailer->load();

        $mail->isSMTP();
        $mail->SMTPDebug = 3;
        $mail->Debugoutput = 'html';
        $mail->Host     = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'yoelrolas@gmail.com';
        $mail->Password = 'kairos120895';
        $mail->SMTPSecure = 'ssl';
        $mail->Port     = 465;

        $mail->setFrom('yoelrolas@gmail.com', 'Partopi Tao');

        // Add a recipient
        $mail->addAddress('rolassimanjuntak@gmail.com');

        // Email subject
        $mail->Subject = 'Send Email via SMTP using PHPMailer in CodeIgniter';

        // Set email format to HTML
        $mail->isHTML(true);

        // Email body content
        $mailContent = "<h1>Send HTML Email using SMTP in CodeIgniter</h1>
            <p>This is a test email sending using SMTP mail server with PHPMailer.</p>";
        $mail->Body = $mailContent;

        // Send email
        if(!$mail->send()){
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        }else{
            echo 'Message has been sent';
        }
    }

    function contact_form() {
        $this->load->library('Mailer');
        $mail = $this->mailer->load();

        $mail->isSMTP();
        $mail->SMTPDebug = 3;
        $mail->Debugoutput = 'html';
        $mail->Host     = 'sg2plcpnl0172.prod.sin2.secureserver.net';
        $mail->SMTPAuth = true;
        $mail->Username = 'noreply@rockstead.com';
        $mail->Password = 'server!2345';
        $mail->SMTPSecure = 'ssl';
        $mail->Port     = 465;

        $mail->setFrom('noreply@rockstead.com', 'Rockstead Capital');
        $mail->addAddress('fund.admin@rockstead.com');
        $mail->addBCC('yoelrolas@gmail.com');
        $mail->Subject = $this->input->post("sub");

        // Set email format to HTML
        $mail->isHTML(true);

        // Email body content
        $mailContent = @"
            <h3>Hi, Administrator</h3>
            <p>This is an auto-generated email from Rockstead Capital Contact Form with following details: </p>
            <table>
            <tr>
            <td>First Name</td><td>:</td><td>".$this->input->post('Fname')."</td>
            </tr>
            <tr>
            <td>Last Name</td><td>:</td><td>".$this->input->post('Lname')."</td>
            </tr>
            <tr>
            <td>Email</td><td>:</td><td>".$this->input->post('email')."</td>
            </tr>
            <tr>
            <td>Message</td><td>:</td><td>".$this->input->post('message')."</td>
            </tr>
            </table>
        ";
        $mail->Body = $mailContent;

        // Send email
        if(!$mail->send()){
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        }else{
            echo 'Message has been sent';
        }
    }

    function _404() {
        $this->load->view('home/error');
    }
}
