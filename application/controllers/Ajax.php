<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 01/10/2018
 * Time: 21:57
 */
class Ajax extends MY_Controller {

    function get_opt_kelurahan()
    {
        $Kd_Kec = $this->input->post("Kd_Kec");
        $ruser = GetLoggedUser();
        echo GetCombobox("SELECT * FROM mkelurahan WHERE Kd_Kecamatan = ".$Kd_Kec.($ruser[COL_ROLEID]==ROLEPPL?" AND Kd_Kelurahan in (select Kd_Kelurahan from mppl_kelurahan where Kd_PPL = ".$ruser[COL_COMPANYID].")":"")." ORDER BY Nm_Kelurahan", COL_KD_KELURAHAN, COL_NM_KELURAHAN);
    }

    function get_opt_keltan()
    {
        $Kd_Kel = $this->input->post("Kd_Kel");
        echo GetCombobox("SELECT * FROM mkeltan WHERE Kd_Kelurahan = ".$Kd_Kel." ORDER BY Nm_KelompokTani", COL_KD_KELOMPOKTANI, COL_NM_KELOMPOKTANI);
    }

    function get_detail_ppl()
    {
        $Kd_PPL = $this->input->get("Kd_PPL");
        $Kd_PPS = $this->input->get("Kd_PPS");
        $_pps = "";
        if(!empty($Kd_PPS)) {
            $_pps = " AND Kd_PPS != $Kd_PPS ";
        }
        $res = $this->db
            ->where(COL_KD_PPL, $Kd_PPL)
            ->get(TBL_MPPL)
            ->row_array();
        //$q = "SELECT * FROM mkelurahan WHERE Kd_Kelurahan in (select Kd_Kelurahan from mppl_kelurahan where Kd_PPL = $Kd_PPL $_pps) AND Kd_Kelurahan NOT IN (SELECT Kd_Kelurahan FROM mpps WHERE Kd_PPL = $Kd_PPL) ORDER BY Nm_Kelurahan";
        $opt_kelurahan = GetCombobox("SELECT * FROM mkelurahan WHERE Kd_Kelurahan in (select Kd_Kelurahan from mppl_kelurahan where Kd_PPL = $Kd_PPL) AND Kd_Kelurahan NOT IN (SELECT Kd_Kelurahan FROM mpps WHERE Kd_PPL = $Kd_PPL $_pps) ORDER BY Nm_Kelurahan", COL_KD_KELURAHAN, COL_NM_KELURAHAN, null, false, true);
        echo json_encode(array('ppl'=>$res, 'opt_kelurahan'=>$opt_kelurahan/*, 'q' => $q*/));
    }

    function get_stock_distribution() {
        $q = @"
        SELECT
        kt.*,
        mkelurahan.Nm_Kelurahan,
        mkecamatan.Nm_Kecamatan,
        (SELECT COUNT(*) FROM mkeltan_anggota a WHERE a.Kd_KelompokTani = kt.Kd_KelompokTani) AS count_anggota,
        (SELECT COUNT(*) FROM mkeltan_anggota a LEFT JOIN mkeltan_komoditas k ON k.Kd_Anggota = a.Kd_Anggota WHERE a.Kd_KelompokTani = kt.Kd_KelompokTani) AS count_komoditas,
        (SELECT COUNT(*) FROM tkeltan_bantuan a WHERE a.Kd_KelompokTani = kt.Kd_KelompokTani) AS count_kegiatan
        FROM mkeltan kt
        LEFT JOIN mkelurahan ON mkelurahan.Kd_Kelurahan = kt.Kd_Kelurahan
        LEFT JOIN mkecamatan ON mkecamatan.Kd_Kecamatan = mkelurahan.Kd_Kecamatan
        ";
        $res = $this->db->query($q)->result_array();
        echo json_encode($res);
    }

    function get_dashboard_partial() {
        $data = array(
            'opt' => $this->input->post('opt'),
            'kdkec' => $this->input->post('Kd_Kec'),
            'kdkel' => $this->input->post('Kd_Kel'),
            'kdpoktan' => $this->input->post('Kd_Keltan')
        );
        $this->load->view('ajax/dashboard_partial', $data);
    }

    function media_upload() {
        $config['upload_path'] = MY_UPLOADPATH;
        $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
        $config['max_size']	= 5120000;
        $config['max_width']  = 1600;
        $config['max_height']  = 1600;
        $config['overwrite'] = FALSE;

        $this->load->library('upload', $config);

        $resp['error'] = "";
        $resp['success'] = 1;
        $resp['Kd_Media'] = null;
        $resp['Nm_File'] = null;
        $resp['url'] = null;
        $resp['id'] = $this->input->post("id");
        if (!empty($_FILES["myfile"]["name"])) {
            if (!$this->upload->do_upload("myfile")) {
                $resp['error'] = $this->upload->display_errors();
                $resp['success'] = 0;
                echo json_encode($resp);
                return;
            }

            $dataupload = $this->upload->data();
            if (!empty($dataupload) && $dataupload['file_name']) {
                $rec = array(
                    COL_NM_FILE => $dataupload['file_name'],
                    COL_CREATEDON => date('Y-m-d H:i:s')
                );
                if($this->db->insert(TBL_MEDIA, $rec)){
                    $resp['success'] = 1;
                    $resp['Kd_Media'] = $this->db->insert_id();
                    $resp['Nm_File'] = $dataupload['file_name'];
                    $resp['url'] = MY_UPLOADURL.$dataupload['file_name'];
                }
            }
        }
        echo json_encode($resp);
    }
}